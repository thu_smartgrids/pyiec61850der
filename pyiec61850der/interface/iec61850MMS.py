# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 16:39:31 2023

@author: chen
"""

# standard python modules
import os
import logging
import time
import numpy as np
import pandas as pd

# local python modules
if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850
else:
    from pylibiec61850.windows import iec61850

import configurator.helper as helper
import interface.sunspec as sunspec
import processing

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)


def updateIedAttribute(iedServer=None, verbose=False):
    """
    This method can be used to update the values and timestamps in the demo IED Server.
    TODO: this method should be adapted to updateIedMX. Later the update method for the DA is given by the FC.
    """

    objIedServer = iedServer.iedServer.iec61850Server
    srvTimeSource = iedServer.configServer.container.timeMode

    logger.info('-------------------------------------------------------------------')
    for idxBuffer, dataBuffer in enumerate(iedServer.listDataBuffer):
        if dataBuffer.isMonitor:
            # [idx, thisVal] = iedServer.getDataBufferIndex(dataBuffer.index)
            # address = iedServer.listMonitorAddress[idx]
            # refVal = iedServer.listMonitorRef[idx]
            # refTstmp = iedServer.listTstmpRef[idx]

            address = dataBuffer.DO.monitorAddressDA
            refVal = dataBuffer.DO.monitorRefDA
            refTstmp = dataBuffer.DO.tstmpRefDA

            if refVal is not None:
                if dataBuffer.valueSource is not None:
                    if helper.isValNumber(dataBuffer.valueSource):
                        # int, float or bool
                        # first use the monitorDA MMS address to determine data type
                        # if data type is undefined in the MMS address reference, then use the data type as provided by the data source

                        if dataBuffer.DO.monitorAddressDA[-2:] == '.f' and type(dataBuffer.valueSource) not in (
                        np.float64, float):
                            # pay attention to numpy float64
                            dataBuffer.valueSource = float(dataBuffer.valueSource)
                            if verbose:
                                logger.info(
                                    f'Data type conversion for DA {dataBuffer.identifier}: type {type(dataBuffer.valueSource)} -> float.')
                        elif dataBuffer.DO.monitorAddressDA[-2:] == '.i' and not isinstance(dataBuffer.valueSource, (
                        int, np.integer, np.int32, np.int64)):
                            # TODO: add logic to distiguish different int types like int32 and int64
                            dataBuffer.valueSource = int(dataBuffer.valueSource)
                        elif dataBuffer.DO.CDC[0] in ('E', 'I') and not isinstance(dataBuffer.valueSource, (
                        int, np.integer, np.int32, np.int64)):
                            # IXX -> int; EXX -> int (Enum literal)
                            dataBuffer.valueSource = int(dataBuffer.valueSource)
                        elif dataBuffer.DO.CDC[0] in ('B', 'S') and type(dataBuffer.valueSource) is not bool:
                            # BXX -> bool (binary), SXX -> bool (single point)
                            dataBuffer.valueSource = bool(dataBuffer.valueSource)
                    else:
                        dataBuffer.valueSource = str(dataBuffer.valueSource)
                        if dataBuffer.valueSource in ('True', 'true'):
                            dataBuffer.valueSource = True
                        elif dataBuffer.valueSource in ('False', 'false'):
                            dataBuffer.valueSource = False
                        else:
                            # TODO: valueSource is non-number (int, float or boolean) -> add a string handler
                            if verbose:
                                logger.info(f'Non-number data type of the DA {dataBuffer.identifier}, skip it')
                            pass
                else:
                    if verbose:
                        logger.info(f'No value available for the DA {dataBuffer.identifier}, skip it')
                    pass

                if type(dataBuffer.valueSource) in (np.float64, float):
                    iec61850.IedServer_updateFloatAttributeValue(objIedServer, iec61850.toDataAttribute(refVal),
                                                                 dataBuffer.valueSource)
                    dataBuffer.valueIEC61850 = iec61850.IedServer_getFloatAttributeValue(objIedServer,
                                                                                         iec61850.toDataAttribute(
                                                                                             refVal))
                elif type(dataBuffer.valueSource) in (int, np.integer, np.int32, np.int64):
                    iec61850.IedServer_updateInt32AttributeValue(objIedServer, iec61850.toDataAttribute(refVal),
                                                                 dataBuffer.valueSource)
                    dataBuffer.valueIEC61850 = iec61850.IedServer_getInt32AttributeValue(objIedServer,
                                                                                         iec61850.toDataAttribute(
                                                                                             refVal))
                elif type(dataBuffer.valueSource) is bool:
                    iec61850.IedServer_updateBooleanAttributeValue(objIedServer, iec61850.toDataAttribute(refVal),
                                                                   dataBuffer.valueSource)
                    dataBuffer.valueIEC61850 = iec61850.IedServer_getBooleanAttributeValue(objIedServer,
                                                                                           iec61850.toDataAttribute(
                                                                                               refVal))
                elif type(dataBuffer.valueSource) is str:
                    iec61850.IedServer_updateVisibleStringAttributeValue(objIedServer, iec61850.toDataAttribute(refVal),
                                                                         dataBuffer.valueSource)
                    dataBuffer.valueIEC61850 = iec61850.IedServer_getStringAttributeValue(objIedServer,
                                                                                          iec61850.toDataAttribute(
                                                                                              refVal))
                else:
                    if verbose:
                        logger.info(
                            f'Invalid type {type(type(dataBuffer.valueSource))} for the DA {dataBuffer.identifier}, skip it')

                # TODO: also update the quality of the DO
                # TODO: with the use of dfNew, the index in self.statistics.dfUpload is completely messed up, solve this problem using another approach
                dfNew = pd.DataFrame(columns=dataBuffer.statistics.dfHistoric.keys())
                dfNew.loc[0, 't'] = iedServer.configServer.container.cTimeUTC
                dfNew.loc[0, 'tstmpUnix'] = iedServer.configServer.container.cTimeUnix
                dfNew.loc[0, 'valueSource'] = dataBuffer.valueSource
                dfNew.loc[0, 'valueIEC61850'] = dataBuffer.valueIEC61850
                dfNew.loc[0, 'nIntegrityUpdate'] = dataBuffer.statistics.nIntegrityUpdate
                dfNew.loc[0, 'nControl'] = dataBuffer.statistics.nControl

                dataBuffer.updateStatistics(dfNew)

                if refTstmp is not None:
                    if srvTimeSource == 'absolute':
                        iec61850.IedServer_updateUTCTimeAttributeValue(objIedServer, iec61850.toDataAttribute(refTstmp),
                                                                       helper.unixTstmpToUint64())
                    elif srvTimeSource == 'simulation':
                        iec61850.IedServer_updateUTCTimeAttributeValue(objIedServer, iec61850.toDataAttribute(refTstmp),
                                                                       helper.unixTstmpToUint64(
                                                                           iedServer.configServer.container.cTimeUnix))
                    else:
                        logger.warning('Unknown time tyoe, can not add timestamp to the float value')

            else:
                logger.info(f'Python reference not available for the DA {dataBuffer.identifier}, skip it')

        else:
            if verbose:
                logger.info(f'The monitoring is deactivated for DO {dataBuffer.DO.identifier}, skip it.')
    logger.info('-------------------------------------------------------------------\n')

    return


def performControl(instConfig=None, dataBuffer=None, val=None):
    """
    Perform a control action using the sunspec interface.
    Note: comparing old and new value is non-sense, some interface always requires a rescan before reading the new value.
    Therefore, we use try except to catch the error occurred during the write action, no error returns isExecuted = True.
    """

    # TODO:
    isExecuted = False

    if val is None:
        pass
    else:
        if dataBuffer.dataSource == 'local':
            # nothing happens
            pass
        elif dataBuffer.dataSource == 'influxdb':
            # nothing happes
            pass
        elif dataBuffer.dataSource == 'random':
            # nothing happes
            pass
        elif dataBuffer.dataSource == 'sunspec':
            idxMapping = instConfig.interface.dfSunspecMapping.loc[
                instConfig.interface.dfSunspecMapping['mmsAddress'] == dataBuffer.DO.monitorAddressDA].index.to_list()
            if len(idxMapping) != 1:
                # TODO: most of control parameters share the same mmsAddress as of monitorDA, this will be different if CDC type like APC get included in
                # libiec61850, in that case we must navigate to the controlDA through monitorDA address, so we directly use monitorDA mmsAddress here
                # check if that could be an issue.
                logger.warning(
                    f'Something went wrong, can not locate lookup index for DA {dataBuffer.DO.controlAddressDA}')
            else:
                # TODO: place data buffer update in a separate method?? iedServer.updateDataBufferConfig()??
                idxMapping = idxMapping[0]
                modelID = instConfig.interface.dfSunspecMapping.loc[idxMapping, 'Model ID']
                paraName = instConfig.interface.dfSunspecMapping.loc[idxMapping, 'Name']
                # valWrite = val/(dataBuffer.scaleFactor * dataBuffer.unitFactor)

                valWrite = processing.routine.writeDataBufferToSource(dataBuffer)

                isWritten = sunspec.writeValToInverter(dataBuffer.connObj, modelID, paraName, valWrite)
                isExecuted = isWritten

    return isExecuted


def floatUpdateCheck(valSource=None, valIEC61850=None):
    """
    This method only checks whether a float value update in server could be detected, if so,
    the dataBuffer objects should acknowledge a write / control action and respond to it accordingly.
    The comparison always convert both value to float and round up to the 4. decimal.

    Here we use bitmap to identify the status of the associated control parameter.
    """

    isUpdate = False
    flag = 0
    if valSource in [None, np.nan]:
        flag += 1
    if valIEC61850 in [None, np.nan]:
        flag += 2

    if flag == 3:
        logger.warning('Both source and IEC 61850 are None, can not proceed')
    elif flag == 2:
        logger.warning('The IEC 61850 server has recieved a None value, considered as no control')
    else:
        if flag == 1:
            valSource = 0
        else:
            try:
                valSource = round(valSource, 4)
            except:
                logger.exception(f'Source value of type {type(valSource)} can not be rounded up to the 4-th decimal')

                # if valIEC61850 == 0:
        #     '''
        #     There is a bug in the libIEC61850 server, sometimes valIEC61850 returns a 0 as value when it is supposed to be a None.
        #     TODO: find out why would the IED server deliver a 0 in that case;
        #               Use other methods to handle 0 values in server for None, we can not just write 0 everywhere.
        #     '''
        #     pass

        try:
            valIEC61850 = round(valIEC61850, 4)
            if abs(valIEC61850 - valSource) > 1e-6:
                # logger.info(f'Detected change in the control value; valSource: {valSource}, valIEC61850 {valIEC61850}')
                isUpdate = True
            else:
                pass
        except:
            logger.exception(f'Value in the IEC 61850 server of type {type(valIEC61850)} can not be roudned up to the 4-th decimal')

    return isUpdate, valSource, valIEC61850


def iedControlWatchdog(instConfig=None, iedServer=None, nIteration=None, intervalControl=None, verbose=False):
    """
    This method can be used to update the values and timestamps in the demo IED Server.
    verbose True means log every single details.
    TODO: considering remove the iteration here, since control watchdog runs in a separate threads now
    TODO: the control watchdog produce too many logs, does verbose=False help? should debug level be used?
    """

    logger.info('-------------------------------------------------------------------')
    logger.info('Control watchdog loops over dataBuffer instance to inspect control actions.')
    logger.info(f'CLS configuration requires {nIteration} iterations, each lasts {intervalControl} seconds.')

    objIedServer = iedServer.iedServer.iec61850Server
    nControl = 0

    i = 0
    while i < nIteration:  # control watch dog processes x time faster than the measurement interval, x = nIteration
        nControlDO = 0
        #logger.info(f'Performing control inspection iteration {i + 1}')
        startTime = time.time()
        try:
            # TODO: define error handlers later
            for idx, dataBuffer in enumerate(iedServer.listDataBuffer):
                if dataBuffer.isControl:
                    nControlDO += 1
                    if verbose:
                        logger.info(f'Detected controllalbe DO: {dataBuffer.identifier}')
                    address = dataBuffer.DO.controlAddressDA
                    # TODO: add address check for the general case
                    refControl = dataBuffer.DO.controlRefDA

                    if refControl is not None:
                        valSource = dataBuffer.valueSource
                        #
                        valIEC61850 = iec61850.IedServer_getFloatAttributeValue(objIedServer,
                                                                                iec61850.toDataAttribute(refControl))

                        if verbose:
                            logger.info(f'Value source: {valSource}; value IEC61850 server: {valIEC61850}')
                        # currently only float and int values are considered
                        [isUpdate, valSource, valIEC61850] = floatUpdateCheck(valSource, valIEC61850)

                        if isUpdate:
                            logger.info('************************************')
                            logger.info('***  control command received ******')
                            logger.info('************************************')
                            logger.info(
                                f'The value of control parameter {dataBuffer.identifier} has been changed: {valSource} -> {valIEC61850}')

                            dataBuffer.valueSource = valIEC61850
                            dataBuffer.valueIEC61850 = valIEC61850

                            logger.info('Now try to perform the control at local level')
                            isExecuted = performControl(instConfig, dataBuffer, valIEC61850)
                            if isExecuted:
                                logger.info('Successfully performed the control command from the IED server side')
                                nControl += 1
                                dataBuffer.statistics.nControl += 1
                            else:
                                logger.info(
                                    'Failed to perform the control command at local level, check the log for more details')
                        else:
                            # debug level logging, just pass
                            if verbose:
                                logger.info(f'check value {dataBuffer.DO.controlAddressDA}, Value in dataBuffer: {dataBuffer.valueSource}; value in server: {dataBuffer.valueIEC61850}')
                            # TODO: it would be wrong to just passing each failed control. A flushing trigger should be implemented, e.g. after 10 failed attempts, ignore this control command.
                            pass
                    else:
                        if verbose:
                            logger.info(f'The reference object of DO {dataBuffer.DO.identifier} is not set, skip it')
                else:
                    if verbose:
                        logger.info(f'The control is deactivated for DO {dataBuffer.DO.identifier}, skip it.')
        except:
            logger.exception(f'Control watchdog encountered error in this iteration, double check the codes for control interface.')

        endTime = time.time()
        #logger.info(f'{nControlDO} DO under control supervision in this iteration.')

        # implement a method to deal with longer control iterations.
        if intervalControl > endTime - startTime:
            tSleep = intervalControl - (endTime - startTime)
            if tSleep < 0:
                i += - tSleep // intervalControl + 1
                tSleep = tSleep % intervalControl
                #logger.info(f'The last control iteration took longer than {intervalControl}s, skip to iteration {i}')
                time.sleep(tSleep)
            else:
                i += 1
                #logger.info(f'Sleep {tSleep}s to synchronize the time')
                time.sleep(tSleep)

    logger.info('Inspection of control actions, finished.')
    logger.info(f'{nControl} control command is executed in the last interval')
    logger.info('-------------------------------------------------------------------\n')


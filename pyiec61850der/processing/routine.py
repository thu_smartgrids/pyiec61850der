# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:13:57 2023

@author: chen

This sub-module contains functions that are necessary for the data processing in simulation routien,
these include:
    - get current value from the profile
    - update measurements for all measuring points
    - passing control values to the data interface instances

TODO: more descp for the data sources

"""

import logging
import random
import os
import numpy as np

if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850

    isWindows = False
else:
    from pylibiec61850.windows import iec61850

    isWindows = True

import interface
import configurator.helper as helper
import processing.calculation as calculation
from configurator.helper import timeoutChecker

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)


# %% main part of the routine submodule
def getValInProfile(instProfile=None, cTimeStr=None, fieldKey='_value'):
    """
    This method captures the corresponding measurement given the current timestamp.
    Currently it only handles single data point.
    TODO: add logics to enable the read of multiple columns (fields in influxdb definition)
        in the input profile instance.

    Usage:
        valNew = processing.routine.getValInProfile(instProfile, cTimeStr, fieldKey='_value')

    Input:
        - instProfile: instance of the profile where the values are stored
        - cTimeStr: the time string of current time
        - fieldKey: the key of the value in the dataframe, for single measurement it takes the default _value

    Output:
        - valNew: the exact value to be read, returning None means could not find the proper value
    """

    valNew = None
    if instProfile is None:
        logger.warning('No profile avaialable, can not read the new value')
    elif instProfile.nVal == 0:
        logger.warning('The profile has length 0, can not read the new value')
    else:
        idxTstmp = [idx for idx, tstmp in enumerate(list(instProfile.dfVal['_time']))
                    if cTimeStr in str(tstmp)]
        if idxTstmp != []:
            valNew = instProfile.dfVal.loc[idxTstmp[0], '_value']
        else:
            logger.warning(f'The required timestep {cTimeStr} not found in the profile dataframe')
    return valNew


def getValGroupInProfile():
    """
    A place holder
    """

    pass


def displayVal(iedServer=None):
    """
    This function simply display the current value of th IEC 61850 parameters,
    it considers both the values from source and values prepared for the IEC 61850 server.
    If the data are not identical on both sides, there must be something wrong.

    TODO: display the values in dataBuffer and values in IED server on one line?
    """

    logger.info('-------------------------------------------------------------------')

    logger.info('Current values of the IEC 61850 DA from the source:')

    for item in iedServer.listDataBuffer:
        if item.isMonitor:
            logger.info(f'{item.identifier}: {item.valueSource}')
    logger.info('-------------------------------------------------------------------\n')

    logger.info('-------------------------------------------------------------------')
    logger.info('Current values of the IEC 61850 DA in the IEC 61850 server:')

    for item in iedServer.listDataBuffer:
        if item.isMonitor:
            logger.info(f'{item.identifier}: {item.valueIEC61850}')

    logger.info('-------------------------------------------------------------------\n')


def writeValActToDataBuffer(dataBuffer=None, valNew=None, listLimit=None):
    """
    This function helps to calculate the actual value of a parameter that is frequently read from a data source. It takes the
    scaleFactor and unitFactor of the pre-configuration into account, based on that it generate the scaled value and write it to
    the dataBuffer instance -> dataBuffer.valueSource.

    If there is anything wrong with the factors, it just ignore the scale factors and write the fresh fetched value directly to the
    dataBuffer, which may cause unplausible values in the IED server. In such a case, the unit and scale of a DA might be messed up,
    but the server can still run. Just pay attention to the unscaled values!

    Besides, if the limit is set to some wrong data types, this function will reactivate the default value, which equals to no limit.
    """

    # if not helper.isValNumber(dataBuffer.limit[1]) or not helper.isValNumber(dataBuffer.limit[0]):
    #     logger.warning('Data type of dataBuffer.limit is not a number, reset it to default [-999999999,999999999]')
    #     dataBuffer.limit = [-999999999,999999999]

    if listLimit['lower'] is None:
        lowerLimit = - np.inf
        logger.warning(f'Detected invalid lower limit for {dataBuffer.identifier}, use -inf instead')
    else:
        lowerLimit = listLimit['lower']

    if listLimit['upper'] is None:
        upperLimit = np.inf
        logger.warning(f'Detected invalid upper limit for {dataBuffer.identifier}, use inf instead')
    else:
        upperLimit = listLimit['upper']

    try:
        valAct = valNew * dataBuffer.unitFactor * dataBuffer.scaleFactor
        if not helper.isValNumber(valAct):
            logger.warning(
                'The var valAct is not a number, please double check the data type of new value, scaleFactor and unitFactor')
            logger.warning(
                'Writing valNew directly instead, without multiplicating scaleFactor and unitFactor, this could cause error')
            dataBuffer.valueSource = max(min(valNew, upperLimit), lowerLimit)
            valAct = valNew
        else:
            dataBuffer.valueSource = max(min(valAct, upperLimit), lowerLimit)
    except:
        valAct = valNew
        logger.exception('Wrong data type, double check the data type of new value, scaleFactor and unitFactor')

    return valAct


def writeDataBufferToSource(dataBuffer=None):
    '''
    This function passes actual values of those parameters that have been controlled by IEC 61850 clients to the corresponding dataBuffer instances.
    '''

    try:
        valWrite = dataBuffer.valueSource / (dataBuffer.unitFactor * dataBuffer.scaleFactor)
        if not helper.isValNumber(valWrite):
            logger.warning(
                'The var valWrite is not a number, please double check the data type of new value, scaleFactor and unitFactor')
            logger.warning(
                'Set dataBuffer.dataSource to valWrite directly instead, without multiplicating scaleFactor and unitFactor, this could cause error')
            valWrite = dataBuffer.valueSource
    except:
        valWrite = dataBuffer.valueSource
        logger.exception('Wrong data type, double check the data type of new value, scaleFactor and unitFactor')

    return valWrite


def initBufferSingleVal(instConfig=None, iedServer=None, dataBuffer=None, cTimeStr=None, **kwargs):
    """
    This function iterates over the list object iedServer.listDataBuffer and generate an initial value
    for each of the parameters in the list as configured. (each parameter can have a different data source)

    When starting a simulation, this fucntion only has to be called once per IEC 61850 Data Attribute during the initialization.

    For parameters that is using a local profile, dataBuffer.profileLocal must have been set before the initialization.
    Calling this function triggers the methode interface.local/influxdb.getProfile given current timestamp, then it takes
    the first value as the init value.

    Usage:
        processing.routine.initBufferSingleVal(instConfig, iedServer, dataBuffer, dataSource, cTimeStr)

    Input:
        - instConfig: instance of the global configuration, containing interface config and mapping tables
        - iedServer: instance of the running iedServer, containing IEC 61850 and communication entries
        - dataBuffer: one instance of the class DataBuffer
        - cTimeStr: current timestamp in string format, e.g. '%Y-%m-%d %H:%M:%S'
        - value: only applicable for the dataSource "random", a user defined random value

    Output:
        The function has no return value, the instance dataBuffer will be updated dynamically.

    """

    valNew = None
    if dataBuffer.dataSource == 'disabled':
        logger.info(f'No data source configured for the DA {dataBuffer.identifier}, skip it')
    elif dataBuffer.dataSource == 'number':
        # if 'value' in dataBuffer.configJSON.keys():
        #     valNew = dataBuffer.configJSON['value']
        if dataBuffer.valueSource is not None:
            valNew = dataBuffer.valueSource
    elif dataBuffer.dataSource == 'random':
        if dataBuffer.valueSource is not None:
            valNew = dataBuffer.valueSource
    elif dataBuffer.dataSource == 'local':
        try:
            valNew = getValInProfile(dataBuffer.profileLocal.profile, cTimeStr, fieldKey='_value')
        except:
            logger.exception(f'Can not find valid data for the parameter {dataBuffer.identifier} in local data profile')
    elif dataBuffer.dataSource == 'influxdb':
        try:
            valNew = getValInProfile(dataBuffer.profileInfluxdb.profile, cTimeStr, fieldKey='_value')
        except:
            logger.exception(f'Can not find valid data for the parameter {dataBuffer.identifier} in influxdb')
    elif dataBuffer.dataSource == 'sunspec':
        valNew = interface.sunspec.readSingleValue(instConfig, iedServer, dataBuffer)
    elif dataBuffer.dataSource == 'calculation':
        logger.info(
            f'Data source for the DA {dataBuffer.identifier} configured to be calculated, no initialisation required.')
    else:
        logger.warning(f'Unknown data source: {dataBuffer.dataSource}, please check the config file.')

    if valNew is None:
        pass
    else:
        dictLimit = updateDataBufferLimit(iedServer, dataBuffer)
        valAct = writeValActToDataBuffer(dataBuffer, valNew, dictLimit)


def updateBufferSingleVal(instConfig=None, iedServer=None, dataBuffer=None, cTimeStr=None):
    """
    This function iterates over the list object iedServer.listDataBuffer and update the current values
    for each of the parameters in the list according to the pre-configured data source.
    (each parameter can have a different data source)

    For parameters that is using a local profile, dataBuffer.profile must have been set before the initialization.
    (which is done by calling the other function initBufferSingleVal)

    For parameters that is using a influxdb profile, it requires the attributes unitFactor, scaleFactor and limit
    to be set during the server or interface initialization.

    Usage:
        processing.routine.updateBufferSingleVal(instConfig, iedServer, dataBuffer, cTimeStr)

    Input:
        - instConfig: instance of the global configuration, containing interface config and mapping tables
        - iedServer: instance of the running iedServer, containing IEC 61850 and communication entries
        - dataBuffer: one instance of the class DataBuffer
        - cTimeStr: current timestamp in string format, e.g. '%Y-%m-%d %H:%M:%S'

    Output:
        The function has no return value, the instance dataBuffer will be updated dynamically.
    """

    valNew = None

    if dataBuffer.dataSource == 'disabled':
        logger.warning(f'No data source configured for the DA {dataBuffer.identifier}, skip it')
    elif dataBuffer.dataSource == 'number':
        pass
    elif dataBuffer.dataSource == 'random':
        if dataBuffer.valueSource is not None:
            dataBuffer.valueSource += random.randint(-1000, 1000) / 10000
        else:
            logger.warning(f'No initial value configured for the DO {dataBuffer.identifier}')
    elif dataBuffer.dataSource == 'local':
        if dataBuffer.profileLocal.profile is None:
            logger.warning(
                f'No short time profile avaialbe for the DA {dataBuffer.identifier}, load the local profile first')
        else:
            valNew = getValInProfile(dataBuffer.profileLocal.profile, cTimeStr, fieldKey='_value')
    elif dataBuffer.dataSource == 'influxdb':
        if dataBuffer.profileInfluxdb.profile is None:
            startTimeStr = helper.getTimeStrByDiff(cTimeStr, timeStrFormat='%Y-%m-%d %H:%M:%S', timeDiff=-300)
            kwTag = interface.influxdb.getTagIEC61850(dataBuffer)
            newProfile = interface.influxdb.getProfile(dataBuffer.influxdb.clientInfluxdbRead,
                                                       dataBuffer.influxdb.bucketRead,
                                                       dataBuffer.influxdb.measurementRead, startTimeStr,
                                                       timeRange=86400, kwTag=kwTag, interval=60)
            if newProfile is not None:
                dataBuffer.profileInfluxdb.profile = newProfile
        valNew = getValInProfile(dataBuffer.profileInfluxdb.profile, cTimeStr, fieldKey='_value')
    elif dataBuffer.dataSource == 'sunspec':
        valNew = interface.sunspec.readSingleValue(instConfig, iedServer, dataBuffer)
    elif dataBuffer.dataSource == 'calculation':
        # TODO: here it will not be checked whether the associated values have been updated at current timestep
        # i.e. the "calculation" might be delayed, implement a method to update DO with priority
        if hasattr(dataBuffer, 'functionHandler'):
            if hasattr(calculation, dataBuffer.functionHandler):
                if hasattr(dataBuffer, 'args'):
                    args = dataBuffer.args
                else:
                    args = []
                dynamicFunction = getattr(calculation, dataBuffer.functionHandler)
                valNew = dynamicFunction(iedServer, dataBuffer, *args)

    else:
        logger.error(f'Unknown data source: {dataBuffer.dataSource}, please check the config file.')

    if valNew is None:
        pass
    else:
        dictLimit = updateDataBufferLimit(iedServer, dataBuffer)
        valAct = writeValActToDataBuffer(dataBuffer, valNew, dictLimit)


def initDataBuffer(instConfig=None, iedServer=None, cTimeStr=None, verbose=True):
    """
    Initialization of the measurements for the IEc 61850 server.
    """

    logger.info('=================   Begin measurement initialization    =================')
    logger.info(f'{iedServer.nDataBuffer} parameters found in the data buffer list')

    for dataBuffer in iedServer.listDataBuffer:
        if not dataBuffer.isMonitor:
            continue
        else:
            if dataBuffer.dataSource in ['local', 'influxdb']:  # only local or influxdb entries require profile
                updateDataProfile(dataBuffer, cTimeStr, timeRange=86400, interval=60)
            initBufferSingleVal(instConfig, iedServer, dataBuffer, cTimeStr)

    # TODO: check whether the updateIedAttribute is necessary during dataBuffer initialisation.
    # interface.iec61850MMS.updateIedAttribute(iedServer)

    if verbose:
        logger.info("Display measurements of the running IED server")
        displayVal(iedServer)

    listDataSource = list(instConfig.interface.dfLookupCSV['dataSource'].unique())
    logger.info(f'Currently active data interfaces in the lookup table are: {listDataSource}.')
    listDataSourceInBuffer = set([dataBuffer.dataSource for dataBuffer in iedServer.listDataBuffer])
    logger.info(f'Currently active data interfaces in the dataBuffers are: {listDataSource}.')
    logger.info('=================   End measurement initialization    =================\n')


def updateDataBuffer(instConfig=None, iedServer=None, cTimeStr=None, triggerTime='00:00:00', verbose=True):
    """
    Iteratively update the measurements for the IEC 61850 server.
    """

    listDataSource = list(instConfig.interface.dfLookupCSV['dataSource'].unique())
    logger.info('-------------------------------------------------------------------')
    logger.info('Start refreshing connection objects.')
    logger.info(f'Currently active data interfaces are: {listDataSource}.')

    for item in listDataSource:
        connObjScan(instConfig.interface.dfLookupCSV, item)

    logger.info('Refreshing connection objects has been completed for this iteration.')
    logger.info('-------------------------------------------------------------------\n')

    logger.info('=================   Begin measurement update    =================')
    logger.info(f'Currently the update interval is configured as {instConfig.CLS.interval} seconds')

    for idx, dataBuffer in enumerate(iedServer.listDataBuffer):
        if not dataBuffer.isMonitor:
            continue
        else:
            if triggerTime in cTimeStr:
                # TODO: check whether the update of profile of many parameters would take more than 1 minute
                logger.info('Data update of the data profile is triggered for the DA {dataBuffer.identifier}')
                updateDataProfile(dataBuffer, cTimeStr, timeRange=86400, interval=60)

            # logger.info(f'Update parameter values in the data buffer {dataBuffer.identifier}')
            updateBufferSingleVal(instConfig, iedServer, dataBuffer, cTimeStr)

        # logger.info('Passing parameter values to the IEC 61850 server')

    interface.iec61850MMS.updateIedAttribute(iedServer)

    logger.info('=================   End measurement update    =================\n')

    if verbose:
        logger.info("Display measurements of the running IED server")
        displayVal(iedServer)


@timeoutChecker(5)  # set a 20s timeout. connObj refresh taking longer than 20s indicates connection interruption.
def connObjScan(dfLookupCSV=None, connType='sunspec'):
    """
    Some connection objects requires regularly data scan to update its data entris, e.g. sunspec conn object.
    This function will be called at the beginning of each updateDataBuffer round to perform a full data scan.
    """

    if connType == 'sunspec':
        dfLookupThisType = dfLookupCSV.loc[dfLookupCSV['dataSource'] == connType]
        listConnObj = list(dfLookupThisType['connObject'].unique())
        logger.info(f'{len(listConnObj)} unique connection objects require refreshing for conntype {connType}.')
        for idx, connObj in enumerate(listConnObj):
            try:
                connObj.scan()
                logger.info(f'refresh of connObj with index {idx} sucessfully executed.')
            except:
                logger.exception(f'Device scan for connObj with index {idx} failed, no data update in this iteration.')
    elif connType in ['local', 'number', 'random', 'calculation']:
        logger.info(f'No connObj update is required for the type "{connType}", proceed to the next step.')
    elif connType == 'influxdb':
        # TODO: implement influxdb connObj
        pass
    elif connType == 'disabled':
        pass
    else:
        logger.info(f'Unknown connObjtype {connType}, no action taken, proceed to the next step.')


def updateDataProfile(dataBuffer=None, cTimeStr=None, timeRange=86400, interval=60):
    """
    This function can be used to update the short term profiles for each DA.
    """

    logger.info('-------------------------------------------------------------------')
    logger.info(f'Start update the short term profile for the DA {dataBuffer.identifier}')

    if dataBuffer.dataSource == 'local':
        if dataBuffer.profileLocal.profileAll is None:
            logger.info(
                f'No local profile avaialbe for the DA {dataBuffer.identifier}, try directly initialize profile')
            if dataBuffer.profileLocal.provider is None:
                logger.info('No local profile provider configured for this DA')
            else:
                try:
                    startTime = cTimeStr
                    endTime = helper.unixTstmpToString(helper.utcTstmpToUnix(startTime) + 86400)
                    dataBuffer.profileLocal.profileAll = interface.local.initProfile(dataBuffer.profileLocal.provider,
                                                                                     'Value', 'Datetime',
                                                                                     '%Y-%m-%d %H:%M:%S', startTime,
                                                                                     endTime)
                    try:
                        dataBuffer.profileLocal.profile = interface.local.getProfile(cTimeStr,
                                                                                     dataBuffer.profileLocal.profileAll,
                                                                                     timeRange=86400, interval=60)
                    except:
                        logger.exception('Update short term profile failed.')
                except:
                    logger.exception('Load local profile failed.')

        else:
            dataBuffer.profileLocal.profile = interface.local.getProfile(cTimeStr, dataBuffer.profileLocal.profileAll,
                                                                         timeRange=86400, interval=60)

    elif dataBuffer.dataSource == 'influxdb':
        startTimeStr = helper.getTimeStrByDiff(cTimeStr, timeStrFormat='%Y-%m-%d %H:%M:%S', timeDiff=-300)

        kwTag = interface.influxdb.getTagIEC61850(dataBuffer)
        newProfile = interface.influxdb.getProfile(dataBuffer.influxdb.clientInfluxdbRead,
                                                   dataBuffer.influxdb.bucketRead, dataBuffer.influxdb.measurementRead,
                                                   startTimeStr,
                                                   timeRange=86400, kwTag=kwTag, interval=60)
        if newProfile is not None:
            dataBuffer.profileInfluxdb.profile = newProfile
    else:
        logger.info(f'No profile interface configured for the data source type {dataBuffer.dataSource}')


def updateDataBufferLimit(iedServer=None, dataBuffer=None):
    """
    This function is used to update the attribute "limit" of DataBuffer instances. Normaly the lower and upper limits are two
    numbers, but to some extend the value could be function handler that points to another dataBuffer.
    e.g. in IEC61850, the max of WRtg is taken from WRtgMax, and OutWSet can be limited by WRtg
    This is just an example, the actual implementation could differ from that.
    
    Anyway, this function takes care of these dataBuffer limits and makes sure that of boundary values are updated
    in a real-time environment.
    """

    dictLimit = dict({'lower': -np.inf, 'upper': np.inf})

    if None in [dataBuffer.unitFactor, dataBuffer.scaleFactor, dataBuffer.limit]:
        logger.warning('The value update requires pre-configuration of the data buffer instance, check the attributes again.')
    else:
        for key, val in dataBuffer.limit.items():
            if not helper.isValNumber(val):
                if type(val) is dict:  # check if function handler exists
                    if 'idxBuffer' in val.keys():
                        [idx, valLimit] = iedServer.getDataBufferIndex(val['idxBuffer'])
                        if valLimit is not None:
                            dictLimit[key] = valLimit
                            logger.info(f'check limit for dataBuffer {dataBuffer.identifier}')
                            logger.info(f'target dataBuffer {iedServer.listDataBuffer[idx].identifier}')
                    elif ['functionHandler', 'args'] <= list(val.keys()):
                        # TODO: this is only a placeholder, implementation follows if a specific application is in demand
                        dynamicFunction = getattr(calculation, val['functionHandler'])
                        dictLimit[key]  = dynamicFunction(val['args'])
                    else:
                        pass
                else:
                    logger.warning(f'Unknown type {type(val)} detected in dataBuffer.limit')
            else:
                dictLimit[key]  = val

            if not helper.isValNumber(dictLimit[key]):
                logger.warning(f'limit {key} of dataBuffer {dataBuffer.identifier} is not a number: {dictLimit[key]}')
    return dictLimit

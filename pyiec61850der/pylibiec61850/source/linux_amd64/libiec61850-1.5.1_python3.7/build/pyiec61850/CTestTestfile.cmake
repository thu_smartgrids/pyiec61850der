# CMake generated Testfile for 
# Source directory: /work/libiec61850-1.5.1/pyiec61850
# Build directory: /work/libiec61850-1.5.1/build/pyiec61850
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_pyiec61850 "/usr/local/bin/python" "/work/libiec61850-1.5.1/pyiec61850/test_pyiec61850.py")
set_tests_properties(test_pyiec61850 PROPERTIES  _BACKTRACE_TRIPLES "/work/libiec61850-1.5.1/pyiec61850/CMakeLists.txt;44;add_test;/work/libiec61850-1.5.1/pyiec61850/CMakeLists.txt;0;")

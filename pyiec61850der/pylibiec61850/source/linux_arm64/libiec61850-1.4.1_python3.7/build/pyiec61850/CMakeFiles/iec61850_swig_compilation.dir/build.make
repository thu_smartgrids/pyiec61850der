# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.13

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /work/pylibiec61850/linux/libiec61850-1.4.1

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /work/pylibiec61850/linux/libiec61850-1.4.1/build

# Utility rule file for iec61850_swig_compilation.

# Include the progress variables for this target.
include pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/progress.make

pyiec61850/CMakeFiles/iec61850_swig_compilation: pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON.stamp


pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON.stamp: ../pyiec61850/iec61850.i
pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON.stamp: ../pyiec61850/iec61850.i
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --blue --bold --progress-dir=/work/pylibiec61850/linux/libiec61850-1.4.1/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Swig compile iec61850.i for python"
	cd /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 && /usr/bin/cmake -E make_directory /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850/CMakeFiles/_iec61850.dir
	cd /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 && /usr/bin/cmake -E touch /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON.stamp
	cd /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 && /usr/bin/cmake -E env SWIG_LIB=/usr/share/swig3.0 /usr/bin/swig3.0 -python -outdir /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 -c++ -I/work/pylibiec61850/linux/libiec61850-1.4.1/build/config -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/common/inc -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/goose -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/sampled_values -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/hal/inc -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc_private -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc_private -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/iso_mms/asn1c -I/work/pylibiec61850/linux/libiec61850-1.4.1/src/logging -I/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc -I/usr/local/include/python3.7m -I/work/pylibiec61850/linux/libiec61850-1.4.1/pyiec61850 -o /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON_wrap.cxx /work/pylibiec61850/linux/libiec61850-1.4.1/pyiec61850/iec61850.i

iec61850_swig_compilation: pyiec61850/CMakeFiles/iec61850_swig_compilation
iec61850_swig_compilation: pyiec61850/CMakeFiles/_iec61850.dir/iec61850PYTHON.stamp
iec61850_swig_compilation: pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/build.make

.PHONY : iec61850_swig_compilation

# Rule to build all files generated by this target.
pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/build: iec61850_swig_compilation

.PHONY : pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/build

pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/clean:
	cd /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 && $(CMAKE_COMMAND) -P CMakeFiles/iec61850_swig_compilation.dir/cmake_clean.cmake
.PHONY : pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/clean

pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/depend:
	cd /work/pylibiec61850/linux/libiec61850-1.4.1/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /work/pylibiec61850/linux/libiec61850-1.4.1 /work/pylibiec61850/linux/libiec61850-1.4.1/pyiec61850 /work/pylibiec61850/linux/libiec61850-1.4.1/build /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850 /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : pyiec61850/CMakeFiles/iec61850_swig_compilation.dir/depend


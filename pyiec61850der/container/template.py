# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 16:11:18 2024

@author: chen
"""

import json
import numpy as np


class NpEncoder(json.JSONEncoder): 
    # code by Tommy and Jie Yang
    # https://stackoverflow.com/questions/50916422/python-typeerror-object-of-type-int64-is-not-json-serializable
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


class YAMLStringTemplate(object):
    '''
    This is only a demonstrator, feel free to add as many placeholders as you want.
    Do not worry about the tab indents, they have to stay that way, because yaml depends
    very much on correct spacing and indents.
    '''
    
    def __init__(self):
        self.headerStr = """version: '3.5'
networks:
  <PLACEHOLDER_NETWORK>-proxy:
    external:
      name: <PLACEHOLDER_NETWORK>
"""

        self.headerStrNoNetwork = """version: '3.5'
"""

        self.builderStr = """services:
  <PLACEHOLDER_SERVICE>:
    build: 
        context: ./
        dockerfile: Dockerfile
    image: <PLACEHOLDER_IMAGE>
    container_name: <PLACEHOLDER_CONTAINER>
    restart: 'no'
    volumes: 
      - /sys:/rootfs/sys:ro
      - /proc:/rootfs/proc:ro
      - /etc:/rootfs/etc:ro
    environment:
      HOST_PROC: /rootfs/proc
      HOST_SYS: /rootfs/sys
      HOST_ETC: /rootfs/etc
    ports:
      - '<PLACEHOLDER_PORT>:61850'
"""

        self.templateStrService = """services:
"""            

        self.templateStr = """  <PLACEHOLDER_SERVICE>:
    image: <PLACEHOLDER_IMAGE>
    container_name: <PLACEHOLDER_CONTAINER>
    restart: 'no'
    volumes: 
      - /sys:/rootfs/sys:ro
      - /proc:/rootfs/proc:ro
      - /etc:/rootfs/etc:ro
<PLACEHOLDER_MOUNT>
    environment:
      HOST_PROC: /rootfs/proc
      HOST_SYS: /rootfs/sys
      HOST_ETC: /rootfs/etc
    ports:
      - '<PLACEHOLDER_PORT>:61850'  
    networks:
      - <PLACEHOLDER_NETWORK>-proxy
"""

        self.templateStrNoNetwork = """  <PLACEHOLDER_SERVICE>:
    image: <PLACEHOLDER_IMAGE>
    container_name: <PLACEHOLDER_CONTAINER>
    restart: 'no'
    volumes: 
      - /sys:/rootfs/sys:ro
      - /proc:/rootfs/proc:ro
      - /etc:/rootfs/etc:ro
      - './<PLACEHOLDER_SERVICE>/config.json:/work/config.json'
      #- '/share/application_backup/virtual-cls-demonstrator/<PLACEHOLDER_FRAMEWORK>/<PLACEHOLDER_SERVICE>:/work'
    environment:
      HOST_PROC: /rootfs/proc
      HOST_SYS: /rootfs/sys
      HOST_ETC: /rootfs/etc
    ports:
      - '<PLACEHOLDER_PORT>:61850'  
"""

        self.footStr = """volumes:
  sys:
  proc:
  etc:

"""


class Placeholder(object):
    '''
    This is only a demonstrator, feel free to add as many placeholders as you want.
    '''    

    def __init__(self):
       self.SERVICE = '<PLACEHOLDER_SERVICE>'
       self.PORT = '<PLACEHOLDER_PORT>'
       self.CONTAINER = '<PLACEHOLDER_CONTAINER>'
       self.IMAGE = '<PLACEHOLDER_IMAGE>'
       self.NETWORK = '<PLACEHOLDER_NETWORK>'
       self.MOUNT = '<PLACEHOLDER_MOUNT>'
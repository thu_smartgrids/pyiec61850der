# # -*- coding: utf-8 -*-
# """
# Created on Thu Feb 16 14:35:39 2023

# @author: chen
# """

import sys
import os

'''
This testing script can be used in the docker container to locate module import errors.
'''

# TODO: change app to cdir
if os.name != 'nt':  # if not windows os
    sys.path.append("/work/")
    listFile = os.listdir("/work/")
    print("check files in working directory")
    print(listFile)
    os.environ["PATH"] = "/work/;" + os.environ["PATH"]
    
import iec61850 as iec
from pylibiec61850.linux import iec61850
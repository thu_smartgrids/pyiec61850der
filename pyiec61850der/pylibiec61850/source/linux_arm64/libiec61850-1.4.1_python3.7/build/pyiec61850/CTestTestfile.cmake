# CMake generated Testfile for 
# Source directory: /work/pylibiec61850/linux/libiec61850-1.4.1/pyiec61850
# Build directory: /work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_pyiec61850 "/usr/local/bin/python" "/work/pylibiec61850/linux/libiec61850-1.4.1/pyiec61850/test_pyiec61850.py")

```
pyiec61850der\
├── doc\
├── figure\
├── pyiec61850der\  # the main project directory
│   ├── communication\  # any kind of communication services
│   │   ├── demo.py  # codes for the demo program using hard-coded IEC 61850 data model
│   │   ├── iec61850App.py  # currently only a place holder
│   │   ├── iec61850Server.py  # the initialisation of the IEC 61850 MMS server
│   │   └── __init__.py
│   │
│   ├── configurator\  # any kind of configuration functions/files
│   │   ├── config.json  # config file for the local control unit
│   │   ├── config.py  # Config class initialiser
│   │   ├── helper.py  # some very useful helper functions
│   │   ├── IEC61850_DA_GUID_demoAutoGenCLS.csv  # returned by iec61850Server.py containing the GUIDs
│   │   ├── IEC61850_DA_lookup_demoAutoGenCLS.csv  # returned by iec61850Server.py containing a parameter lookup table
│   │   ├── logging.py  # logger initialiser
│   │   └── __init__.py
│   │
│   ├── container\
│   │   ├── batch\  # a place holder for massive container configuration
│   │   ├── build\  # docker build template
│   │   │   ├── docker-compose.yml
│   │   │   ├── Dockerfile
│   │   │   └── requirements.txt
│   │   └── config_yaml.py  # a place holder for massive container configuration
│   │
│   ├── converter\  # any kind of model converters (one IEC 61850 - XML converter will be posted soon) 
│   │
│   ├── data\  # local data hub for profiling or prediction
│   │   ├── profile\  # local load/PV/E-mobility profiles
│   │   │   └── 0a45182b-2f4a-4a2f-b136-91cd182a457e.csv  # a demo CSV profile for test purpose
│   │   ├── profile.py  # Profile class initialiser
│   │   └── __init__.py
│   │
│   ├── interface\  # any kind of interfaces (as long as data transfer is required)
│   │   ├── iec61850MMS.py  # IEC 61850 MMS interface
│   │   ├── influxdb.py  # influxdb interface
│   │   ├── local.py  # local file system interface
│   │   ├── runtime.py  # passing variables in runtime 
│   │   └── __init__.py
│   │
│   ├── logs\  # all log files should fleed in here
│   │
│   ├── model\  # anything related to IEC 61850 data model
│   │   ├── IEC61850_dataModel\  # communication.iec61850Server.py will read the data model from this folder
│   │   ├── iec61850DataTemplates.py  # pre-defined DER data model templates
│   │   ├── iec61850DataModelGenerator.py  # IEC 61850 data model generator
│   │   ├── modelManager.py  # some supporting functions for the IEC 61850 data model
│   │   └── outputs\  # storage of new data models returned by the model generator
│   │
│   ├── optimization\  # a place holder for the implementation of numerical optimasation algorithms
│   │
│   ├── processing\  # anything related to data processing
│   │   ├── calculation.py  # a place holder for local data calculation
│   │   ├── model.py  # a place holder for mathematical modelling
│   │   ├── routine.py  # runtime data processing routines
│   │   └── __init__.py
│   │
│   ├── pylibiec61850\  # compiled libiec61850 for python binding (ATTENTION: force git add each new source lib, otherwise git ignores the build subfolder)
│   │   ├── source\  # after container build, this folder will be deleted completely to shrink image size
│   │   │   ├── linux_amd64\  # storage of all compiled pylibiec61850 source files for amd64 (x86-64) linux applications
│   │   │   ├── linux_arm64\  # storage of all compiled pylibiec61850 source files for arm64 linux applications
│   │   │   ├── windows\  # storage of all compiled pylibiec61850 source files for windows applications
│   │   ├── linux\  # the required version of linux source files will be copied into this folder
│   │   ├── windows\  # the required version of windows source files will be copied into this folder
│   │   └── __init__.py
│   │
│   ├── secret\  # here you should store confidential info locally
│   │   └── influxdb_template.json
│   │
│   ├── simulation\  # anything related to the grid simulation
│   │   ├── pyiec61850cls.py  # a demo programm for IEC 61850 server using local static data model
│   │   ├── pyiec61850clsDemo.py  # a demo programm for IEC 61850 server using hard-coded data model
│   │   ├── pyiec61850dgm.py  # a demo programm for low voltage grid simulation
│   │   └── __init__.py
│   │
│   ├── tester\
│   │   ├── iedClientTester.py  # tester for the IEC 61850 MMS server
│   │   ├── moduleTester.py  # a test script to check if the import of pylibiec61850 works
│   │   └── runTester.py  # it contains all test functions
│   │
├── tutorial\  # placeholder for tutorials
│   └── README.md
└── README.md
```

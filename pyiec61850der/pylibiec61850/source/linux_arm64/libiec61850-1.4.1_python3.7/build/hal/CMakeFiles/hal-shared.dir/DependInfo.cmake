# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/ethernet/linux/ethernet_linux.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/ethernet/linux/ethernet_linux.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/filesystem/linux/file_provider_linux.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/filesystem/linux/file_provider_linux.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/memory/lib_memory.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/memory/lib_memory.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/serial/linux/serial_port_linux.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/serial/linux/serial_port_linux.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/socket/linux/socket_linux.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/socket/linux/socket_linux.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/thread/linux/thread_linux.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/thread/linux/thread_linux.c.o"
  "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/time/unix/time.c" "/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/CMakeFiles/hal-shared.dir/time/unix/time.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "config"
  "../src/common/inc"
  "../src/goose"
  "../src/sampled_values"
  "../src/hal/inc"
  "../src/iec61850/inc"
  "../src/iec61850/inc_private"
  "../src/mms/inc"
  "../src/mms/inc_private"
  "../src/mms/iso_mms/asn1c"
  "../src/logging"
  "../hal/inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

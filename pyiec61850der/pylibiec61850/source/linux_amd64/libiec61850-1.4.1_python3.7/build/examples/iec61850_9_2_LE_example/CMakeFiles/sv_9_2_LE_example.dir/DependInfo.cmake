# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/work/libiec61850-1.4.1/examples/iec61850_9_2_LE_example/iec61850_9_2_LE_example.c" "/work/libiec61850-1.4.1/build/examples/iec61850_9_2_LE_example/CMakeFiles/sv_9_2_LE_example.dir/iec61850_9_2_LE_example.c.o"
  "/work/libiec61850-1.4.1/examples/iec61850_9_2_LE_example/static_model.c" "/work/libiec61850-1.4.1/build/examples/iec61850_9_2_LE_example/CMakeFiles/sv_9_2_LE_example.dir/static_model.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "config"
  "../src/common/inc"
  "../src/goose"
  "../src/sampled_values"
  "../src/hal/inc"
  "../src/iec61850/inc"
  "../src/iec61850/inc_private"
  "../src/mms/inc"
  "../src/mms/inc_private"
  "../src/mms/iso_mms/asn1c"
  "../src/logging"
  "../hal/inc"
  "../examples/iec61850_9_2_LE_example/."
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/work/libiec61850-1.4.1/build/src/CMakeFiles/iec61850.dir/DependInfo.cmake"
  "/work/libiec61850-1.4.1/build/hal/CMakeFiles/hal.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

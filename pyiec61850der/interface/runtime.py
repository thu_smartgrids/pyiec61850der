# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 12:40:02 2023

@author: chen

TODO: add documentation for the runtime data interface
    - DataBuffer class
    - DA/DO class
    - Profile
    - Statitic
    - Database
"""

import pandas as pd
import logging
from dataclasses import dataclass
import configurator.helper as helper
import interface
import json
import os
from datetime import datetime
from configurator.helper import timeoutChecker

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)


@dataclass()
class Profile(object):
    """
    TODO: add detailed information
    columns of the dfVal should be _time, _value

    Later, each BDA/DA in the IEC 61850 could have a instance profile.

    The profile could be historical measurements, forecasts or predicted values using other techniques.

    Note: not all attributes are always used when an instance is initialized, e.g. local profile doesn't
        imply knowledge about MMS address and guid. If an attributes has value None, most likely it is not
        required for that instance.

    TODO: compare with interface.runtime.DataBuffer(), the profile instance can copy configuration there
    """

    def __init__(self, **kwargs):

        self.GUID = None
        self.dfVal = None
        self.nVal = None
        self.provider = None
        self.profile = None
        self.profileHist = None
        self.profileAll = None
        self.timeWindow = (None, None)  # startTime, endTime
        self.timeWindowUnix = (None, None)

        if kwargs is not None:
            for key, value in kwargs.items():
                setattr(self, key, value)


# TODO: is it correct to put the IEC61850 DO and DA classes here in interface?
# Consider moving them to communication.iec61850Server??

@dataclass()
class IEC61850DA(object):
    """
    IEC 61850 relevant information.
    Similar to the class IEC61850DA, currently we only use this class to store some information.
    The processing logic in processing.routine will not access the IEC61850DA instances,
    but in the future, it could be necessary to go down to the DA level.
    """

    def __init__(self, **kwargs):
        self.level = 'DA'  # BDA/SDA, DA, DO, ...
        self.identifier = None
        self.mmsAddress = None
        self.commAddress = None
        self.mmsAddressDO = None
        self.commAddressDO = None
        self.name = None
        self.type = None
        self.bType = None
        self.FC = None
        self.description = None  # currently not used
        self.refLD = None
        self.refLN = None
        self.refDO = None
        self.refDA = None
        self.GUID = None

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.warning(f'Unknown attribute {key} has been added and set, this may cause inconsistency')
                else:
                    setattr(self, key, value)

    def __str__(self):
        """
        Use the DA identifier to represent the DA object description.
        """
        return f'{self.identifier}'

    def __call__(self):
        """
        Use the DA identifier to represent the DA object when it is called.
        """
        return self.identifier


@dataclass()
class IEC61850DO(object):
    """
    IEC 61850 relevant information.
    Since many DA do not need integrity update, it is not necessary to go down to the DA level.
    So we link the DO with DataBuffer instances.

    But the data class IEC61850DA is still available, in the prototype phase we store all the information
    anyway, so the attribute listDA should consist of many IEC61850DA instances. This might be overengineering.
    But it is easier to remove specific functions than adding them later.

    Without further notion, address means the MMS address of the DO following this scheme:
        <IED_name>_<LD name>/<LN_name>.<DO name>.<DA name>.<SDA name>
    In addition, a communication address is also designed following this scheme:
        <IED_name>$<LD name>$<LN_name>$<DO name>$<DA name>$<SDA name>
    The communication address is a backup plan in case some applicaiton can not process
    the separators (_, / or .) properly.

    TODO: evaluate whether this is a good concept
    The following 4 attributes determine the primay monitoring and control parameter of a DO.
        - self.monitorAddressDA
        - self.controlAddressDA
        - self.monitorRefDA
        - self.controlRefDA
    With their help the processing functions will not need to access the DA at a lower lever, instead they can
        just use the these addresses and references to perform data reading and control.

    TODO: there are many things to be specified and modified. Here are some of the
        most important notes:
            - currently the DataBuffer is linked onto the DO level, so each instance of
                DataBuffer must contain one instance of this class IEC61850DO
            - to reflect the DAs of a DO, we use the two lists listNameDA and listRefDA
            - But, a DA can also have more child-objects, this is currently not being handelled
            - to determine the monitor and control DA, some hard coding logics are necessary
    """

    def __init__(self, **kwargs):
        self.level = 'DO'  # BDA/SDA, DA, DO, ...
        self.identifier = None  # currently the same as mmsAddress, this attribute could be used for other type of identifier
        self.mmsAddress = None
        self.commAddress = None
        self.name = None
        self.type = None
        self.GUID = None
        self.CDC = None
        self.FCDA = None
        self.description = None  # currently not used
        self.refLD = None
        self.refLN = None
        self.refDO = None

        # attributes that are relevant for the DA of associated DO
        self.listNameDA = None  # a lost of DA names
        self.listRefDA = None  # a list of DA references
        self.listMmsAddressDA = None  # a list of the DA MMS addresses
        self.listDA = None  # a list of DA object instances of the class IEC61850DA

        self.monitorAddressDA = None  # maximal 1 DA for the monitoring direction
        self.controlAddressDA = None  # maximal 1 DA for the control direction
        self.monitorRefDA = None  # maximal 1 DA for the monitoring direction
        self.controlRefDA = None  # maximal 1 DA for the control direction
        self.qualityRefDA = None  # q of the DA
        self.tstmpRefDA = None  # t of the DA

        self.idxMonitorDA = None
        self.idxControlDA = None

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.warning(
                        f'Unknown attribute {key} has been added and set, attention, this may cause inconsistency')
                else:
                    setattr(self, key, value)

    def __str__(self):
        """
        Use the DO identifier to represent the DO object description.
        """
        return f'{self.identifier}'

    def __call__(self):
        """
        Use the DO identifier to represent the DO object when it is called.
        """
        return self.identifier

    def addIec61850DA(self, newDA=None):
        """
        Create a new IEC61850DA instance and update the relevant attributes in IEC61850DO
        accordingly.
        """

        if self.listDA is None:
            self.listDA = []
            self.listNameDA = []
            self.listRefDA = []
            self.listMmsAddressDA = []

        self.listDA.append(newDA)
        self.listNameDA.append(newDA.name)
        self.listRefDA.append(newDA.refDA)
        self.listMmsAddressDA.append(newDA.mmsAddress)

    def selectPrimaryDA(self):
        """
        It should be clear that each IEC 61850 DO would contain a bunch of different DA, but in many cases one of them would
        be the most frequently used parameter for monitoring and control. Therefore, this method will be called after all DA
        instances of the class IEC61850DA are initialized, it selects one (if there exists any) primary DA for the monitoring
        as well as control direction, note the address and reference object and add them to the lists in the DataBuffer instances.

        These primary DA ref is only a short cut for simply application. For complexer application you may consider get a deeper
        insight into the IEC61850DA instances.

        Theoretically one can dynamically assign primary DA names based on CDC type, but for this prototype we go wild by using
        a fix coded dictionary.

        Mapping is defined as following:
            CDC Type		monitoring DA	 	control DA
            -----------------------------------------------
            APC				mxVal.f				ctlVal.f
            ASG				setMag.f			setMag.f
            BCR				actVal				--
            CMV				cVal.mag.f			--
            CSG				--					--
            CUG				--					--
            DEL				phsAB.cVal.mag.f	--				(Currently take phaseAB)
            DPL				--					--
            ENC				stVal				ctlVal
            ENG				setVal				setVal
            ENS				stVal				--
            LPL				--					--
            INC				stVal				ctlVal
            ING				setVal				setVal
            INS				stVal				--
            MV				mag.f				--
            ORG				setSrcRef			setSrcRef
            ORS				stVal				--
            SPC				stVal				ctlVal
            SPG				setVal				setVal
            SPS				stVal				--
            TCS				stVal				--
            TSG				setTm				setTm
            VSG				setVal				setVal
            WYE				phsA.cVal.mag.f		--				(Currently take phaseA)
        """

        dictPrimaryDA = dict({
            'APC': {'monitor': 'mxVal.f', 'control': 'ctlVal.f'},
            'ASG': {'monitor': 'setMag.f', 'control': 'setMag.f'},
            'BCR': {'monitor': 'actVal', 'control': None},
            'CMV': {'monitor': 'cVal.mag.f', 'control': None},
            'CSG': {'monitor': None, 'control': None},
            'CUG': {'monitor': None, 'control': None},
            'DEL': {'monitor': 'phsAB.cVal.mag.f', 'control': None},
            'DPL': {'monitor': None, 'control': None},
            'ENC': {'monitor': 'stVal', 'control': 'ctlVal'},
            'ENG': {'monitor': 'setVal', 'control': 'setVal'},
            'ENS': {'monitor': 'stVal', 'control': None},
            'LPL': {'monitor': None, 'control': None},
            'INC': {'monitor': 'stVal', 'control': 'ctlVal'},
            'ING': {'monitor': 'setVal', 'control': 'setVal'},
            'INS': {'monitor': 'stVal', 'control': None},
            'MV': {'monitor': 'mag.f', 'control': None},
            'ORG': {'monitor': 'setSrcRef', 'control': 'setSrcRef'},
            'ORS': {'monitor': 'stVal', 'control': None},
            'SPC': {'monitor': 'stVal', 'control': 'ctlVal'},
            'SPG': {'monitor': 'setVal', 'control': 'setVal'},
            'SPS': {'monitor': 'stVal', 'control': None},
            'TCS': {'monitor': 'stVal', 'control': None},
            'TSG': {'monitor': 'setTm', 'control': 'setTm'},
            'VSG': {'monitor': 'setVal', 'control': 'setVal'},
            'WYE': {'monitor': 'phsA.cVal.mag.f', 'control': None},
        })

        if self.listDA is None:
            logger.info(f'There is no DA in the list of DO {self.name}, can not proceed.')
        elif self.CDC not in dictPrimaryDA:
            logger.info(f'The CDC type {self.CDC} is not yet supported.')
        else:
            if dictPrimaryDA[self.CDC]['monitor'] is not None:
                identiferMonitor = f'{self.mmsAddress}.{dictPrimaryDA[self.CDC]["monitor"]}'
                if identiferMonitor in self.listMmsAddressDA:
                    self.monitorAddressDA = identiferMonitor
                    self.idxMonitorDA = \
                    [idx for idx in range(len(self.listDA)) if self.listDA[idx].mmsAddress == identiferMonitor][0]
                    self.FCDA = self.listDA[self.idxMonitorDA].FC
                    self.monitorRefDA = self.listDA[self.idxMonitorDA].refDA
                else:
                    logger.info(f'The monitor DA with identifer {identiferMonitor} not found in the DA list.')

                # only monitoring DA can provide info about q and t
                if 'q' in self.listNameDA:
                    self.qualityRefDA = [DA.refDA for DA in self.listDA if DA.name == 'q'][0]

                if 't' in self.listNameDA:
                    self.tstmpRefDA = [DA.refDA for DA in self.listDA if DA.name == 't'][0]

            if dictPrimaryDA[self.CDC]['control'] is not None:
                identiferControl = f'{self.mmsAddress}.{dictPrimaryDA[self.CDC]["control"]}'
                if identiferControl in self.listMmsAddressDA:
                    self.controlAddressDA = identiferControl
                    self.idxControlDA = \
                    [idx for idx in range(len(self.listDA)) if self.listDA[idx].mmsAddress == identiferControl][0]
                    self.controlRefDA = self.listDA[self.idxControlDA].refDA
                else:
                    logger.info(f'The control DA with identifer {identiferControl} not found in the DA list.')


@dataclass()
class Statistic(object):
    """
    Statistical relevant information.
    TODO: add more desc
    """

    def __init__(self, **kwargs):
        # TODO: make the list Column dynamic??
        self.listColumn = ['t', 'tstmpUnix', 'q', 'valueIEC61850', 'valueSource', 'nIntegrityUpdate', 'nControl']
        self.nIntegrityUpdate = 1
        self.nQualityChange = 0
        self.nDataUpdate = 0
        self.nControl = 0
        self.dfHistoric = pd.DataFrame(columns=self.listColumn)
        self.dfUpload = pd.DataFrame(columns=self.listColumn)
        self.listTstmp = None
        self.listVal = None
        self.timeCreate = 'unknown'

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.warning(f'Unknown attribute {key} has been added and set, this may cause inconsistency')
                else:
                    setattr(self, key, value)


class Database(object):
    """
    Information related to database handlers
    TODO: add more desc
    """

    def __init__(self, **kwargs):
        self.name = None
        self.type = None
        self.queryHandler = None  # currently not used by influxdb, do not get confused.
        self.writeHandler = None  # currently not used by influxdb, do not get confused.
        self.connStatusQuery = None
        self.connStatusWrite = None
        self.identifier = None
        self.GUID = None
        self.bucketRead = None  # this is a name convention intuited by influxdb
        self.bucketWrite = None  # this is a name convention intuited by influxdb

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.info(f'Unknown attribute {key} has been added and set, this may cause inconsistency')
                else:
                    setattr(self, key, value)


@dataclass()
class DataBuffer(object):
    """
    An inner-class containing properties that specify the data interfaces between the IED server and other data
    service providers, such as timeseries database, metadata database, SCADA and the simulation environment.

    This data class is very important! It is the interface between the runtime environment and the simulated IEC 61850
    services. It connects the IEC 61850 application to the data pool, local DER units and controller interfaces. It passes
    the measurements collected during the simulation to the IED server and react to the control commands performed by the
    IED clients.

    After an DataBuffer instance has been initialised (most likely by the service module communication.iec61850Server),
    it is barely empty, it contains only the default setup except that some specific configurations are passed by kwargs.
    While the configuration of data points are stored in the CSV lookup table. The user specified configuration  will be
    transfered to the attributes of DataBuffer instances by the function communication.iec61850Server.readLookupCSV(listReadColumn).

    So the attributes here should contain required information for:
        - detailed IEC 61850 description (MMS address, parent DO)
        - current values of the parameter, both on the server and client sides
        - placeholder for profiles, if load/gen profile or prediction is available,
        - time series database connction (for profiles), i.e. the data entries in a database and database handlers
        - metadb connection (for registration), i.e. the data entries in a database and database handlers

    TODO: consolidate the attributes for DO databuffer

    TODO: many of the follwing attributes should later be converted to IEC 61850 parameters

    TODO: check whether the data processing logics associated with the class DataBuffer is overengineering

    TODO: add description of dataSource:
        - disabled
        - random
        - local
        - influxdb
        - processed
        - DER
        - ... ...
    """

    '''
    TODO: some thoughts here
        - lowest level is DO DataBuffer
        - generate listDataBuffer only for DO or also for DA?
        - write d, q, t, units as attributes?
        - Use IEC server shadow?
        - or initialize for each DataBuffer a sub-databuffer for the DAs?
        - or should we handle the DO by CDC type?
    '''

    def __init__(self, **kwargs):
        self.index = None
        self.dataSource = 'disabled'
        self.identifier = None  # currently use the IEC 61850 MMS address as identifier
        self.GUID = None
        self.containerGUID = None
        self.containerName = None

        # IEC 61850
        self.DO = IEC61850DO()

        # measurement and control values
        self.valueSource = None
        self.valueIEC61850 = None
        self.isMonitor = False
        self.isControl = False
        self.limit = dict({'lower': -999999999, 'upper': 999999999})  # now make it a mutable list instead of tuple for runtime processing
        self.valueRated = None
        self.unitFactor = 1  # abandon unitFactor as it is a type of scaleFactor??
        self.scaleFactor = 1
        self.unit = None

        # profile
        self.profileLocal = Profile()
        self.profileInfluxdb = Profile()

        # statistics
        # TODO: the name "statistic" is kind of confusing here, originally it was indeed a DataFrame storing some
        #  statistical information, later it was used as data archive but the name remained unchanged. Find a way to
        #  rule out this confusion...
        self.statistics = Statistic()
        self.statistics.timeCreate = 'timeCreate_' + datetime.today().strftime('%Y%m%d%H%M')

        # database interface
        self.influxdb = Database()
        self.metadb = Database()  # place holder for metadata database, which is relevant for the registration

        # communication connection object
        self.configJSON = None  # intermediate storage for the config JSON string
        self.connObj = None  # This could be used to specify local communication, e.g. for Sunspec interface

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.warning(f'Unknown attribute {key} has been added and set, this may cause inconsistency')
                else:
                    setattr(self, key, value)

    def __str__(self):
        """
        Use the DataBuffer identifier to represent the DataBuffer object description.
        """
        return f'{self.identifier}'

    def __call__(self):
        """
        Use the DataBuffer identifier to represent the DataBuffer object when it is called.
        """
        return self.identifier

    def updateAttribute(self, **kwargs):
        """
        TODO: more details here.
        This method taks a dictionary **kwargs as input parameter, parse the parameters stored in the
        dict and update the attributes of the DataBuffer instances accordingly. Of course you can also
        pass single kwargs without adding the **.


        In the next step it adds a new instance to the list listDataBuffer. This method could be used
        iteratively when creating a large IEC 61850 data model and initializing its IEC 61850 server.
        """

        if kwargs is not None:
            for key, value in kwargs.items():
                if key not in dir(self):
                    setattr(self, key, value)
                    logger.warning(f'Unknown attribute {key} has been added and set, this may cause inconsistency')
                else:
                    setattr(self, key, value)

    def updateStatistics(self, dfNew=None):
        """
        This method is called during each iteration of the IEC 61850 server, it writes the last valid value of active
        DO into the dataBuffer.statistics attribute.
        """

        try:
            self.statistics.dfHistoric = pd.concat([self.statistics.dfHistoric, dfNew], ignore_index=True)
            self.statistics.dfUpload = pd.concat([self.statistics.dfUpload, dfNew], ignore_index=True)
            self.statistics.nIntegrityUpdate += 1
        except:
            logger.exception('Unknown error occurred when add new Dataframe row to the statistic attributes.')

    @timeoutChecker(60)
    def uploadStatisticsToInfluxdb(self, bucket=None, measurement=None, **kwargs):
        """
        This methods uploads the most latest measurements to the pre-configured influxdb regularly.
        It requires the influxdb write handler.

        Use kwargs to specify additional tags
        """

        if self.influxdb.clientInfluxdbWrite is None:
            pass
        # logger.warning(f'Influxdb write handler for DO {self.DO.name} does not exist, can not upload data')
        elif self.influxdb.clientInfluxdbWrite.health().status != 'pass':
            logger.warning(f'Influxdb write handler for DO {self.DO.name} has a bad connection')
            # TODO: implement a keep-alive or reconnecting mechanism later
        else:
            listKeys = list(self.statistics.dfUpload.keys())
            # reset index, otherwise big problem with mulitple 0 index
            self.statistics.dfUpload = self.statistics.dfUpload.reset_index()
            listRecords = []
            identifier = self.DO.monitorAddressDA

            try:
                for idx in range(len(self.statistics.dfUpload)):
                    recordMetrics = {}
                    fields = {}
                    tags = {}
                    recordMetrics['measurement'] = measurement
                    tags['LD'] = identifier.split('/')[0].split('_')[1]
                    tags['LN'] = identifier.split('/')[1].split('.')[0]
                    tags['DO'] = self.DO.name
                    tags['DA'] = self.DO.listDA[self.DO.idxMonitorDA].name
                    tags['CDC'] = self.DO.CDC
                    tags['FC'] = self.DO.FCDA
                    tags['GUID'] = self.containerGUID
                    tags['name'] = self.containerName

                    for key, item in kwargs:
                        tags[key] = item
                    recordMetrics['tags'] = tags

                    for key in listKeys:
                        if key == 't':
                            # TODO: this time format conversion could cause error! find a solution to deal with it.
                            # always use UTC time
                            recordMetrics['time'] = helper.convertTimeStrFormat(self.statistics.dfUpload.loc[idx, key],
                                                                                '%Y-%m-%d %H:%M:%S',
                                                                                '%Y-%m-%dT%H:%M:%S.%fZ')

                        else:
                            val = self.statistics.dfUpload.loc[idx, key]
                            if helper.isValNumber(val):
                                val = float(val)
                                # TODO: to avoid upload error, convert all numbers to float, find a solution for this.
                                fields[key] = val
                    recordMetrics['fields'] = fields

                    listRecords.append(recordMetrics)

                if listRecords != []:
                    interface.influxdb.writeDataToInfluxdb(self.influxdb.clientInfluxdbWrite, listRecords,
                                                           self.influxdb.bucketWrite)

                self.statistics.dfUpload = pd.DataFrame(columns=self.statistics.listColumn)
            except:
                logger.exception(f'Upload to influxdb for DO {self.DO.name} failed, wait for the next iteration.')

    def exportStatisticsLocal(self):
        """
        This method exports the Attribute dataBuffer.statistics to a local csv file. The attribute is a pandas DataFrame
        and the file name is its DO.identifier, the slash / is replaced by underscore _.

        It only take dataBuffer instances with isMonitor == True into account.
        """

        if self.isMonitor:
            if not os.path.isdir(f'./data/archive/{self.statistics.timeCreate}'):
                os.mkdir(f'./data/archive/{self.statistics.timeCreate}')
            filepath = f'./data/archive/{self.statistics.timeCreate}/{self.DO.identifier.replace("/", "_")}.csv'
            try:
                self.statistics.dfHistoric.to_csv(filepath)
            except PermissionError:
                logger.exception('Can not write the export CSV file, probably the file is opened!')
            except:
                logger.exception('Unknown error occurred when export CSV, wait for the next trigger in 15 minutes.')


def createStaticListDataBuffer(iedServer=None, listMmsAddressDO=None, listInitVal=None, **kwargs):
    """
    Initialize the data buffer list using pre-configured lists. This function only works on some demo, simply, static
    IEC 61850 data models for test purpose. It only works for MX value types. Most of the attributes will be left empty,
    therefore DO NOT consider using this function for advanced applications!!

    As long as the IEC 61850 data model gets bigger, working with lists will no longer be pythonic. In that case please
    turn to use the method interface.iec61850Server.createDataBuffer interatively during the initialization of the data model
    and IEC 61850 MMS server.

    The first list listAddressMMS is obligatory, it should contain the MMS address (or DA identifier)
    of each associated DA, one example is 'demoVirtualCLS_demoProsumerXY/PV1_MMXU0.TotW.mag.f'
    The second list listInitVal is optional.

    """

    iedServer.listDataBuffer = []

    if len(listMmsAddressDO) != len(listInitVal):
        logger.warning('The list of MMS addresses and initial values have different length')

    for idx, item in enumerate(listMmsAddressDO):
        newDataBuffer = DataBuffer()
        # index will be written when creating the IED server
        newDataBuffer.DO.identifier = item
        newDataBuffer.identifier = item
        newDataBuffer.containerGUID = iedServer.containerGUID
        newDataBuffer.containerName = iedServer.containerName

        if listInitVal is not None:
            newDataBuffer.valueSource = listInitVal[idx]
            newDataBuffer.valueIEC61850 = listInitVal[idx]

        iedServer.listDataBuffer.append(newDataBuffer)

    iedServer.nDataBuffer = len(iedServer.listDataBuffer)

    iedServer.listMonitorRef = [None] * iedServer.nDataBuffer
    iedServer.listControlRef = [None] * iedServer.nDataBuffer
    iedServer.listMonitorAddress = [None] * iedServer.nDataBuffer
    iedServer.listControlAddress = [None] * iedServer.nDataBuffer
    iedServer.listQualityRef = [None] * iedServer.nDataBuffer
    iedServer.listTstmpRef = [None] * iedServer.nDataBuffer

    if kwargs is not None:
        for key, value in kwargs.items():
            setattr(iedServer, key, value)

    # # some attributes that are only required by the demo server
    # iedServer.container_pvsys = []
    # iedServer.container_all_objects = []
    # iedServer.dictUpdateVal = []
    # iedServer.iedModel = None









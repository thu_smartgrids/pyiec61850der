# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/work/libiec61850-1.5.1/examples/goose_subscriber/goose_subscriber_example.c" "/work/libiec61850-1.5.1/build/examples/goose_subscriber/CMakeFiles/goose_subscriber_example.dir/goose_subscriber_example.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "config"
  "../src/common/inc"
  "../src/goose"
  "../src/sampled_values"
  "../src/hal/inc"
  "../src/iec61850/inc"
  "../src/iec61850/inc_private"
  "../src/mms/inc"
  "../src/mms/inc_private"
  "../src/mms/iso_mms/asn1c"
  "../src/logging"
  "../hal/inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/work/libiec61850-1.5.1/build/src/CMakeFiles/iec61850.dir/DependInfo.cmake"
  "/work/libiec61850-1.5.1/build/hal/CMakeFiles/hal.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

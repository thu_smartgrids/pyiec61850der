# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 12:31:24 2024

@author: chen
"""

import seaborn as sns
import matplotlib.pyplot as plt
import copy
import pandas as pd
import numpy as np

sns.set_style("whitegrid")
sns.set(font_scale = 2)

def plotPrediction(dfMG, dirResult):    
    # compare actual measurement and prediction
    fig = plt.figure(figsize=(16,12), dpi=300)
    sns.set_style("whitegrid")
    sns.set(font_scale = 2)
    dfPlot = dfMG[['Pgen', 'Pgen_pred', 'Pload', 'Pload_pred']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Overview of the "measured" data and "predicted" data', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Active power consumption/generation [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/1_data_overview.png')


    
def plotReference(dfMG, dirResult):    
    fig = plt.figure(figsize=(16,8), dpi=600)
    # plt.style.use("whitegrid")
    dfPlot = dfMG[['Pnet', 'Pnet_pred', 'Pref']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    # ax.set_title('Comparison of "measured" net consumption and reference trajectories', fontsize=32)
    ax.set_xlabel('Minute index in one day', fontsize=32)
    ax.set_ylabel('Aggreated net power demand [kW]', fontsize=32)
    ax.set_xlim([0,1440])
    ax.set_ylim([-900,700])
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    plt.show()
    fig.savefig(f'{dirResult}/2_net_consumption_vs_reference.png')
    
    fig = plt.figure(figsize=(16,12), dpi=300)
    # plt.style.use("whitegrid")
    dfPlot = dfMG[['Pgen', 'Pload', 'Pref']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Comparison of "measured" gen and load and reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Comparison of gen and load with reference trajectory[kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/2.1_gen_load_vs_reference.png')
      
    fig = plt.figure(figsize=(16,12), dpi=300)
    # plt.style.use("whitegrid")
    dfPlot = dfMG[['Pgen_pred', 'Pload_pred', 'Pref']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Comparison of "prediction" and reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Comparison of prediction with reference trajectory[kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/2.2_prediction_vs_reference.png')
    
    
def plotBenchmark(dfMGBenchmark, dirResult):
    # benchmark deviation compared to Pref
    fig = plt.figure(figsize=(16,12), dpi=300)
    dfPlot = dfMGBenchmark[['Pdeviation', 'Pdeviation_noControl']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Deviation compared to the reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Deviation of aggreated net active power [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/3_benchmark_deviation.png')
    
    
    fig, ax = plt.subplots(figsize=(16,12), dpi=300)
    sns.set_style("whitegrid")
    sns.set(font_scale = 2)
    dfPlot = dfMGBenchmark[['Pnet', 'Pref']]
    dfControl = dfMGBenchmark[['meanControl']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Comparison of the aggregated net consumption', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Aggreated net active power [kW]', fontsize=32)
    
    ax.legend(loc='upper left')
    ax2 = ax.twinx()
    ax2 = sns.lineplot(data=dfControl, linestyle=':', lw=10, palette=['r'], legend=False)
    ax2.set_ylabel('Average PV power limit of the inverter [kW]', fontsize=32)
    ax2.set_ylim([0, max(dfMGBenchmark['P_control'])])
    plt.show()
    fig.savefig(f'{dirResult}/4_benchmark.png')


def plotResult(listResult, listDfNode, listProcessingTime, dfMG, dfMGBenchmark, dirResult):
    
    # niter and processing time
    dfResult = pd.DataFrame()
    for idx, res in enumerate(listResult):
        for key in res.keys():
            if type(res[key]) in [int, float]:
                dfResult.loc[idx, key] = res[key]
    if 'execution_time' not in dfResult.keys():
        dfResult['execution_time'] = np.array(listProcessingTime)
    # dfResult.to_csv(f'{dirResult}/dfResult.csv')
    
    # for i in range(len(listDfNode)):
    #     listDfNode[i].to_csv(f'{dirResult}/node_{i}.csv')   
        
    fig = plt.figure(figsize=(16,12), dpi=300)
    ax = sns.lineplot(data=dfResult['nit'], linestyle='-',color='b', lw=5, legend='full')
    ax.set_title('Computation time using algorithm SLSQP', fontsize=32)
    ax.set_ylim((0,120))
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Number of iterations', fontsize=32)
    ax2 = ax.twinx()
    ax2 = sns.lineplot(data=dfResult['execution_time'], linestyle='-', lw=5, color='r', legend='full')
    ax2.set_ylabel('Processing time [s]', fontsize=32)
    ax.legend(['Number of iterations'], loc=(0.65, 0.85))
    ax2.legend(['processing time'], loc=(0.65, 0.8))
    plt.show()
    fig.savefig(f'{dirResult}/5_opt_niter_processingTime.png')

    # # comparison of Pref and Pnet
    # fig = plt.figure(figsize=(16,12), dpi=300)
    # sns.set_style("darkgrid")
    # sns.set(font_scale = 2)
    # dfPlot = dfMG[['Pnet', 'Pref']]
    # dfControl = dfMG[['meanControl']]
    # ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    # ax.set_title('Comparison of "measured" net consumption and reference trajectories', fontsize=32)
    # ax.set_xlabel('timestamp indexes', fontsize=32)
    # ax.set_ylabel('Aggreated net active power consumption [kW]', fontsize=32)
    # plt.show()
    # fig.savefig(f'{dirResult}/6_MPC_net_consumption_vs_reference.png')


    # deviation compared to Pref
    fig = plt.figure(figsize=(16,12), dpi=300)
    #dfPlot = dfMG[['Pdeviation', 'Pdeviation_accurate_pred', 'Pdeviation_noControl']]
    dfPlot = dfMG[['Pdeviation']]
    dfPlot['Pdeviation_benchmark'] = dfMGBenchmark['Pdeviation']
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('SLSQP Deviation compared to the reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Deviation of aggreated net active power [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/6_opt_deviation.png')
    
    
    # status parameter
    fig, ax = plt.subplots(figsize=(16,12), dpi=300)
    # dfPlot = dfMG[['Pnet', 'Pnet_accurate_pred', 'Pref']]
    dfPlot = dfMG[['Pnet', 'Pref']]
    dfPlot['Pnet_benchmark'] = dfMGBenchmark['Pnet']
    dfState= dfMG[['meanControl']]
    ax = sns.lineplot(data=dfPlot, linestyle='-', lw=5)
    ax.set_title('Comparison of the aggregated net consumption', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Aggreated net active power [kW]', fontsize=32)
    ax.legend(loc='upper left')
    ax2 = ax.twinx()
    ax2.set_ylim([0, 15])
    ax2 = sns.lineplot(data=dfState, linestyle=':', lw=10, palette=['r'], legend=False)
    ax2.set_ylabel('Average PV curtailment [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirResult}/7_opt_curtailment.png')


def plotComparisonMPC(dirProject):
    pathBM_max = f'{dirProject}/I_100_benchmark_max/dfMG_result.csv'
    pathBM_flex = f'{dirProject}/I_100_benchmark_flex/dfMG_result.csv'
    pathMPC_SLSQP_3 = f'{dirProject}/I_100_N_3_SLSQP/dfMG_result.csv'
    pathMPC_SLSQP_5 = f'{dirProject}/I_100_N_5_SLSQP/dfMG_result.csv'
    # pathMPC_SLSQP_10 = f'{dirProject}/I_100_N_10_SLSQP/dfMG_result.csv'
    pathMPC_TC_3 =  f'{dirProject}/I_100_N_3_trust-constr/dfMG_result.csv'
    pathMPC_TC_5 =  f'{dirProject}/I_100_N_5_trust-constr/dfMG_result.csv'
    # pathMPC_TC_10 =  f'{dirProject}/I_100_N_10_trust-constr/dfMG_result.csv'
    
    dfBM_max = pd.read_csv(pathBM_max)
    dfBM_flex = pd.read_csv(pathBM_flex)
    dfMPC_SLSQP_3 = pd.read_csv(pathMPC_SLSQP_3)
    dfMPC_SLSQP_5 = pd.read_csv(pathMPC_SLSQP_5)
    # dfMPC_SLSQP_10 = pd.read_csv(pathMPC_SLSQP_10)
    dfMPC_TC_3 = pd.read_csv(pathMPC_TC_3)
    dfMPC_TC_5 = pd.read_csv(pathMPC_TC_5)
    # dfMPC_TC_10 = pd.read_csv(pathMPC_TC_10)
    
    # deviation compared to Pref
    dfPlot = pd.DataFrame()
    dfPlot['BM_max'] = dfBM_max['Pdeviation']
    dfPlot['BM_flex'] = dfBM_flex['Pdeviation']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pdeviation']
    dfPlot['MPC_SLSQP_5'] = dfMPC_SLSQP_5['Pdeviation']
    # dfPlot['MPC_SLSQP_10'] = dfMPC_SLSQP_10['Pdeviation']
    dfPlot['MPC_TC_3'] = dfMPC_TC_3['Pdeviation']
    dfPlot['MPC_TC_5'] = dfMPC_TC_5['Pdeviation']
    # dfPlot['MPC_TC_10'] = dfMPC_TC_10['Pdeviation']
    
    fig = plt.figure(figsize=(16,12), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[400:1100], linestyle='-', lw=3, dashes=False)
    ax.set_xlim([400,1100])
    ax.set_title('Deviation compared to the reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Deviation of aggreated net active power [kW]', fontsize=32)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    plt.show()
    fig.savefig(f'{dirProject}/opt_deviation.png')
    
    
    dfPlot = pd.DataFrame()
    dfPlot['Pref'] = dfBM_max['Pref']
    dfPlot['BM_max'] = dfBM_max['Pnet']
    dfPlot['BM_flex'] = dfBM_flex['Pnet']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pnet']
    dfPlot['MPC_SLSQP_5'] = dfMPC_SLSQP_5['Pnet']
    # dfPlot['MPC_SLSQP_10'] = dfMPC_SLSQP_10['Pnet']
    dfPlot['MPC_TC_3'] = dfMPC_TC_3['Pnet']
    dfPlot['MPC_TC_5'] = dfMPC_TC_5['Pnet']
    # dfPlot['MPC_TC_10'] = dfMPC_TC_10['Pnet']
    
    fig = plt.figure(figsize=(16,12), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[400:1100], linestyle='-', lw=5, dashes=False)
    ax.set_xlim([400,1100])
    ax.set_title('Comparison of the aggregated net consumption', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Aggreated net active power [kW]', fontsize=32)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    # ax.legend(loc='upper left')
    # ax2 = ax.twinx()
    # ax2.set_ylim([0, max(dfMG['P_control'])])
    # ax2 = sns.lineplot(data=dfState, linestyle=':', lw=10, palette=['r'], legend=False)
    # ax2.set_ylabel('Average PV curtailment [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirProject}/opt_Pnet_comp.png')
    
    
    
    dfBM_max['meanControl_percent']  = dfBM_max['meanControl']/dfBM_max['P_max_mean']*100
    dfBM_flex['meanControl_percent']  = dfBM_flex['meanControl']/dfBM_flex['P_max_mean']*100
    dfPlot = pd.DataFrame()
    dfPlot['BM_max'] = dfBM_max['meanControl_percent']
    dfPlot['BM_flex'] = dfBM_flex['meanControl_percent']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['meanControl_percent']
    dfPlot['MPC_SLSQP_5'] = dfMPC_SLSQP_5['meanControl_percent']
    # dfPlot['MPC_SLSQP_10'] = dfMPC_SLSQP_10['meanControl_percent']
    dfPlot['MPC_TC_3'] = dfMPC_TC_3['meanControl_percent']
    dfPlot['MPC_TC_5'] = dfMPC_TC_5['meanControl_percent']
    # dfPlot['MPC_TC_10'] = dfMPC_TC_10['meanControl_percent']
    
    fig = plt.figure(figsize=(16,12), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[400:1100], linestyle='-', lw=5, dashes=False)
    ax.set_xlim([400,1100])
    ax.set_ylim([0,100])
    ax.set_title('Comparison of PV curtailment', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Average value of curtailment setpoint [%]', fontsize=32)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    # ax.legend(loc='upper left')
    # ax2 = ax.twinx()
    # ax2.set_ylim([0, max(dfMG['P_control'])])
    # ax2 = sns.lineplot(data=dfState, linestyle=':', lw=10, palette=['r'], legend=False)
    # ax2.set_ylabel('Average PV curtailment [kW]', fontsize=32)
    plt.show()
    fig.savefig(f'{dirProject}/opt_curtailment_comp.png')
    
    
    dfPlot = pd.DataFrame()
    dfPlot['tstmp'] = dfBM_max['tstmp']
    dfPlot['BM_max'] = dfBM_max['Pdeviation']
    dfPlot['BM_flex'] = dfBM_flex['Pdeviation']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pdeviation']
    dfPlot['MPC_SLSQP_5'] = dfMPC_SLSQP_5['Pdeviation']
    # dfPlot['MPC_SLSQP_10'] = dfMPC_SLSQP_10['Pdeviation']
    dfPlot['MPC_TC_3'] = dfMPC_TC_3['Pdeviation']
    dfPlot['MPC_TC_5'] = dfMPC_TC_5['Pdeviation']
    # dfPlot['MPC_TC_10'] = dfMPC_TC_10['Pdeviation']#
    
    fig = plt.figure(figsize=(20,12), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.boxplot(data=dfPlot[480:1080], showfliers=False, whis=3, color=".8",  width=.5)
    ax.set_ylabel('Deviation to Pref [kW]', fontsize=32)
    ax.set_ylim([-250,150])
    plt.xticks(rotation=75, fontsize=24)
    plt.show()
    fig.savefig(f'{dirProject}/error_analysis.png')


def plotComparisonDocker(dirProject):
    pathBM_max = f'{dirProject}/I_100_benchmark_max/dfMG_result.csv'
    pathBM_flex = f'{dirProject}/I_100_benchmark_flex/dfMG_result.csv'
    pathMPC_SLSQP_3 = f'{dirProject}/I_100_N_3_SLSQP/dfMG_result.csv'
    pathMPC_SLSQP_3_docker = f'{dirProject}/I_100_N_3_SLSQP_docker/dfMG_result.csv'
    pathMPC_TC_3 =  f'{dirProject}/I_100_N_3_trust-constr/dfMG_result.csv'

    dfBM_max = pd.read_csv(pathBM_max)
    dfBM_flex = pd.read_csv(pathBM_flex)
    dfMPC_SLSQP_3 = pd.read_csv(pathMPC_SLSQP_3)
    dfMPC_SLSQP_3_docker = pd.read_csv(pathMPC_SLSQP_3_docker)
    dfMPC_TC_3 = pd.read_csv(pathMPC_TC_3)
    
    # deviation compared to Pref
    
    
    dfPlot = pd.DataFrame()
    dfPlot['BM_max'] = dfBM_max['Pdeviation']
    dfPlot['BM_flex'] = dfBM_flex['Pdeviation']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pdeviation']
    dfPlot['MPC_SLSQP_3_docker'] = dfMPC_SLSQP_3_docker['Pdeviation']
    
    fig = plt.figure(figsize=(16,12), dpi=300)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[480:1020], linestyle='-', lw=3, dashes=False)
    ax.set_xlim([400,1100])
    ax.set_title('Deviation compared to the reference trajectories', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Deviation of aggreated net active power [kW]', fontsize=32)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    
    plt.show()
    fig.savefig(f'{dirProject}/opt_deviation_docker.png')


    dfPlot = pd.DataFrame()
    dfPlot['Pref'] = dfBM_max['Pref']
    dfPlot['BM_max'] = dfBM_max['Pnet']
    dfPlot['BM_flex'] = dfBM_flex['Pnet']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pnet']
    dfPlot['MPC_SLSQP_3_docker'] = dfMPC_SLSQP_3_docker['Pnet']
    
    fig = plt.figure(figsize=(16,8), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[480:1020], lw=5, dashes=False, 
                      color=['g', 'lightblue', 'orange', 'purple', 'red'])
    ax.lines[0].set_linestyle("-")
    ax.lines[1].set_linestyle("--")
    ax.lines[2].set_linestyle("--")
    ax.lines[3].set_linestyle("dotted")
    ax.lines[4].set_linestyle("dotted")
    
    ax.set_xlim([400,1100])
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Aggreated net active power [kW]', fontsize=32)
    legend = plt.legend([line for line in ax.lines],
                        ['P_ref', 'BM_max', 'BM_flex', 'MPC_SLSQP_3', 'MPC_SLSQP_docker'],frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    plt.show()
    fig.savefig(f'{dirProject}/opt_Pnet_comp_docker.png')
    
    
    
    dfBM_max['meanControl_percent']  = dfBM_max['meanControl']/dfBM_max['P_max_mean']*100
    dfBM_flex['meanControl_percent']  = dfBM_flex['meanControl']/dfBM_flex['P_max_mean']*100
    dfPlot = pd.DataFrame()
    dfPlot['BM_max'] = dfBM_max['meanControl_percent']
    dfPlot['BM_flex'] = dfBM_flex['meanControl_percent']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['meanControl_percent']
    dfPlot['MPC_SLSQP_3_docker'] = dfMPC_SLSQP_3_docker['meanControl_percent']
    
    fig = plt.figure(figsize=(16,12), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.lineplot(data=dfPlot[480:1020], linestyle='-', lw=5, dashes=False)
    ax.set_xlim([400,1100])
    ax.set_ylim([0,100])
    ax.set_title('Comparison of PV curtailment', fontsize=32)
    ax.set_xlabel('timestamp indexes', fontsize=32)
    ax.set_ylabel('Average value of curtailment setpoint [%]', fontsize=32)
    legend = plt.legend(frameon=True)
    legend.get_frame().set_facecolor('white')
    legend.get_frame().set_edgecolor('black')
    plt.show()
    fig.savefig(f'{dirProject}/opt_curtailment_comp_docker.png')

    
    dfPlot = pd.DataFrame()
    dfPlot['tstmp'] = dfBM_max['tstmp']
    dfPlot['BM_max'] = dfBM_max['Pdeviation']
    dfPlot['BM_flex'] = dfBM_flex['Pdeviation']
    dfPlot['MPC_SLSQP_3'] = dfMPC_SLSQP_3['Pdeviation']
    dfPlot['MPC_SLSQP_3_docker'] = dfMPC_SLSQP_3_docker['Pdeviation']
    
    fig = plt.figure(figsize=(16,8), dpi=600)
    # plt.style.use("whitegrid")
    ax = sns.boxplot(data=dfPlot[480:1020], showfliers=False, whis=3, color=".8",  width=.5)
    ax.set_ylabel('Deviation to Pref [kW]', fontsize=24)
    ax.set_ylim([-250,100])
    plt.show()
    fig.savefig(f'{dirProject}/error_analysis_docker.png')
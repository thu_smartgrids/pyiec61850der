# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 17:29:11 2023

@author: chen
"""

import pandas as pd
import numpy as np
import scipy.stats as stats
import math
import os
import copy
import time
import scipy
from scipy.interpolate import splrep, BSpline
from scipy.optimize import minimize
from scipy.optimize import Bounds
from scipy.optimize import LinearConstraint
from scipy.optimize import NonlinearConstraint
import seaborn as sns
import matplotlib.pyplot as plt
from collections import OrderedDict

import plot.plotMG as plotMG
import optimization.optMG as optMG



#%%
##############################################################################
##################    manipulation dfMG if I<100   ###########################
##############################################################################

def dataPrep(dfMG, listDfNode, I, scalingLoad=4, minControlPct=30, scalingForecast=False):
    nTstmp = len(dfMG)
     
    pathDockerConfig = './tester/sim_tests/test01_simplified_MPC/config_MG_nodes.xlsx'
    dfConfig = pd.read_excel(pathDockerConfig, sheet_name='Config')    
    
    
    dfSyntheticLoad = dfMG['Pnet'].copy()
    dfSyntheticLoad[:] = 0.0
    
    for i in range(I):
        listDfNode[i]['Pload'] = listDfNode[i]['Pload']*scalingLoad
        dfSyntheticLoad += listDfNode[i]['Pload']
        
    dfLoadPred = dfSyntheticLoad.copy()/I
    
    # add noise to the data and create "predicted data"
    for i in range(I):
        listDfNode[i]['Pload_pred'] = dfLoadPred + np.random.normal(0, 0.25, nTstmp)
        if scalingForecast:
            maxGen = np.max(listDfNode[i]['Pgen'])
            maxForecast = np.max(listDfNode[i]['Pgen_pred'])
            listDfNode[i]['Pgen_pred'] = copy.copy(listDfNode[i]['Pgen_pred'])*maxGen/maxForecast
        
    # update PV capacity
    for i in range(I):
        listDfNode[i]['P_max'] = copy.copy(dfConfig['pvRatedPower'][i])
        listDfNode[i]['P_min'] = listDfNode[i]['P_max'] * minControlPct/100
        listDfNode[i]['P_control'] = copy.copy(listDfNode[i]['P_max'])
        listDfNode[i]['P_delta'] = 0
        listDfNode[i]['Pgen_actual'] = copy.copy(listDfNode[i]['Pgen'])
        
    dfMG['P_control'] = np.mean([node['P_control'] for node in listDfNode], axis=0)
    dfMG['P_max_mean'] = np.mean([node['P_max'] for node in listDfNode], axis=0)
    dfMG['P_min_mean'] = np.mean([node['P_min'] for node in listDfNode], axis=0)
    dfMG['Pload_pred'] = dfSyntheticLoad
    dfMG['Pload'] = dfSyntheticLoad
    dfMG['Pgen_pred'] =  np.sum([node['Pgen_pred'] for node in listDfNode], axis=0)
    dfMG['Pnet_pred'] = dfMG['Pload_pred'] - dfMG['Pgen_pred']
    dfMG['Pnet'] = dfMG['Pload'] - dfMG['Pgen']
    
    return dfMG, listDfNode


def addNoise(dfMG, I, scaleFactor=1):
    '''
    add a bell curve and some gaussian noise to the "measurement" to represent 
    a reference trajectory given by the overlaying grid operator
    '''
    
    nTstmp = len(dfMG)
    
    # first generate the basic bell curve
    mu = 0
    sigma = math.sqrt(0.5)
    x = np.linspace(-12*sigma, 12*sigma, nTstmp)
    y0 = stats.norm.pdf(x, mu, sigma)*np.max(dfMG['Pgen'])*scaleFactor
    
    # then add some Gaussian white noise to the data set
    y1 = np.exp(-x**2) + 0.05 * np.random.randn(nTstmp)
    y = y0+ y1
    
    # fig = plt.figure(figsize=(16,12), dpi=300)
    # sns.set_style("darkgrid")
    # ax = sns.lineplot(data=[y], linestyle='-', lw=5)
    # plt.show()
    
    dfMGNew = dfMG.copy()
    dfMGNew['Pref'] = dfMG['Pnet'] + pd.Series(y)

    return dfMGNew


def exportRawData(dfMG, listDfNode, dirRaw): 
    if not os.path.exists(f'{dirRaw}'):
        os.mkdir(f'{dirRaw}')
        
    if not os.path.exists(f'{dirRaw}/node_raw'):
        os.mkdir(f'{dirRaw}/node_raw')
    
    for i in range(I):
        listDfNode[i].to_csv(f'{dirRaw}/node_raw/node_{i}.csv', index=False)
    
    dfMG.to_csv(f'{dirRaw}/dfMG_raw.csv', index=False)


def exportResult(dfMG, listDfNode, dirResult):

    # export data
    if not os.path.exists(f'{dirResult}/node_result'):
        os.mkdir(f'{dirResult}/node_result')
    
    for i in range(I):
        listDfNode[i].to_csv(f'{dirResult}/node_result/node_{i}.csv', index=False)
    dfMG.to_csv(f'{dirResult}/dfMG_result.csv', index=False)
    
#%% Global setup (The data have been prepared, so dataPrep steps are commented out)

I = 100
dirProject = './tester/sim_tests/test01_simplified_MPC'
if not os.path.exists(dirProject):
    os.mkdir(dirProject)
    
dfMG = pd.read_csv(f'{dirProject}/raw_data_scaled/dfMG_raw.csv')
nTstmp = len(dfMG)

listDfNode = []
for i in range(I):
    listDfNode.append(pd.read_csv(f'{dirProject}/raw_data_scaled/node_raw/node_{i}.csv'))
    
# dirRaw = f'{dirProject}/raw_data'
dirRawScaled = f'{dirProject}/raw_data_scaled'

# # data prep already done, so comment out the code section below
# plotData.plotPrediction(dfMG, dirRaw)
# plotData.plotReference(dfMG, dirRaw)
# exportRawData(dfMG, listDfNode, dirRaw)

# [dfMG, listDfNode] = dataPrep(dfMG, listDfNode, I, scalingLoad=1, minControlPct=10, scalingForecast=True)
# dfMG = addNoise(dfMG, I, scaleFactor=1)
# exportRawData(dfMG, listDfNode, dirRawScaled)

plotMG.plotReference(dfMG, dirRawScaled)
plotMG.plotPrediction(dfMG, dirRawScaled)

#%% Perform benchmark (linear solver) tests without IEC 61850 communication

for benchmark in ['max', 'flex']:
    testName = f'I_{I}_benchmark_{benchmark}'
    dirResult = f'{dirProject}/{testName}'
    if testName not in os.listdir(dirProject):
        os.mkdir(dirResult)
    
    # only observe 8:00 - 19:00 UTC -> [480:1140]
    [dfMGBenchmark, listDfNodeBenchmark] = optMG.derControlLinear(dfMG, listDfNode, idxRange=(480,1020), nominationType=benchmark)
    plotMG.plotBenchmark(dfMGBenchmark, dirResult)
    exportResult(dfMGBenchmark, listDfNodeBenchmark, dirResult)


# %% Perform MPC tests without IEC 61850 communication

for N in [3, 5, 10]:
    # first use solver SLSQP
    method = 'SLSQP'
    options = {'disp': True, 'ftol':1e-1, 'maxiter':300}
    testName = f'I_{I}_N_{N}_{method}'
    dirResult = f'{dirProject}/{testName}'
    if testName not in os.listdir(dirProject):
        os.mkdir(dirResult)
    [listDfNodeMPC, dfMGMPC, listResult, listProcessingTime] = optMG.derControlMPC(N, listDfNode, dfMG, isIEC61850=False,
                                                                                   idxRange=(480,1020), method=method, options=options)
    exportResult(dfMGMPC, listDfNodeMPC, dirResult)
    plotMG.plotResult(listResult, listDfNodeMPC, listProcessingTime, dfMGMPC, dfMGBenchmark, dirResult)
    
    # Then use solver trust-constr
    method = 'trust-constr'
    options = {'disp': True, 'verbose': 1, 'maxiter':300, 'gtol': 1e-1, 'xtol': 1e-1, 'sparse_jacobian': True}
    testName = f'I_{I}_N_{N}_{method}'
    dirResult = f'{dirProject}/{testName}'
    if testName not in os.listdir(dirProject):
        os.mkdir(dirResult)
    [listDfNodeMPC, dfMGMPC, listResult, listProcessingTime] = optMG.derControlMPC(N, listDfNode, dfMG, isIEC61850=False,
                                                                                   idxRange=(480,1020), method=method, options=options)
    exportResult(dfMGMPC, listDfNodeMPC, dirResult)
    plotMG.plotResult(listResult, listDfNodeMPC, listProcessingTime, dfMGMPC, dfMGBenchmark, dirResult)

plotMG.plotComparisonMPC(dirProject)

# %%  Perform MPC tests with IEC 61850 communication by connecting docker containers
# The containers must be started manually

dfMG['P_rate_mean'] = dfMG['P_max_mean']
dfMG['Pgen'] = np.nan
dfMG['Pload'] = np.nan
# assume the initialization is fine
dfMG['P_control'] = np.nan
dfMG['P_max_mean'] = np.nan
dfMG['P_min_mean'] = np.nan

for i in range(I):
    listDfNode[i]['Pgen'] = np.nan
    listDfNode[i]['Pload'] = np.nan
    listDfNode[i]['P_delta'] = 0
    # listDfNode[i]['P_max'] = np.nan
    # listDfNode[i]['P_min'] = np.nan
    # listDfNode[i]['P_control'] = np.nan
    listDfNode[i]['Pgen_actual'] = np.nan
    listDfNode[i]['P_control_pct'] = np.nan

testName = f'I_{I}_N_{N}_{method}_docker'
dirResult = f'{dirProject}/{testName}'
if testName not in os.listdir(dirProject):
    os.mkdir(dirResult)

N = 3 
method = 'SLSQP'
options = {'disp': True, 'ftol':1e-1, 'maxiter':300}
startIdx = 480  # sometimes the timing is bad, the MPC might have to start from time step index 481 or later
idxRange=(startIdx,startIdx+60*9)

[listDfNodeMPC, dfMGMPC, listResult, listProcessingTime] = optMG.derControlMPC(N, listDfNode, dfMG, isIEC61850=True,
                                                                               idxRange=idxRange, method=method, options=options)
exportResult(dfMGMPC, listDfNodeMPC, dirResult)
plotMG.plotResult(listResult, listDfNodeMPC, listProcessingTime, dfMGMPC, dfMGBenchmark, dirResult)
plotMG.plotComparisonDocker(dirProject)
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  5 10:56:21 2024

@author: chen

This test script can be used to monitor the resouce usage of a sub-module.
Primarily targeted at developers for code performance optimization.
"""

import cProfile, pstats, io
from pstats import SortKey
import sys

import configurator as config
from configurator.helper import timeoutChecker
from configurator.logging import rotatingLogger

nameLogger = 'testLogger'
nameLogfile = 'testLogger'   
logger = rotatingLogger(nameLogger, nameLogfile) 

import simulation.pyiec61850cls as pyiec61850cls

PATH_CONFIG = './tester/func_tests/configTester.yaml'


maxIntervals=6*60*2
timeout = 60*maxIntervals

@timeoutChecker(timeout)
def runResourceTeser():
    [clsConfig, iedServer] = pyiec61850cls.runVirtualCLS(pathConfig=PATH_CONFIG,routineTimeout=timeout)

    
pr = cProfile.Profile()
pr.enable()

runResourceTeser()

pr.disable()
s = io.StringIO()
sortby = SortKey.CUMULATIVE
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())

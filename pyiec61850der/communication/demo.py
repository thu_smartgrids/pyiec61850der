# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 11:36:52 2023

@author: chen. morris
"""

# standard python modules
import sys
import os
import logging

# local python modules
import configurator.config as config
import interface

if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850
else:
    from pylibiec61850.windows import iec61850
  
# nameLogger = 'iedLogger'  
# logger = rotatingLogger(nameLogger)
# logger.info('Logger {} initialized'.format(nameLogger))

logger = logging.getLogger(__name__)
    

class demoIedServer(object):
    """
    Create a demo IED server that contains one load or PV Logical Node. The server uses
    a static IEC 61850 data model.
    """
    
    def __init__(self):
        self.configServer = config.ConfigCLS()
        self.configServer.scaleFactor = 5
        self.configServer.unitFactor = 1000
        self.configServer.pvRatedPower = 30*self.configServer.unitFactor  # rated power for random mode
        # the demo server is associated only the following 4 MMS parameters, so they are hard coded in this class
        # TODO: for the practical application, the assignment of index and identifier must be dynamic during the 
        #   IEC 61850 server build process
        
        self.listMmsAddressDO = ['demoVirtualCLS_demoProsumerXY/PV1_MMXU0.TotW',
                          'demoVirtualCLS_demoProsumerXY/PV1_MMXU0.TotVar',
                          'demoVirtualCLS_demoProsumerXY/PV1_MMXU0.Hz',
                          'demoVirtualCLS_demoProsumerXY/PV1_MMXU0.OutWSet',
                          ]
        self.listDataBuffer = []
        self.listMonitorRef = []
        self.listMonitorAddress = []
        self.listControlRef = []
        self.listControlAddress = []
        self.listMmsAddressDA = []
        self.listQualityRef = []
        self.listTstmpRef = []
        self.nDataBuffer = 0
        
        # TODO: the list of init value is not required, could have left it to processing.routine.initValDA
        listInitVal = [7500, 200, 50, 1.0]
        interface.runtime.createStaticListDataBuffer(self, self.listMmsAddressDO, listInitVal)
        
        
        self.objLD = []
        self.iedModel = []
        
        
        class IedServer(object):
            """
            An inner-class to store the IED server object as well as status information.
            Key performance indicators should be regularly exchanged with the parameters in the IED server.
            """

            iec61850Server = None
        
        self.iedServer = IedServer()
        
    
    def create_IED_model_device(self, DeviceName):   
        """
        TODO: desc
        """
        # Logical Device
        self.objLD = iec61850.LogicalDevice_create(DeviceName, self.iedModel)
        # LLN0
        LN_LLN0     = iec61850.LogicalNode_create("LLN0", self.objLD)
        # LLN0 - DOs / CDCs
        DO_LLN0_Beh     = iec61850.CDC_ENS_create("Beh", iec61850.toModelNode(LN_LLN0), 0)
        DA_LLN0_Beh_stVal = iec61850.ModelNode_getChild(iec61850.toModelNode(DO_LLN0_Beh), "stVal")
        DO_LLN0_Loc     = iec61850.CDC_ENS_create("Loc", iec61850.toModelNode(LN_LLN0), 0)
        DO_LLN0_Mod     = iec61850.CDC_ENC_create("Mod", iec61850.toModelNode(LN_LLN0), 0, 1)
        DA_LLN0_Mod_stVal = iec61850.ModelNode_getChild(iec61850.toModelNode(DO_LLN0_Mod), "stVal")
        DO_LLN0_NamPlt = iec61850.CDC_DPL_create("NamPlt", iec61850.toModelNode(LN_LLN0), 0)
        # LPHD1
        LN_LPHD1 = iec61850.LogicalNode_create("LPHD1", self.objLD)
        # LPHD1 - DOs / CDCs
        DO_LPHD1_PhyHealth = iec61850.CDC_ENS_create("PhyHealth", iec61850.toModelNode(LN_LPHD1), 0)
        DO_LPHD1_PhyNam = iec61850.CDC_DPL_create("PhyNam", iec61850.toModelNode(LN_LPHD1), 0)
        DO_LPHD1_Proxy = iec61850.CDC_SPS_create("Proxy", iec61850.toModelNode(LN_LPHD1), 0)
    
    
    def create_IED_model_essential(self):
        """
        TODO: desc
        """
        setattr(self, "iedModel", 0)
            
        IedModelName = self.configServer.IedModelName + '_'            
        self.iedModel = iec61850.IedModel_create(IedModelName)
        
        logger.info("Auto generating IED model: %s" %(IedModelName))
        return
        
    def create_demo_IED_model(self):
        """
        Create LN (pvsys or load) from JSON tag
        """

        # Def
        dictUpdateVal = dict()
        
        PF_attr = []
        Iec_DA = []
        Iec_DO_name = []
        Iec_DA_mag = []
        Iec_DA_t = []
            
        # create_LN
        DeviceName = self.configServer.DeviceName
        logger.info(f'LN Name: {DeviceName}')
            
        if self.configServer.derType == 'pv':
            SysTypPref = 'PV'
        elif self.configServer.derType == 'load':
            SysTypPref = 'LOAD'
        else:
            SysTypPref = 'UNKNOWN'
        
        
        
        LNDevInstance = '1'
        logger.info(f'Current LN Dev Instance: {LNDevInstance}')
        
        LNType = "MMXU"
        LNInstance = int(LNDevInstance)-1
        LNName = SysTypPref + str(LNDevInstance) + "_" + LNType + str(LNInstance)
        LNconfigStr = DeviceName + "_LN_" + LNName	
        		
        LNconfig = setattr(self, LNconfigStr, 0)
        LNconfig = iec61850.LogicalNode_create(LNName, self.objLD)
        	
        # create_DS 
        DSconfigStr = DeviceName + "_DS_" + LNName	
        DSconfig = setattr(self, DSconfigStr, 0)
        DSconfig = iec61850.DataSet_create(DSconfigStr, LNconfig)	
        
        RCB_configStr = DeviceName + "_RP_" + LNName
        RCB_configStrID = RCB_configStr + "_01"
        RCB_config = setattr(self, RCB_configStr, 0)
        RCB_config =  iec61850.ReportControlBlock_create(RCB_configStr, LNconfig, RCB_configStrID, False, DSconfigStr , 1, 8, 0, 0, 10000)
        
        self.DSconfig = DSconfig
        self.DSconfigStr = DSconfigStr
        self.RCB_config = RCB_config
        self.RCB_configStr = RCB_configStr
        
        # Create Variables
        ### Create Ubb
        Value = "Hz"	  
        ValueType = "mag_f"
        ValueDataType = "mag.f"
        DA_t = 't'
        DOconfig = DeviceName + "_DO_"  + LNName + "_" + Value
        DAconfig = DeviceName + "_DA_"  + LNName + "_" + Value	+ "_" + ValueType
        DAconfig_t = DeviceName + "_DA_"  + LNName + "_" + Value	+ "_" + DA_t
        Iec_DO_name.append(DOconfig)
        
        
        localconfigDO = setattr(self, DOconfig, 0)
        localconfigDO = iec61850.CDC_MV_create(Value, iec61850.toModelNode(LNconfig), 0, False)
        localconfigDA = setattr(self, DAconfig, 0)
        localconfigDA = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), ValueDataType)
        localconfigDA_t = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), DA_t)
        
        idxDO = None
        for idx, addressDO in enumerate(self.listMmsAddressDO):
            if Value in addressDO:
                idxDO = idx
        if idxDO is not None:
            self.listMonitorRef[idxDO] = localconfigDA
            self.listMonitorAddress[idxDO] = f'{self.listMmsAddressDO[idxDO]}.{ValueDataType}'
            self.listTstmpRef[idxDO] = localconfigDA_t
        
        PF_attr.append("m:u")
        Iec_DA.append(localconfigDA)
        Iec_DA_mag.append(0)
        Iec_DA_t.append(localconfigDA_t)
        
        setattr(self.listDataBuffer[2], 'index', PF_attr.index("m:u"))  # hard coded here!!
        
        #Add DataSet Entry
        DSentryMMS = LNName + "$MX$" + Value
        DSconfigEntryStr = DeviceName + "_DS_" + LNName + "_" + Value
        localconfigDSentry = setattr(self, DSconfigEntryStr, 0)
        localconfigDSentry = iec61850.DataSetEntry_create(DSconfig, DSentryMMS, -1, None)
        
        ### Create DO TotW
        Value = "TotW"	
        ValueType = "mag_f"
        ValueDataType = "mag.f"
        DOconfig = DeviceName + "_DO_"	+ LNName + "_" + Value
        DAconfig = DeviceName + "_DA_"  + LNName + "_" + Value	+ "_" + ValueType
        Iec_DO_name.append(DOconfig)
        	
        localconfigDO = setattr(self, DOconfig, 0)
        localconfigDO = iec61850.CDC_MV_create(Value, iec61850.toModelNode(LNconfig), 0, False)
        localconfigDA = setattr(self, DAconfig, 0)
        localconfigDA = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), ValueDataType)
        localconfigDA_t = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), DA_t)
        
        idxDO = None
        for idx, addressDO in enumerate(self.listMmsAddressDO):
            if Value in addressDO:
                idxDO = idx
        if idxDO is not None:
            self.listMonitorRef[idxDO] = localconfigDA
            self.listMonitorAddress[idxDO] = f'{self.listMmsAddressDO[idxDO]}.{ValueDataType}'
            self.listTstmpRef[idxDO] = localconfigDA_t
        
        PF_attr.append("m:P:bus1")
        Iec_DA.append(localconfigDA)
        Iec_DA_mag.append(0)
        Iec_DA_t.append(localconfigDA_t)
        setattr(self.listDataBuffer[0], 'index', PF_attr.index("m:P:bus1"))  # hard coded here!!
        
        #Add DataSet Entry
        DSentryMMS = LNName + "$MX$" + Value
        DSconfigEntryStr = DeviceName + "_DS_" + LNName + "_" + Value
        localconfigDSentry = setattr(self, DSconfigEntryStr, 0)
        localconfigDSentry = iec61850.DataSetEntry_create(DSconfig, DSentryMMS, -1, None)
        
        ### Create TotVar
        Value = "TotVar"	
        ValueType = "mag_f"
        ValueDataType = "mag.f"
        DOconfig = DeviceName + "_DO_"	+ LNName + "_" + Value
        DAconfig = DeviceName +"_DA_"  + LNName + "_" + Value	+ "_" + ValueType
        Iec_DO_name.append(DOconfig)
        	
        localconfigDO = setattr(self, DOconfig, 0)
        localconfigDO = iec61850.CDC_MV_create(Value, iec61850.toModelNode(LNconfig), 0, False)
        localconfigDA = setattr(self, DAconfig, 0)
        localconfigDA = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), ValueDataType)
        localconfigDA_t = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), DA_t)
        
        idxDO = None
        for idx, addressDO in enumerate(self.listMmsAddressDO):
            if Value in addressDO:
                idxDO = idx
        if idxDO is not None:
            self.listMonitorRef[idxDO] = localconfigDA
            self.listMonitorAddress[idxDO] = f'{self.listMmsAddressDO[idxDO]}.{ValueDataType}'
            self.listTstmpRef[idxDO] = localconfigDA_t
        
        
        
        PF_attr.append("m:Q:bus1")
        Iec_DA.append(localconfigDA)
        Iec_DA_mag.append(0)
        Iec_DA_t.append(localconfigDA_t)
        setattr(self.listDataBuffer[1], 'index', PF_attr.index("m:Q:bus1"))  # hard coded here!!
        
        #Add DataSet Entry
        DSentryMMS = LNName + "$MX$" + Value
        DSconfigEntryStr = DeviceName + "_DS_" + LNName + "_" + Value
        localconfigDSentry = setattr(self, DSconfigEntryStr, 0)
        localconfigDSentry = iec61850.DataSetEntry_create(DSconfig, DSentryMMS, -1, None)
        
        ######### DO for control ################################
        Value = "OutWSet"	
        ValueType = "setMag_f"
        ValueDataType = "setMag.f"
        DOconfig = DeviceName + "_DO_"	+ LNName + "_" + Value
        DAconfig = DeviceName + "_DA_"  + LNName + "_" + Value	+ "_" + ValueType
        Iec_DO_name.append(DOconfig)
        	
        localconfigDO = setattr(self, DOconfig, 0)
        localconfigDO = iec61850.CDC_ASG_create(Value, iec61850.toModelNode(LNconfig), iec61850.CDC_OPTION_UNIT, False)
        localconfigDA = setattr(self, DAconfig, 0)
        localconfigDA = iec61850.ModelNode_getChild(iec61850.toModelNode(localconfigDO), ValueDataType)
        
        idxDO = None
        for idx, addressDO in enumerate(self.listMmsAddressDO):
            if Value in addressDO:
                idxDO = idx
        if idxDO is not None:
            self.listMonitorRef[idxDO] = localconfigDA
            self.listMonitorAddress[idxDO] = f'{self.listMmsAddressDO[idxDO]}.{ValueDataType}'
            self.listTstmpRef[idxDO] = None
            
            self.listControlRef[idx] =  localconfigDA
            self.listControlAddress[idx] = f'{self.listMmsAddressDO[idxDO]}.{ValueDataType}'
        
        PF_attr.append("Pmax_uc")
        Iec_DA.append(localconfigDA)
        Iec_DA_mag.append(0)
        Iec_DA_t.append(None)
        setattr(self.listDataBuffer[3], 'index', PF_attr.index("Pmax_uc"))  # hard coded here!!
        
        #Add DataSet Entry
        DSentryMMS = LNName + "$SP$" + Value
        DSconfigEntryStr = DeviceName + "_DS_" + LNName + "_" + Value
        localconfigDSentry = setattr(self, DSconfigEntryStr, 0)
        localconfigDSentry = iec61850.DataSetEntry_create(DSconfig, DSentryMMS, -1, None)
            
        # write initial IED data model
        dictUpdateVal["PF_attr"] = PF_attr  
        dictUpdateVal["Iec_DA"] = Iec_DA
        dictUpdateVal["Iec_DO_name"] = Iec_DO_name
        dictUpdateVal["Iec_DA_mag"] = Iec_DA_mag
        dictUpdateVal["Iec_DA_t"] = Iec_DA_t
            
        logger.info("LD created: {}".format(DeviceName))
        return (dictUpdateVal)
    
    def create_IED_server(self):
        """
        TODO: desc
        """
        self.iedServer.iec61850Server = iec61850.IedServer_create(self.iedModel)
        logger.info("IED server created")
    
    def start_IED_server(self):
        """
        TODO: desc
        """
        iec61850.IedServer_start(self.iedServer.iec61850Server, self.configServer.localTcpPort)
        logger.info("IED server started")
        return
    
    def buildDemoServer(self):
        """
        If any configuration needs to be changed, do it before calling this method.
        """
        container_all_objects = []
        container_pvsys = []
        
        logger.info('=================   Begin build demo IEC 61850 server   =================')
        listNameLN = ['MMXU']
        self.create_IED_model_essential()
        self.create_IED_model_device(self.configServer.DeviceName)
        for NameLN in listNameLN:                  
            dictUpdateVal = self.create_demo_IED_model()
            container_all_objects.append(dictUpdateVal)
            container_pvsys.append(dictUpdateVal)
        
        self.create_IED_server()
        self.start_IED_server()
        
        logger.info('Verif DO in the IED data model')     
        logger.info(container_pvsys)
        
        logger.info('verify LD in the IED model')
        logger.info('IED LD count: {}'.format(iec61850.IedModel_getLogicalDeviceCount(self.iedModel)))
        
        logger.info('Verif Data Sets in the IED data model') 
        obj_dataset = iec61850.IedModel_lookupDataSet(self.iedModel, '{}_{}/PV1_MMXU0${}'.format(self.configServer.IedModelName, self.configServer.DeviceName, self.DSconfigStr))    
        if obj_dataset != None:
            logger.info('Found data set with name: {}'.format(obj_dataset.name))
        else:
            logger.warning('No data set available')
        logger.info('Data set size: {}'.format(iec61850.DataSet_getSize(obj_dataset)))
        
        logger.info('=================   End build demo IEC 61850 server   =================\n')


def initStaticDemoServer(instConfig=None):
    iedServer = demoIedServer()
    iedServer.buildDemoServer()
    iedServer.configServer.cTime = instConfig.cTime
    iedServer.configServer.cTimeUnix = instConfig.cTimeUnix
    
    # TODO: the configuration of dataBuffer for each IEC 61850 is hard coded here, it requires a generalization!! 
    iedServer.listDataBuffer[0].dataSource = instConfig.dataSource
    iedServer.listDataBuffer[1].dataSource = 'random'
    iedServer.listDataBuffer[2].dataSource = 'random'
    iedServer.listDataBuffer[0].isMonitor = True
    iedServer.listDataBuffer[1].isMonitor = True
    iedServer.listDataBuffer[2].isMonitor = True
    iedServer.listDataBuffer[3].isMonitor = True
    iedServer.listDataBuffer[3].isControl = True
    
    if instConfig.derType == 'pv':
        iedServer.listDataBuffer[0].unitFactor = instConfig.unitFactor
        iedServer.listDataBuffer[0].scaleFactor = instConfig.scaleFactor
        iedServer.listDataBuffer[0].valueRated = instConfig.pvRatedPower
        iedServer.listDataBuffer[0].limit = (0, instConfig.pvRatedPower * instConfig.unitFactor * instConfig.scaleFactor)
    
    # TODO: find a way to pass the attributes of clsConfig to the dataBuffer instances, copy config is not the ultimate solution!
    for dataBuffer in iedServer.listDataBuffer:
        dataBuffer.influxdb.bucketRead = instConfig.bucketRead
        dataBuffer.influxdb.bucketWrite = instConfig.bucketWrite
        dataBuffer.influxdb.GUID = instConfig.clsGUID
        dataBuffer.influxdb.queryHandler = instConfig.clientInfluxdbRead
        dataBuffer.influxdb.writeHandler = instConfig.clientInfluxdbWrite

    if iedServer.listDataBuffer[0].dataSource == 'local':
        profileAllData = interface.local.initProfile(instConfig.filenameProfile, 'Value', 'Datetime', 
                                '%Y-%m-%d %H:%M:%S', instConfig.startTime, instConfig.endTime)
        iedServer.listDataBuffer[0].profileLocal.profileAll = profileAllData  # here the index 0 refers to TotW.mag.f
    
    return iedServer


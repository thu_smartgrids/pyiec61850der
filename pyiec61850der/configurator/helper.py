# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 14:31:22 2023

@author: chen

TODO: straight out the time format here, many functions has utc is the function name 
    but they may not really handle a time string as UTC properly!
"""

import os
import sys
from datetime import datetime, timedelta, timezone
import pytz  # this module deals with time zones
import time
import calendar
import json
import logging
import uuid
import numbers
import numpy as np

logger = logging.getLogger(__name__)

if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850

else:
    from pylibiec61850.windows import iec61850


# %% This section deals with some timout handlers

"""
Timeout decorator in case a function is stucked in one single never-ending process.
Ref: https://stackoverflow.com/questions/492519/timeout-on-a-function-call
attention: the solution with package signal does not work on Windows.

The windows solution posted by Rich was abandoned because it leaves a forever running thread.
BTW: signal package causes error when used in a multithreading pool, so this approach has to be
dumped anyway...
So we use the open-source code of aaronchall instead.
License @aaronchall: 
https://gist.github.com/aaronchall/6331661fe0185c30a0b4


TODO: this is still a bad solution for our applicaiton. It exists just to avoid runtime error
in windows environment.
Consider use the wrapt_timeout_decorator lib to handle windows timeouts
Ref: https://towardsdatascience.com/adding-timeouts-to-functions-using-wrapt-timeout-decorator-21790890a49b

"""

import sys
import threading
from time import sleep

try:
    import thread
except ImportError:
    import _thread as thread

try:  # use code that works the same in Python 2 and 3
    range, _print = xrange, print


    def print(*args, **kwargs):
        flush = kwargs.pop('flush', False)
        _print(*args, **kwargs)
        if flush:
            kwargs.get('file', sys.stdout).flush()
except NameError:
    pass


def cdquit(fn_name):
    # print to stderr, unbuffered in Python 2.
    print('{0} took too long'.format(fn_name), file=sys.stderr)
    sys.stderr.flush()  # Python 3 stderr is likely buffered.
    thread.interrupt_main()  # raises KeyboardInterrupt


def timeoutChecker(max_timeout: int):
    '''
    use as decorator to exit process if
    function takes longer than max_timeout seconds
    '''

    def outer(fn):
        def inner(*args, **kwargs):
            timer = threading.Timer(max_timeout, cdquit, args=[fn.__name__])
            timer.start()
            try:
                result = fn(*args, **kwargs)
            finally:
                timer.cancel()
            return result

        return inner

    return outer


'''
TODO: this piece of code can not be used till the error in mulitthreading 
Error trace:
Traceback (most recent call last):
  File "/work/simulation/pyiec61850cls.py", line 167, in updateDB
    updateDataBuffer(instConfig, iedServer, cTime, verbose=True)
  File "/work/processing/routine.py", line 358, in updateDataBuffer
    connObjScan(instConfig.interface.dfLookupCSV, item)
  File "/work/configurator/helper.py", line 141, in time_limited
    signal.signal(signal.SIGALRM, handler)
  File "/usr/local/lib/python3.7/signal.py", line 47, in signal
    handler = _signal.signal(_enum_to_int(signalnum), _enum_to_int(handler))
ValueError: signal only works in main thread

# import signal
# from functools import wraps
# def timeoutChecker(timeout_secs: int):
#     def wrapper(func):
#         @wraps(func)
#         def time_limited(*args, **kwargs):
#             # Register an handler for the timeout
#             def handler(signum, frame):
#                 raise Exception(f"Timeout for function '{func.__name__}'")
#             # Register the signal function handler
#             signal.signal(signal.SIGALRM, handler)
#             # Define a timeout for your function
#             signal.alarm(timeout_secs)
#             result = None
#             try:
#                 result = func(*args, **kwargs)
#             except Exception as exc:
#                 raise exc
#             finally:
#                 # disable the signal alarm
#                 signal.alarm(0)
#             return result
#         return time_limited
#     return wrapper
'''

def setWorkDir():
    '''
    TODO: desc
    '''

    if os.name != 'nt':  # if not windows os
        sys.path.append("/work/")
        listFile = os.listdir("/work/")
        logger.info("check files in working directory")
        logger.info(listFile)
        os.environ["PATH"] = "/work/;" + os.environ["PATH"]
    else:
        logger.info('Running in windows environment, no change required')
        pass


def getCurrentTimestamp(localTimezone='Europe/Berlin', timeOffset=0):
    """

    This function takes the wished time format and timezone as input,
    returns a tuple of the current timestamp in all possible format (Unix, String, datetime).
    The function based on the assumption that datetime.now(timezone.utc) always returns the datetime object
    with current UTC time.

    Alternatively it is possible to read local time directly using datetime.now(), but we can not always ensure that
    the local machine has a correct timezone setting, for this reason we always get utc time and set timezone as an
    input argument

    Input:
    - localTimezone: a timezone string according to ISO 8601, 'UTC' means UTC time
    - offset: a float number for time shift in second, negative means time delay

    Clarification of the timezones:
    - UTC: Coordinated Universal Time, which is also the Greenwich Mean Time
    - local time: e.g. CET should handle the time switch between CETS and CETW

    Clarification of the time formats:
    - Unix: the UNIX time as a float number
    - String: the time string in this format YYYY-mm-ddTHH:MM:SS.XXXX
    - datetime: the original datetime type object

    Note: The parameter timeOffset allows you to add some time shift to the local time,
    it should only be used if a time delay is known or has been set in the simulation.
    Time shift caused by time zones should be handled with the timezone designator, not this offset!!

    """
    TIME_STRING = '%Y-%m-%dT%H:%M:%S.%f'
    currentTimeUTC = datetime.now(timezone.utc) + timedelta(seconds=timeOffset)
    strCurrentTimeUTC = datetime.strftime(currentTimeUTC, TIME_STRING)
    UnixCurrentTimeUTC = time.mktime(currentTimeUTC.timetuple())

    if localTimezone != 'UTC':
        currenTimeLocal = currentTimeUTC.astimezone(pytz.timezone(localTimezone))
        strCurrentTimeLocal = datetime.strftime(currenTimeLocal, TIME_STRING)
        UnixCurrentTimeLocal = time.mktime(currenTimeLocal.timetuple())
        currentTimestamp = (UnixCurrentTimeLocal, strCurrentTimeLocal, currenTimeLocal)
    else:
        currentTimestamp = (UnixCurrentTimeUTC, strCurrentTimeUTC, currentTimeUTC)

    return currentTimestamp


def checkTimeStringFormat(timeString=None, timeFormat=None):
    '''
    This method helps to validate the time string format when handling data interface.
    '''

    try:
        time.strptime(timeString, timeFormat)
        return True
    except ValueError:
        return False


def utcTstmpToUnix(timeStrUTC=None, timeStrFormat='%Y-%m-%d %H:%M:%S'):
    '''
    Convert a UTC timestamp string into a UNIX float time value.
    '''

    valDatedatetime = datetime.strptime(timeStrUTC, timeStrFormat)
    timeUnix = time.mktime(valDatedatetime.timetuple())

    return timeUnix


def unixTstmpToUint64(unixTime=None, localTimezone=None):
    '''
    Get current time and convert to uint64_t for libiec61850 applications.
    If timezone is None, UTC time will be used.

    # TODO: consider using standard python lib to handle the time diff
    '''

    if unixTime is None:
        current_time = datetime.now()
    else:
        current_time = datetime.utcfromtimestamp(unixTime)

    if localTimezone is None:
        localTimezone = 'UTC'
        offset_time = 0
    else:
        timeUTC = datetime.now(timezone.utc).replace(tzinfo=None)
        timeLocal = datetime.now(pytz.timezone(localTimezone)).replace(tzinfo=None)
        diff = timeLocal - timeUTC
        offset_time = diff.seconds

    current_time_uint64_t = (calendar.timegm(current_time.utctimetuple()) + offset_time) * 1000

    return current_time_uint64_t


def unixTstmpToString(unixTime=None, timeStrFormat='%Y-%m-%d %H:%M:%S'):
    '''
    TODO: desc
    '''

    timeString = datetime.strftime(datetime.fromtimestamp(round(int(unixTime) / 10) * 10), timeStrFormat)
    return timeString


def roundTimeString(cTimeUnix=None):
    '''
    TODO: desc
    '''

    cTimeString = unixTstmpToString(cTimeUnix)
    if int(cTimeString[-2:]) == 0:
        pass
    elif int(cTimeString[-2:]) in range(0, 31):
        cTimeString = cTimeString[:-2] + '00'
    elif int(cTimeString[-2:]) in range(31, 60):
        if int(cTimeString[-5:-3]) < 59:
            cTimeString = f'{cTimeString[:-6]}:{int(cTimeString[-5:-3]) + 1}:00'
        else:
            cTimeString = cTimeString[:-2] + '00'
    return cTimeString


def convertTimeStrFormat(timeStr=None, fromFormat=None, toFormat=None):
    '''
    TODO: desc
    '''

    timeStrUnix = utcTstmpToUnix(timeStr, fromFormat)
    timeStrNew = unixTstmpToString(timeStrUnix, toFormat)
    return timeStrNew


def getTimeStrByDiff(timeStr=None, timeStrFormat='%Y-%m-%d %H:%M:%S', timeDiff=-300):
    '''
    TODO: desc
    '''

    timeUnix = utcTstmpToUnix(timeStr, timeStrFormat)
    timeUnixNew = timeUnix + timeDiff
    timeStrNew = unixTstmpToString(timeUnixNew, timeStrFormat)
    return timeStrNew


def getUTCStrByTimezone(timeStr=None, timeStrFormat='%Y-%m-%d %H:%M:%S', localTimezone=None):
    '''
    TODO: desc
    '''

    timeUnix = utcTstmpToUnix(timeStr, timeStrFormat)
    timeUTC = datetime.now(timezone.utc).replace(tzinfo=None)
    timeLocal = datetime.now(pytz.timezone(localTimezone)).replace(tzinfo=None)
    diff = timeLocal - timeUTC
    timeUnixNew = timeUnix - diff.seconds
    timeStrNew = unixTstmpToString(timeUnixNew, timeStrFormat)
    return timeStrNew


def isValidGUID(val):
    '''
    TODO: desc
    '''

    try:
        uuid.UUID(str(val))
        return True
    except ValueError:
        return False


def isValNumber(val):
    """
    Check if a variable is number
    """

    isNumber = isinstance(val, numbers.Number)

    if isNumber and np.isnan(val):
        isNumber = False

    return isNumber
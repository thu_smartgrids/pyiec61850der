# -*- coding: utf-8 -*-
"""
Created on Tue Dec 20 11:55:52 2022

@author: chen

# TODO: this wholt yaml template thing is so inefficient, has to be replaced by a yaml parsing.
"""


import yaml
import json
import os
import pandas as pd
import copy
import uuid
import random
import numpy as np
from datetime import datetime
import shutil

import container.template as template

yamlTemplate = template.YAMLStringTemplate()
npEncoder = template.NpEncoder()
placeholder = template.Placeholder()

def genYamlBuilder(cDir='.', nameDockerImage='demoBuilder', builderPort=10000, 
                   builderNetwork='demoNetwork', identifier='_'):
    
    '''
    Generate the yaml file for image builder.
    The output docker-compose file for the builder might be faulty, you probably need to modify it by hand before
    putting it into the container platform. But this only has to be done once.

    '''
    
    if isAddTimeId:
        identifier = '_' + datetime.strftime(datetime.fromtimestamp(datetime.today().timestamp()), '%Y%m%d%H%M%S')
    else:
        identifier = '_'
    builderImage = f'{nameDockerImage}:1.0'
    builderServiceName = f'{nameDockerImage}{identifier}imagebuilder'
    builderContainerName = f'{nameDockerImage}{identifier}builder'
    dirImageBuilder = f'{cDir}/output/builder'
    if not os.path.exists(dirImageBuilder):
        os.mkdir(dirImageBuilder)
    
    # first create builder application
    strYmlFile = ''
    strYmlFile = strYmlFile + yamlTemplate.headerStr.replace(placeholder.NETWORK, builderNetwork)
    strYmlFile = strYmlFile + yamlTemplate.builderStr.replace(placeholder.SERVICE, builderServiceName)\
                .replace(placeholder.IMAGE, builderImage)\
                .replace(placeholder.CONTAINER, builderContainerName)\
                .replace(placeholder.PORT, str(builderPort))
    strYmlFile = strYmlFile + yamlTemplate.footStr

    with open(f'{dirImageBuilder}/docker-compose.yml', 'w') as f:
        f.write(strYmlFile) 
        
    return builderImage


def genYamlGroup(pathSCL=None, dfMapping=None, dictConfigYaml=None, dfLookupTable=None, maxNumberAppGroup=None, 
                 dirOutput=None, groupNetwork=None, nameDockerImage=None, pathSecret=None):
    '''
    Generate the yaml file for container groups

    '''
    nCLS = len(dfMapping)
    listIdxAll = list(range(nCLS))
    listIdxAll.reverse()
    nGroup = int(nCLS/maxNumberAppGroup)
    
    for g in range(nGroup):
        n = 0
        pathGroup = f'{dirOutput}/{nameDockerImage}_group{g}'
        if not os.path.exists(pathGroup):
            os.mkdir(pathGroup)

        pathGroupComposeYml = f'{pathGroup}/docker-compose.yml'
        
        isGroupFull = False
        strYmlFile = ''
        strYmlFile = strYmlFile + yamlTemplate.headerStr.replace(placeholder.NETWORK, groupNetwork)
        strYmlFile = strYmlFile + yamlTemplate.templateStrService
        while isGroupFull == False:  
            # add modified string to the docker-compose yaml templates
            idx = listIdxAll.pop()
            idxNode = list(dfMapping['filename']).index(f'node_{idx}.csv')
            nodeName = dfMapping.loc[idxNode, 'filename'].split('.')[0]
            nodeGUID = dfMapping.loc[idxNode, 'GUID']
            port = startPort + idx
      
            nodeImage = f'{nameDockerImage}:1.0'
            nodeServiceName = f'{nameDockerImage}{identifier}node{idx}'
            nodeContainerName = f'{nameDockerImage}{identifier}node{idx}'
            pathNode = f'{pathGroup}/node{idx}'
            pathNodeMount = f'./node{idx}'
            pathNodeConfigYaml = f'{pathNode}/config.yaml'
            pathNodeLookup = f'{pathNode}/lookup.csv'
            pathNodeSCL = f'{pathNode}/node{idx}.cid'
            pathNodeSCLMount = f'./configurator/node{idx}.cid'
            pathNodeSecret = f'{pathNode}/influxdb.json'

            dictConfigYamlOutput = copy.deepcopy(dictConfigYaml)
            dictConfigYamlOutput['Container']['containerName'] = nodeContainerName
            dictConfigYamlOutput['Container']['containerGUID'] = nodeGUID
            dictConfigYamlOutput['Container']['serviceName'] = nodeServiceName
            dictConfigYamlOutput['Interface']['pathSCL'] = pathNodeSCLMount
            
            if not os.path.exists(pathNode):
                os.mkdir(pathNode)

            mountStr = f"""      - '{pathNodeMount}/config.yaml:/work/{dictConfigYamlOutput['Interface']['pathConfigYAML'].replace('./','')}'
      - '{pathNodeMount}/lookup.csv:/work/{dictConfigYamlOutput['Interface']['pathLookupCSV'].replace('./','')}'
      - '{pathNodeMount}/influxdb.json:/work/secret/influxdb.json'
      - '{pathNodeMount}/node{idx}.cid:/work/{dictConfigYamlOutput['Interface']['pathSCL'].replace('./','')}'"""   
        
            strYmlFile = strYmlFile + yamlTemplate.templateStr.replace(placeholder.SERVICE, nodeServiceName)\
                .replace(placeholder.IMAGE, nodeImage)\
                .replace(placeholder.CONTAINER, nodeContainerName)\
                .replace(placeholder.MOUNT, mountStr)\
                .replace(placeholder.PORT, str(port))\
                .replace(placeholder.NETWORK, groupNetwork)

          
            # export new config.yaml for each node
            
    
            with open(pathNodeConfigYaml, 'w') as f:
                yaml.dump(dictConfigYamlOutput,f)
            
            
            # export new lookup table for each node
            dfLookupTableNode = copy.deepcopy(dfLookupTable)
            for i in range(len(dfLookupTableNode)):
                configJSON = dfLookupTableNode.loc[i, 'configJSON']                
                if type(configJSON) is str:
                    dictJSON = json.loads(configJSON.replace("\'", "\""))
                    if 'GUID' in dictJSON.keys():
                        dictJSON['GUID'] = nodeGUID
                        dfLookupTableNode.loc[i, 'configJSON'] = '"' + str(dictJSON) + '"'                    
            dfLookupTableNode.to_csv(pathNodeLookup, index=False)            
            
            
            # copy the CID file for each node, rename it with node index
            shutil.copyfile(pathSCL, pathNodeSCL)
            shutil.copyfile(pathSecret, pathNodeSecret)
            
            
            n+=1
            if n == maxNumberAppGroup:
                isGroupFull = True
                strYmlFile = strYmlFile + yamlTemplate.footStr
                with open(pathGroupComposeYml, 'w') as f:
                    f.write(strYmlFile) 

# global setting
nameProject = 'demo_batch'

cDir = f'./container/batch/{nameProject}'
pathMapping = f'{cDir}/mapping4Influxdb.csv'
pathMappingOutput = pathMapping.split('.')[0] + '_update.csv'
pathConfigFile = f'{cDir}/config.yaml'
pathLookupTable = f'{cDir}/IEC61850_DA_lookup_tester_DER_slim_CIRED.csv'
pathSCL = f'{cDir}/IEC61850_DER_v1_slim_CIRED.cid'
pathSecret = f'{cDir}/influxdb.json'
dirOutput = f'{cDir}/output'
if not os.path.exists(dirOutput):
    os.mkdir(dirOutput)
isAddTimeId = False

if isAddTimeId:
        identifier = '_' + datetime.strftime(datetime.fromtimestamp(datetime.today().timestamp()), '%Y%m%d%H%M%S') + '_'
else:
    identifier = '_'
    
# load configuration files
dfMapping = pd.read_csv(pathMapping, engine='python')
dfLookupTable = pd.read_csv(pathLookupTable, engine='python')

#dfMappingOutput = copy.deepcopy(dfMapping)
with open(pathConfigFile, "r") as stream:
    dictConfigYaml = yaml.safe_load(stream)


# settings for docker image builder
builderPort = 43300
builderNetwork = 'influxdb_network'
nameDockerImage = 'pyiec61850_mpc_tester'  # image name must be lowercase


# settings for container application groups

startIdx = 0
endIdx = None  # set to None if no limitation should be applied, otherwise an integer index
maxNumberAppGroup = 10  # define how many containers an docker application could contain
startPort = 43301
groupNetwork = 'influxdb_network'


# generate the configuration files
# builderImage = genYamlBuilder(cDir, nameDockerImage, builderPort, builderNetwork, identifier)
genYamlGroup(pathSCL, dfMapping, dictConfigYaml, dfLookupTable, maxNumberAppGroup, dirOutput, groupNetwork, nameDockerImage, pathSecret)


# Install script for directory: /work/pylibiec61850/linux/libiec61850-1.4.1

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelopmentx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/libiec61850" TYPE FILE FILES
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_base.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_time.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_thread.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_filesystem.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_ethernet.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/hal_socket.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/tls_config.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/platform_endian.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/hal/inc/lib_memory.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/common/inc/libiec61850_common_api.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/common/inc/libiec61850_platform_includes.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/common/inc/linked_list.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/common/inc/string_utilities.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_client.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_common.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_server.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_model.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_cdc.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_dynamic_model.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/iec61850/inc/iec61850_config_file_parser.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_value.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_common.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_types.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_type_spec.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_client_connection.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/mms_server.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/mms/inc/iso_connection_parameters.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/goose/goose_subscriber.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/goose/goose_receiver.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/goose/goose_publisher.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/sampled_values/sv_subscriber.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/sampled_values/sv_publisher.h"
    "/work/pylibiec61850/linux/libiec61850-1.4.1/src/logging/logging_api.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/work/pylibiec61850/linux/libiec61850-1.4.1/build/hal/cmake_install.cmake")
  include("/work/pylibiec61850/linux/libiec61850-1.4.1/build/examples/cmake_install.cmake")
  include("/work/pylibiec61850/linux/libiec61850-1.4.1/build/src/cmake_install.cmake")
  include("/work/pylibiec61850/linux/libiec61850-1.4.1/build/pyiec61850/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/work/pylibiec61850/linux/libiec61850-1.4.1/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")

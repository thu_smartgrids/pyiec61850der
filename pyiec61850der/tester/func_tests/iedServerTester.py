import importlib.util
import configurator as config

config.setEnv()
ifExist = importlib.util.find_spec("pylibiec61850")
if ifExist is None:
    print('Warning -- The local module pyiec61850der is not configured properly!!')
    print('A essential lib pylibiec61850 can not be found in current working dir!!')
    print('Please check the structure of the working directory!!')
else:
    from configurator.logging import rotatingLogger
    nameLogger = 'testLogger'
    nameLogfile = 'testLogger'   
    logger = rotatingLogger(nameLogger, nameLogfile) 
    
    import simulation.pyiec61850cls as pyiec61850cls
    
    PATH_CONFIG = './tester/func_tests/configTester.yaml'

    [clsConfig, iedServer] = pyiec61850cls.runVirtualCLS(pathConfig=PATH_CONFIG, routineTimeout=0)

# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 15:51:57 2023

@author: chen
"""

class Profile(object):
    '''     
    TODO: add detailed information
    columns of the dfVal should be _time, _value
    
    Later, each BDA/DA in the IEC 61850 could have a instance profile.
    
    The profile could be historical measurements, forecasts or predicted values using other techniques.
    
    Note: not all attributes are always used when an instance is initialized, e.g. local profile doesn't 
        imply knowledge about MMS address and guid. If an attributes has value None, most likely it is not
        required for that instance.
    
    TODO: add an example here for one measurement
    Example:
        iec61850Level = 'BDA'  # BDA/SDA, DA, DO, ... 
        name = None
        addressMMS = None
        guid = None
        identifierStr = None
        dfVal = None
        nVal = None
    
    TODO: compare with interface.runtime.DataBuffer(), the profile instance can copy configuration there
    '''
    
    def __init__(self):
        self.iec61850Level = 'BDA'  # BDA/SDA, DA, DO, ... 
        self.name = None
        self.addressMMS = None
        self.guid = None
        self.identifierStr = None
        self.dfVal = None
        self.nVal = None
        
        # listTimestamp = []
        # listTimestampUnix = []
        # listVal = []
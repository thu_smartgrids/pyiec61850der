# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 15:29:11 2023

@author: chen
"""

import sys
import logging
from logging.handlers import RotatingFileHandler


class ColorLogFormatter(logging.Formatter):
    """
    Inspired by the answer of Sergey Pleshakov in:
    https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output
    """

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = '%(levelname)s - %(name)s - %(message)s'

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def rotatingLogger(nameLogger='unknownLogger', nameLogfile=None):
    """
    rotatingLogger - a logger function that can be repeatly used in the pyiec61850der module

    Create a rotating log for a specific process.
    Each logger writes in up to BACKUP_COUNT log files, each rotation log file has MAX_BYTES
    """

    MAX_BYTES = 300 * 1000 * 1000
    BACKUP_COUNT = 10

    '''
     DEBUG logging level may cause log overflow (e.g. the Rx:timeout debug log from influxdb), level it down to INFO.
     TODO: eventually we have to configure different loggers for all the sub-modules, currently most
      of them just use logger = logging.getLogger(__name__) to inherit the logger from the parent module.
      Try to fix this issue later.
    '''

    # logging.basicConfig(stream=sys.stdout, filemode='a')
    logging.disable(logging.DEBUG)

    logger = logging.getLogger(nameLogger)
    logger.setLevel(logging.DEBUG)

    # create a rotating handler
    if nameLogfile is None:
        nameLogfile = nameLogger
    pathLogFile = f'./logs/{nameLogfile}.log'

    fh = RotatingFileHandler(pathLogFile, maxBytes=MAX_BYTES, backupCount=BACKUP_COUNT)
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(process)d - %(name)s - %(module)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    sh = logging.StreamHandler(sys.stdout)
    sh.setLevel(logging.INFO)
    formatter = ColorLogFormatter()
    sh.setFormatter(formatter)

    # if multiple handlers should be merged into one logger, the next two rows need to be removed.
    # if logger.hasHandlers:
    #     logger.handlers.clear()
    logger.addHandler(fh)
    logger.addHandler(sh)

    return logger

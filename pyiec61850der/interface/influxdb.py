# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 21:42:53 2023

@author: chen
"""

from influxdb_client import InfluxDBClient, Point, WritePrecision, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from configurator import helper
from interface.runtime import Profile
import logging
from configurator.helper import timeoutChecker

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)


def connectInfluxdb(url=None, org=None, token=None, **kwargs):
    """
    Initialize connection to influxdb, create read and write handlers.
    Note: the write handlers may be associated with a token that only has the access to one specific bucket.
        This limitation is not reflected here. The tokenWrite is only used to establish the writing API service.

    Currently for all influxdb actions, configuration in dataBuffer.influxdb will be used both for read (data acquisition)
    and write (upload statistics). It is assumed that one organisation would use the same influxdb for the storage of historical
    data and incoming real-time data. Using the influxdb config file in ./secret, different read/write handlers could be
    configured for mulitple influxdb urls.

    Usage:
        clientInfluxdbRead, clientInfluxdbWrite = interface.influxdb.connectInfluxdb(url, org, tokenRead, tokenWrite)

    Input:
        - url: URL of influxdb
        - org: to which org are the tokens assigned
        - token: token for reading/writeing data, left empty will cause returning the client of type None
        - **kwargs: placeholder for other supplementary keywords argument

    Output:
        - clientInfluxdb: reading/writing handler for influxdb API
        - clientInfluxdbWrite: writing handler for influxdb API
    """

    logger.info('=================   Begin init influxdb connection    =================')

    if token is not None:
        clientInfluxdb = InfluxDBClient(url=url, token=token, org=org, debug=False, timeout=10000)
        # here we set a higher timeout to disable DEBUG:Rx:timeout log message.

        health = clientInfluxdb.health()
        if health.status == 'pass':
            logger.info('Influxdb client is ready to connect')
        else:
            logger.error('Influxdb client has no connection to server')
    else:
        clientInfluxdb = None

    logger.info('=================   End init influxdb connection    =================\n')

    return clientInfluxdb


def getTagIEC61850(dataBuffer=None):
    '''
    Generate the kwTag dictionary based on the IEC 61850 relevant information of
    one dataBuffer instance.
    TODO: find much easier way to locate the filters for influxdb tag.
    '''
    addressDA = dataBuffer.DO.monitorAddressDA
    items = addressDA.split('.')
    LD = items.pop(0)
    DO = items.pop(0)
    kwTag = {}
    kwTag['LD'] = ''.join([x for x in LD.split('_')[1].split('/')[0] if not x.isdigit()])
    kwTag['LN'] = addressDA.split('/')[1][0:4]
    kwTag['DO'] = dataBuffer.DO.name
    kwTag['DA'] = '.'.join(items)
    kwTag['CDC'] = dataBuffer.DO.CDC
    kwTag['FC'] = dataBuffer.DO.FCDA
    if dataBuffer.influxdb.GUID is not None:  # use
        kwTag['GUID'] = dataBuffer.influxdb.GUID

    return kwTag


def getProfile(queryHandler=None, bucket=None, measurement=None, startTimeStr=None, timeRange=86400,
               interval=60, isUTC=True, fieldKey=None, kwTag=None, **kwargs):
    """
    This method will get the PV/load profile from influxdb if called.
    The profile will be queried on a daily basis.

    In the function name, we use the word get instead of load to avoid confusion
    with electrical load profile.

    TODO: consider using only Unix time for influxdb, converting time string is annoying.

    Usage:
        records = interface.influxdb.getProfile(queryHandler, bucket, startTimeStr, timeRange=86400, kwTag={'keyword': value},
                                                fieldKey='Value', interval='1m')

    Input:
        - queryHandler: influxdb reading handler -> clientInfluxdbRead
        - bucket: bucket name for the data query
        - startTimeStrUTC: start time string of the profile time window, according to influxdb time string format:
            * '%Y-%m-%dT%H:%M:%S' for local time
            * '%Y-%m-%dT%H:%M:%SZ' for UTC time
        - timeDiff: time range in seconds, it will be used to define the endTimeStrUTC
        - kwTag: keyword-value pair(s) for the influxdb tags as filter. Currently we use single keyword value pair.
        - fieldKey: the name of the '_field' to localize expected parameter value
        - interval: time resolution of the data query, written as string, e.g. '1m', '15m', etc.
        - **kwargs: placeholder for other supplementary keywords argument

    Output;
        - records: take the records in the response of influxdb as output variable

    """

    STANDARD_TIME_FORMAT_INFLUXDB = '%Y-%m-%dT%H:%M:%SZ'
    if startTimeStr[-1] != 'Z':
        startTimeStr = startTimeStr + 'Z'
    if helper.checkTimeStringFormat(startTimeStr, '%Y-%m-%d %H:%M:%SZ') or \
            helper.checkTimeStringFormat(startTimeStr, '%Y-%m-%d %H:%M:%S'):
        startTimeStr = startTimeStr.replace(' ', 'T')  # influxdb require a T in the middle!!
    if isUTC == False:
        # TODO: here add a logic for time conversion into UTC
        pass

    newProfile = None

    logger.info('=================   Begin update profile   =================')
    logger.info(f'Start profile query, start time stamp {startTimeStr}, time range {timeRange} seconds')

    if not helper.checkTimeStringFormat(startTimeStr, STANDARD_TIME_FORMAT_INFLUXDB):
        logger.error(f'Time string formats {startTimeStr} {STANDARD_TIME_FORMAT_INFLUXDB} do not match, \
                     can not proceed to querying data from influxdb')
    else:
        endTimeStr = helper.unixTstmpToString(
            helper.utcTstmpToUnix(startTimeStr, STANDARD_TIME_FORMAT_INFLUXDB) + timeRange,
            STANDARD_TIME_FORMAT_INFLUXDB)

    queryFlux = f'from(bucket:"{bucket}") |> range(start: {startTimeStr}, stop: {endTimeStr})'

    if measurement is not None:
        queryFlux += f' |> filter(fn:(r) => r["_measurement"] == "{measurement}")'

    if kwTag is not None:
        for key, val in kwTag.items():
            queryFlux += f' |> filter(fn:(r) => r["{key}"] == "{val}")'
    else:
        logger.warning(f'Primary kwTag is None, data query may return multiple data sets')

    if fieldKey is not None:
        # TODO: consider add the field name as key in the configJSON.
        queryFlux += f' |> filter(fn:(r) => r["_field"] == "{fieldKey}")'
        queryFlux += f' |> aggregateWindow(every: {interval}s, fn: last, createEmpty: true) \
                |> fill(usePrevious: true) \
                |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")'
    else:
        # fieldKey is None works fine particularly when one certain field can be located using kwTag dict.
        logger.warning(f'Field string is None, use default key _value, data query may return multiple data sets')
        queryFlux += f' |> aggregateWindow(every: {interval}s, fn: last, createEmpty: true) \
                |> fill(usePrevious: true)'

    try:
        records = queryHandler.query_api().query_data_frame(query=queryFlux)
        if type(records) is list:
            records = records[0]
            logger.warning('Found multiple data sets in influxdb with given configuration, take the first set')

        # in the last line of query flux, columnKey: ["_field"] causes the orginal column name to be remained
        if fieldKey is not None:
            records = records.rename(columns={fieldKey: '_value'})

        nValRef = timeRange / interval
        if len(records) == 0:
            logger.warning('No data found for this day, the simulation will use profile of the previous day')
        elif len(records) > nValRef:
            logger.warning('Found some duplicated data, but no problem, we move on')
        elif len(records) < nValRef:
            logger.warning(f'Found data gap, {nValRef - len(records)} data points are missing.')
            logger.warning('Not a problem, the server will use the previous value in the simulation')
        else:
            logger.info('Data query was successfull, profile loaded')

        logger.info('=================   End update profile   =================\n')

        nVal = len(records)
        # for idx in range(nVal):
        #     records.loc[idx, '_timeUnix'] = helper.(records.loc[idx, '_timeUnix'],)
        if nVal > 0:
            newProfile = Profile()
            newProfile.dfVal = records
            newProfile.nVal = nVal
            newProfile.provider = bucket
            for key, value in kwTag.items():
                setattr(newProfile, key, value)

            newProfile.timeWindow = (records.loc[0, '_time'], records.loc[nVal - 1, '_time'])
            # TODO: add timeUnix column for later use

            # newProfile.timeWindowUnix
        else:
            logger.warning('Influxdb returned an empty list, check the data query construction again.')
        # newProfile.timeWindowUnix = (records.loc[0, '_timeUnix'], records.loc[nVal-1, '_timeUnix'])

    except:
        logger.exception('Geting profile from influxdb has failed.')
    return newProfile


def writeDataframe(writeHandler=None, dfWrite=None, bucket=None, measurement=None, kwTag=None, **kwargs):
    """
    This method will be called once per hour, it pushes the collected measurements
    and control set points to the bucket 7601_virtualCLS_testResults.

    Usage:
        isSuccess = interface.influxdb.writeDataframe(writeHandler, dfWrite, bucket, measurement, kwTag)

    Input:
        - writeHandler: write handler of the influxdb -> clientInfluxdbWrite
        - dfWrite: the dataframe which is to be written in influxdb
        - bucket: bucket name for the data writing
        - measurement: _measurement, in which the dataframe should be stored
        - kwTag: keyword-value pair(s) for the influxdb tags as filter. It could be multiple tags
        - **kwargs: placeholder for other supplementary keywords argument

    Output:
        - isSuccess: True -> writing to influxdb was successful,
                     False -> writing to influxdb failed, the process should wait for the next trigger
    TODO: consider use the status code returned by the influxdb write handler to decide whether the writing process
        was successful or not.

    """

    logger.info('=================   Begin write dataframe into influxdb   =================')

    # TODO: currently we rebuild the dataframe into a list of dictionaries, it works quite well, but could consume much time
    #   when handling large amounts of data. Consider perserving the structure of the dataframe and push it directly to influxdb
    listRecords = []
    for idx in dfWrite.index:
        recordMetrics = {}
        fields = dfWrite.drop(columns=['_time']).loc[idx].to_dict()
        # recordMetrics['measurement'] = measurement
        # recordMetrics['tags'] = kwTag
        recordMetrics['fields'] = fields
        recordMetrics['time'] = dfWrite.loc[
            idx, '_time']  # it is supposed that time string formating is not required here
        listRecords.append(recordMetrics)

    try:
        write_api = writeHandler.write_api(
            write_options=WriteOptions(batch_size=5000, flush_interval=10_000, jitter_interval=2_000,
                                       retry_interval=5_000))
        write_api.write(bucket=bucket, record=listRecords)
        logger.info('Data upload to infludb has been triggered.')
        logger.info(f'{len(listRecords)} data points were uploaded to infludb bucket {bucket}.')
        logger.info('=================   End write dataframe into influxdb   =================\n')
        return True

    except:
        logger.exception('data upload failed, keep the data list unchanged for the next trigger interval')
        logger.info('=================   End write dataframe into influxdb   =================\n')
        return False


def distributeInfluxdbHandler(iedServer=None, instConfig=None):
    """
    This method is used to allow the data buffer instances share a single influxdb handler.
    """

    for dataBuffer in iedServer.listDataBuffer:
        # passing influxdb configuration including the handler

        listKey = [item for item in dir(instConfig.influxdb) if '__' not in item]
        influxLevel = dataBuffer.isMonitor + 2 * dataBuffer.isControl
        # influx level:
        #	0 - no monitoring, no control
        #	1 - only monitoring
        #	2 - only control
        #	3 - monitoring + control
        for key in listKey:
            isPassing = True
            if key == 'clientInfluxdbRead':
                # whether the DB requries influxdb handler depends on if it reads profile data from influxdb
                # so isPassing=True if dataBuffer.isMonitor = True and dataBuffer.dataSource = infludb
                # TODO: make this part cleaner
                if not dataBuffer.isMonitor or dataBuffer.dataSource != "influxdb":
                    isPassing = False
            if key == 'clientInfluxdbWrite' and influxLevel == 0:
                isPassing = False

            if isPassing:
                setattr(dataBuffer.influxdb, key, getattr(instConfig.influxdb, key))
            else:
                setattr(dataBuffer.influxdb, key, None)

        if dataBuffer.influxdb.measurementWrite is None:
            # overwrite measurementWrite by <nameIED>_<containerName>, this is for pushing data and statistics to influxdb write bucket.
            dataBuffer.influxdb.measurementWrite = f'{instConfig.CLS.nameIED}_{instConfig.container.containerName}'

            # double check whether the lookup CSV contains influxdb related configuration for this DO
        if dataBuffer.dataSource == 'influxdb' and dataBuffer.configJSON is not None:
            for keyJSON in dataBuffer.configJSON.keys():
                setattr(dataBuffer.influxdb, keyJSON, dataBuffer.configJSON[keyJSON])


def updateInfluxdbHandler(dataBuffer=None, **kwargs):
    """
    This method is allows the user to configure the influxdb handler individually for each single data buffer instance.
    The method only overwrites existing attributes, unknown keys will be ignored.
    """

    if kwargs is not None:
        for key, val in kwargs.items():
            if key in dataBuffer.influxdb.keys():
                setattr(dataBuffer.influxdb, key, val)
            else:
                logger.warning(f'The given key {key} does not exist, it will be ignored.')


@timeoutChecker(10)
def writeDataToInfluxdb(writeHandler=None, records=None, bucket=None, options=None):
    """
    This method simply performs a write action to upload data to influxdb. Write handler should have been created
    during the service initialisation.
    """

    if options is None:
        options = WriteOptions(batch_size=5000, flush_interval=10_000, jitter_interval=2_000, retry_interval=5_000)

    write_api = writeHandler.write_api(write_options=options)
    write_api.write(bucket=bucket, record=records)
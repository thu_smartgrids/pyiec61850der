# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 13:27:23 2023

@author: chen
"""


import logging
import pandas as pd
from configurator import helper
from interface.runtime import Profile
import copy

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)

def getProfile(startTimeStrUTC=None, instProfile=None, timeRange=86400, interval=60, **kwargs):
    """
    This method will get the PV/load profile from local csv file if called.
    The profile will be queried on a daily basis.
    
    In the function name, we use the word get instead of load to avoid confusion
    with electrical load profile.
    
    Usage:
        newProfile = interface.local.getProfile(startTimeStrUTC, instProfile, timeRange=86400, interval=60)
    
    Input:
        - startTimeStrUTC: UTC time string of the start time, which can be read from one instance of class Config
        - instProfile: the instance of the profile that contains all available data (created by initProfile)
        - timeRange: this defines the time window (unit is second) of the desired profile, usually 3600, 7200 or 86400
        - interval: time resolution (unit is second) of the profile. It could be automatically calculated, here we use it to detect data gap
        - **kwargs: placeholder for other supplementary keywords argument
        
    Output;
        - newProfile: return a new profile including the dfVal. The profile instance needs to be updated
            accordingly.    
    
    """
    
    logger.info('=================   Begin update profile   =================') 
    logger.info(f'Start profile query, start time stamp {startTimeStrUTC}, time range {timeRange} seconds')    
    
    endTimeStrUTC = helper.unixTstmpToString(helper.utcTstmpToUnix(startTimeStrUTC) + timeRange)
    
    if instProfile.dfVal.loc[0, '_time'][0:10] == startTimeStrUTC[0:10]:
        startIdx = 0  # this deals with the case in which the simulation begins in the middle of a day
    else:
        startIdx = instProfile.dfVal.index[instProfile.dfVal['_time'] == startTimeStrUTC].tolist()
        if startIdx == []:
            logger.warning('Required start time not in the dataframe, can not query new profile')
            startIdx = None
        else:
            startIdx = startIdx[0]
    
    endIdx = instProfile.dfVal.index[instProfile.dfVal['_time'] == endTimeStrUTC].tolist()

    if endIdx == []:
        logger.warning('Required start time not in the dataframe, can not query new profile')
        endIdx = None
    else:
        endIdx = endIdx[0]    
    
    if startIdx is None and endIdx is None:
        dfNew = instProfile.dfVal[-1:0]  # this returns a empty dataframe with only column keys
    else:
        dfNew = instProfile.dfVal[startIdx:endIdx]
    
    nValRef = timeRange / interval
    if  len(dfNew) == 0:
        logger.warning('No data found for this day, the simulation will use profile of the previous day')
    elif len(dfNew) > nValRef:
        logger.warning('Found some duplicated data, but no problem, we move on')
    elif len(dfNew) < nValRef:
        logger.warning(f'Found data gap, {nValRef - len(dfNew)} data points are missing.')
        logger.warning('Not a problem, the server will use the previous value in the simulation')
    else:
        logger.info('Data query was successfull, profile loaded')
    
    logger.info('=================   End update profile   =================\n') 
    
    nVal = len(dfNew)
    newProfile = Profile()
    newProfile.GUID = instProfile.GUID
    newProfile.provider = instProfile.provider
    newProfile.dfVal = dfNew
    newProfile.nVal = nVal
    newProfile.timeWindow = (dfNew.loc[0, '_time'], dfNew.loc[nVal-1, '_time'])
    newProfile.timeWindowUnix = (dfNew.loc[0, '_timeUnix'], dfNew.loc[nVal-1, '_timeUnix'])
    
    return newProfile

# TODO: evaluate whether it is helpful to init the entire profile, maybe it is better
#   to recursively load local profiles in each iteration

def initProfile(filename=None, colPara='Value', colTime='Datetime', 
                timeStrFormat='%Y-%m-%d %H:%M:%S', startTimeStr=None, endTimeStr=None, **kwargs):
    """
    This method will get the PV/load profile locally from a pre-configed csv 
    file in the current working directory.
    
    The profile will be loaded only once by the initialization and will be used
    throughout the entire simulation.
    
    Currently we handle one parameter per profile, in otherwords, if a csv file containts 10 different measurements,
    then 10 profiles need to be initialized separately. 
    TODO: make the parameter colPara a list or tuple to load multiple parameters in one function.
    
    Usage:
        newProfile = interface.local.initProfile(filename, colPara, colTime, 
                                                 timeStrFormat='%Y-%m-%d %H:%M:%S', startTimeStr=None, endTimeStr=None)
        
    Input:
        - filename: name of the local csv file, including extension
        - colPara: column name in the csv file that contains the data
        - colTime: column name in the csv file that contains the string timestamps
        - timeStrFormat: format of time string, default '%Y-%m-%d %H:%M:%S'
        - startTimeStr: start time string of the profile time window, None means starting at the row 0
        - endTimeStr: end time string of the profile time window, None means ending at the last row
        - **kwargs: placeholder for other supplementary keywords argument
    
    Output;
        - Return the newProfile instance, in which the time column will always have the name '_time'. Additionally
            the unix time (float) will be stored in the column 'UNIX'
            
    """
    
    STANDARD_TIME_STRING = '%Y-%m-%d %H:%M:%S'
    newProfile = Profile()
    logger.info('=================   Begin local profile initialization    =================') 
    dfProfile = pd.read_csv(f'./data/profile/{filename}', sep=',')  #
    
    # TODO: this method is too slow, consider use Unix float time to read the profile data.
    
    if startTimeStr is None:
        idxStartTime = 0
    else:
        if timeStrFormat != STANDARD_TIME_STRING:
            logger.info('The time string in local profile does not match with standard format, reform of time string')
            logger.info('Convert startTimeStr and endTimeStr for indexing')
            startTimeStr =  helper.convertTimeStrFormat(startTimeStr, STANDARD_TIME_STRING, timeStrFormat)
            endTimeStr = helper.convertTimeStrFormat(endTimeStr, STANDARD_TIME_STRING, timeStrFormat)
            
        idxStartTime = dfProfile.index[dfProfile[colTime] == startTimeStr].tolist()
        idxEndTime = dfProfile.index[dfProfile[colTime] == endTimeStr].tolist()
        if idxStartTime == []:
            logger.warning(f'Start time {startTimeStr} not in the profile list, load the profile from row 0')
            idxStartTime = 0
        else:
            idxStartTime = idxStartTime[0]
        if idxEndTime == []:
            logger.warning(f'End time {endTimeStr} not in the profile list, load the profile till the last row')
            idxEndTime = -1
        else:
            idxEndTime = idxEndTime[0]

    dfProfileSelected = dfProfile[idxStartTime:idxEndTime]      
    nVal = len(dfProfileSelected)

    if 'UNIX' not in list(dfProfileSelected.keys()):  # TODO: the column name _timeUnix is not standardized!!
        listTimestampUnix = [helper.utcTstmpToUnix(tstmp, timeStrFormat) for tstmp in list(dfProfileSelected[colTime])]
        dfProfileSelected['_timeUnix'] = listTimestampUnix
    else:
        dfProfileSelected = dfProfileSelected.rename(columns = {'UNIX':'_timeUnix'})
    
    if colTime != '_time':
        dfProfileSelected = dfProfileSelected.rename(columns = {colTime:'_time'})
    if colPara != '_value':
        dfProfileSelected = dfProfileSelected.rename(columns = {colPara:'_value'})
    
    dfProfileSelected = dfProfileSelected.reset_index(drop=True)

    
    logger.info('-------------------------------------------------------------------')
    logger.info(f'Local profile {filename} has been loaded')
    logger.info(f'The selected time range contains {nVal} valid timestamps')
    logger.info('-------------------------------------------------------------------\n')
    
    if timeStrFormat != STANDARD_TIME_STRING:
        for idx in dfProfileSelected.index.tolist():
            dfProfileSelected.loc[idx, '_time'] = helper.convertTimeStrFormat(dfProfileSelected.loc[idx, '_time'],
                                                                              timeStrFormat, STANDARD_TIME_STRING)     
    newProfile.provider = filename
    if helper.isValidGUID(filename.replace('.csv','')):
        newProfile.GUID = filename.replace('.csv','')
    newProfile.dfVal = dfProfileSelected
    newProfile.nVal = nVal
    newProfile.timeWindow = (dfProfileSelected.loc[0, '_time'], dfProfileSelected.loc[nVal-1, '_time'])
    newProfile.timeWindowUnix = (dfProfileSelected.loc[0, '_timeUnix'], dfProfileSelected.loc[nVal-1, '_timeUnix'])
    
    logger.info('=================   End local profile initialization    =================\n') 
    
    return newProfile
    
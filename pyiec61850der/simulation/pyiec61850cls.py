# -*- coding: utf-8 -*-

"""
Created on Sun Aug 22 22:44:01 2021

@author: Chen
"""

# standard built-in lib
import sys
import os
import time
import random
import calendar
from datetime import datetime
import json
import logging
import threading
from multiprocessing.pool import ThreadPool

# standard lib requiring installation
import pandas as pd
import numpy as np
from influxdb_client import InfluxDBClient, Point, WritePrecision, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS

# local lib
from configurator.logging import rotatingLogger
from processing.routine import updateDataBuffer
from interface.iec61850MMS import iedControlWatchdog
import configurator.helper as helper
import configurator.config as config
import model
import communication.iec61850Server as iec61850Server
import interface
import processing

if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850
else:
    from pylibiec61850.windows import iec61850

from configurator.logging import rotatingLogger

logger = rotatingLogger(__name__)


def updateServerTime(instConfig=None, isRoundup=False):
    if isRoundup:  # do a rounding trick to make sure the time will be HH:MM:00
        instConfig.container.cTime = helper.roundTimeString(instConfig.container.cTimeUnix)
    else:
        instConfig.container.cTime = helper.unixTstmpToString(instConfig.container.cTimeUnix)
    instConfig.container.cTimeUTC = helper.getUTCStrByTimezone(instConfig.container.cTime,
                                                               localTimezone=instConfig.container.timezone)


def syncServerTime(instConfig=None):
    instConfig.container.timeSyncCounter = 0
    cTime = helper.getCurrentTimestamp()
    instConfig.container.cTimeUnix = cTime[0]
    updateServerTime(instConfig)


def uploadToInfluxdb(dataBuffer):
    dataBuffer.uploadStatisticsToInfluxdb(dataBuffer.influxdb.bucketWrite, dataBuffer.influxdb.measurementWrite)


def influxdbConnObj(instConfig=None):
    """
    Initialize or refresh the influxdb interface object
    """
    # influxdb interface initialisation
    logger.info('-------------------------------------------------------------------')
    logger.info('Start influxdb interface initialisation')

    filenameSecret = 'influxdb.json'
    isReadSecret = config.readSecret(instConfig, filenameSecret)
    if isReadSecret:
        logger.info('Influxdb configuration successfully loaded, now check the connection to db')
        try:
            logger.info('Try to initialize influxdb handler for data reading')
            clientInfluxdbRead = interface.influxdb.connectInfluxdb(instConfig.influxdb.urlRead,
                                                                    instConfig.influxdb.orgRead,
                                                                    instConfig.influxdb.tokenRead)
            instConfig.influxdb.clientInfluxdbRead = clientInfluxdbRead
        except:
            logger.exception('Connecting to influxdb failed, use local profile instead for loading profiles')
            instConfig.CLS.dataSource = 'local'

        try:
            logger.info('Try to initialize influxdb handler for data uploading')
            clientInfluxdbWrite = interface.influxdb.connectInfluxdb(instConfig.influxdb.urlWrite,
                                                                     instConfig.influxdb.orgWrite,
                                                                     instConfig.influxdb.tokenWrite)

            instConfig.influxdb.clientInfluxdbWrite = clientInfluxdbWrite

        except:
            logger.exception('Connecting to influxdb failed, upload dataBuffer statistics may not work properly')

    else:
        logger.warning('Loading influxdb configuration failed, use local profile instead')

    logger.info('End of influxdb interface initialisation')
    logger.info('-------------------------------------------------------------------\n')


def clsInitialization(instConfig=None):
    """
    Initialize the CLS application (aka the virtual IEC 61850 IED Server)
    """

    # config initialisation
    instConfig.container.cTime = instConfig.container.startTime
    instConfig.container.cTimeUTC = helper.getUTCStrByTimezone(instConfig.container.cTime,
                                                               localTimezone=instConfig.container.timezone)
    instConfig.container.cTimeUnix = instConfig.container.startTimeUnix
    logger.info(f'Initialse the IEC 61850 server with the start Time {instConfig.container.startTime}')
    influxdbConnObj(instConfig)

    pathSCL = instConfig.interface.pathSCL
    if pathSCL is None:
        # dynamic IEC 61850 data model generation
        dmGen = model.IEC61850DataModelGenerator(pathConfigFile=instConfig.interface.pathConfigDataModel,
                                                 nameIED=instConfig.CLS.nameIED)
        [pathOutputSCL, dictOutputSCL] = dmGen.main(readConfigYAML=True, configYAML=instConfig.interface.dictConfigYAML)
        instConfig.interface.pathSCL = pathOutputSCL
    else:
        instConfig.interface.pathSCL = pathSCL
        config.updateListDER(instConfig)
        nameSCL = os.path.basename(pathSCL)
        instConfig.container.serviceName = nameSCL.split('.')[0]
        instConfig.container.containerName = nameSCL.split('.')[0]

    # IEC 61850 communication server initialisation
    # following steps are necessary because the class IEC61850ServerMMS has no connection to instConfig
    # TODO: implement a method in model generator to establish a automatic link between the two objects instConfig and iedServer,
    #   it will mainly be used to exchange configuration information and object references.
    iedServer = iec61850Server.runIedServerMMS(instConfig)
    iedServer.configServer.container = instConfig.container  # synchronise the server configuration with global config (mainly about the time setting)
    iedServer.configServer.der = instConfig.der
    if pathSCL is None:
        iedServer.configServer.der.dictListDER = dmGen.Config.dictListDER

    instConfig.interface.pathLookupCSV = iedServer.pathLookupCSV
    instConfig.interface.dfLookupCSV = iedServer.dfLookupCSV
    instConfig.interface.dfLookupActive = iedServer.dfLookupActive

    # sunspec interface
    interface.sunspec.initSunspecInterface(instConfig, iedServer)

    # distribute the influxdb handler despite of the configured dataSource
    interface.influxdb.distributeInfluxdbHandler(iedServer, instConfig)

    # initialising the values of all DataBuffer instances
    processing.routine.initDataBuffer(instConfig, iedServer, instConfig.container.cTime, verbose=True)

    # update dataBuffer once to avoid unexpected control immediately after the initialization
    processing.routine.updateDataBuffer(instConfig, iedServer, instConfig.container.cTime, verbose=True)

    return iedServer, instConfig


def updateDB(instConfig, iedServer):
    tic = time.perf_counter()
    cTime = instConfig.container.cTime
    try:
        updateDataBuffer(instConfig, iedServer, cTime, verbose=True)
        logger.info('Update measurement was successful.')
    except AttributeError:
        logger.exception('Update measurement failed, probably bad database handler, wait for the next iteration')
    except:
        logger.exception('Unknown error occurred when updating the dataBuffer')
    toc = time.perf_counter()

    return toc - tic


def controlWD(instConfig, iedServer, interval):
    tic = time.perf_counter()
    nIteration = instConfig.CLS.nControlIteration
    intervalControl = (interval - instConfig.CLS.timeMonitorReserve) / nIteration

    iedControlWatchdog(instConfig, iedServer, nIteration, intervalControl, verbose=False)
    toc = time.perf_counter()

    return toc - tic


def otherTasks(instConfig, iedServer, interval, timeRoutine, secondsInRoutineNew, isNewRoutine, newRoutineTimer):
    time.sleep(interval * 0.2)
    tic = time.perf_counter()
    timeUpload = instConfig.CLS.timeTriggerDataUpload

    flagDataExport = False
    flagDataUpload = False

    if instConfig.container.routineCounter == 0:
        pass
    else:
        if instConfig.container.cTimeUnix % timeRoutine < interval:
            flagDataExport = True
        elif instConfig.container.cTimeUnix % timeRoutine > timeRoutine - interval:
            flagDataExport = True

        if instConfig.container.cTimeUnix % timeUpload < interval:
            flagDataUpload = True
        elif instConfig.container.cTimeUnix % timeUpload > timeUpload - interval:
            flagDataUpload = True

    #####################################   thread 4 - trigger data export/upload   ############################
    if flagDataExport:
        logger.info('-------------------------------------------------------------------')
        logger.info('Export CSV to local data storage has been triggered.')
        for dataBuffer in iedServer.listDataBuffer:
            dataBuffer.exportStatisticsLocal()
        logger.info('Export CSV to local data storage is completed.')
        logger.info('-------------------------------------------------------------------\n')
    if flagDataUpload:
        logger.info('-------------------------------------------------------------------')
        logger.info('Upload dataBuffer statistics to remote database influxdb has been triggered.')

        threads = []
        for dataBuffer in iedServer.listDataBuffer:
            newThread = threading.Thread(target=uploadToInfluxdb, args=(dataBuffer,))
            threads.append(newThread)
            newThread.start()

        for t in threads:
            t.join()

        logger.info('Upload dataBuffer statistics to remote database influxdb is completed.')
        logger.info('-------------------------------------------------------------------\n')

    #####################################   thread 5 - refresh influx connObj   ##############################
    # criteria for refresh: every 4 hours do one refresh (Otherwise it may cause "Max retries exceeded with url" error when posting data to influxdb )
    logger.info('-------------------------------------------------------------------')
    logger.info(
        f'Check running time of the influxdb connection object, current connObj count {instConfig.influxdb.influxdbConnCounter}.')

    if secondsInRoutineNew // (
            60 * 60 * 4) > instConfig.influxdb.influxdbConnCounter:  # influxdb conn obj has been up for 4 hours
        logger.info('The influxdb connection object needs to be restarted.')
        instConfig.influxdb.influxdbConnCounter += 1
        try:
            instConfig.influxdb.clientInfluxdbRead.close()
            instConfig.influxdb.clientInfluxdbWrite.close()
            logger.info('Successfully closed open influxdb connection')
        except:
            logger.exception('Influxdb connection can not be closed as it already had a problem, just refresh it.')

        influxdbConnObj(instConfig)
        interface.influxdb.distributeInfluxdbHandler(iedServer, instConfig)
    else:
        logger.info(
            f'The influxdb connObj has been up for {secondsInRoutineNew % (60 * 60 * 4)} seconds, keep going.')
    logger.info('-------------------------------------------------------------------\n')

    #####################################   thread 6 - check service restarter   ###########################
    logger.info('-------------------------------------------------------------------')
    # criteria for restart is -> cTime == 8am (This is because lost of SunSpec communication over night could be faulty)
    logger.info(
        f'Check routine restart trigger. Is new routine? -> {isNewRoutine}; Accumulated time Vs. routine timer {secondsInRoutineNew} s Vs. {newRoutineTimer} s')
    if secondsInRoutineNew > newRoutineTimer and isNewRoutine:  # its after 8 am and no restart has been triggered, then do it now
        logger.info('The IEC 61850 server needs to be restarted.')
        isNewRoutine = False
        instConfig.container.isAbort = True
    logger.info('-------------------------------------------------------------------\n')

    toc = time.perf_counter()

    return toc - tic


def clsRoutine(instConfig=None, iedServer=None):
    """
    This function performs a routine for the simulation using IEC 61850 compliant CLS application.
    The time management is kind of messed up here, following features should be implemented in the near future:
        TODO: split the subprocess into sub-functions and struct the clsRoutine more clearly
        TODO: find a way to synchronize the time between different services, perhaps using a TimeManager class.
        TODO: consider use the parameter routineStartTime to trigger the server_routine, so that all containers start at the same time.
        TODO: implement a method to accelarate the control watchdog in case the execution requires more time due to bad communication quality
        TODO: simply adding up the processing time to the timer may cause time mismatch, consider use actual time in real-time simulation
        TODO: the function syncServerTime set instConfig.container.cTimeUnix to actual UTC0 time, can not sync time in simulation mode
    """

    #####################################   service initialisation ###################################

    isNewRoutine = False
    routineDuration = 60 * 60 * 24  # one day is considered a routine cycle
    newRoutineTimer = 60 * 60 * 8  # restart the service at 8 am
    secondsInRoutine = instConfig.container.cTimeUnix % routineDuration
    instConfig.influxdb.influxdbConnCounter = secondsInRoutine // (60 * 60 * 4)
    instConfig.container.routineCounter = 0
    instConfig.container.timeSyncCounter = 0
    overtimeOffset = 0  # offset in seconds for fixing time interval mismatch

    # perform a time synchronisation before the routine starts
    instConfig.container.cTimeUnix = time.time()
    syncServerTime(instConfig)

    while instConfig.container.cTimeUnix < instConfig.container.endTimeUnix and instConfig.container.isAbort is False:

        #####################################   thread 1 - time setting  ###################################
        tic_iteration = time.perf_counter()  # use this time to compensate the processing time in each iteration.
        interval = instConfig.CLS.interval - overtimeOffset
        timeRoutine = instConfig.CLS.timeTriggerRoutine

        logger.info('-------------------------------------------------------------------')
        logger.info("Server running")
        logger.info(
            f'Actual current datetime: {instConfig.container.cTime}; current Unix time: {instConfig.container.cTimeUnix}')
        updateServerTime(instConfig)
        logger.info(f'Current datetime string for data query: {instConfig.container.cTime}')
        secondsInRoutineNew = instConfig.container.cTimeUnix % routineDuration

        if secondsInRoutineNew < secondsInRoutine:  # indicates the start of a new day
            logger.info(f'Accumulated time in the current routine changed from {secondsInRoutine} s -> {secondsInRoutineNew} s')
            isNewRoutine = True
        else:
            logger.info(
                f'Accumulated time in the current routine increased {secondsInRoutine} s -> {secondsInRoutineNew} s')
            logger.info('Still in the same processing routine.')
        secondsInRoutine = secondsInRoutineNew

        #####################################  here starts the multithreading pool  ##################################
        pool = ThreadPool(processes=3)
        results = []

        #####################################   thread 2 - update dataBuffer   ###################################
        results.append(pool.apply_async(updateDB, args=(instConfig, iedServer)))

        #####################################   thread 3 - release control watchdog  ##############################

        results.append(pool.apply_async(controlWD, args=(instConfig, iedServer, interval)))

        #####################################   thread 4-6 - other tasks  ######################################
        results.append(pool.apply_async(otherTasks, args=(instConfig, iedServer, interval, timeRoutine,
                                                          secondsInRoutineNew, isNewRoutine, newRoutineTimer)))

        pool.close()
        pool.join()

        instConfig.container.routineCounter += 1
        instConfig.container.timeSyncCounter += 1
        logger.info(f"All tasks accomplished in routine with index {instConfig.container.routineCounter} ")

        #####################################   thread 7 - time synchronisation   ##############################
        toc_iteration = time.perf_counter()
        updatebufferTime = results[0].get()
        controlTime = results[1].get()
        otherTime = results[2].get()
        iterationTime = toc_iteration - tic_iteration

        logger.info(f'This iteratio started with an offset of: {overtimeOffset} seconds')
        logger.info(f'Time consumed by the subprocess update databuffer: {updatebufferTime} seconds')
        logger.info(f'Time consumed by the subprocess control watchdog: {controlTime} seconds')
        logger.info(f'Additional processing time for other tasks: {otherTime} seconds')
        logger.info(f'Total duration for the current iteration: {iterationTime} seconds')

        if iterationTime > interval:
            if iterationTime < 2 * interval:  # correct the overtime offset in the next iteration
                overtimeOffset = iterationTime - interval
                logger.info(
                    f'This iteration is {overtimeOffset} seconds overtime, this will be wiped out in the next one')
            else:  # just skip to the next possible iteration
                overtimeOffset = 0
                logger.warning(
                    'This iteration took too much time: {iterationTime} s, skip direct to the next valid iteration.')
                nIntervals = iterationTime // interval
                bufferTime = iterationTime % interval
                time.sleep(interval - bufferTime)  # fill up the time diff
                instConfig.container.cTimeUnix += interval * (
                            nIntervals + 1)  # TODO: might this cause time mismatch if the interval is not 60s?
                updateServerTime(instConfig)
        else:
            overtimeOffset = 0
            totalTime = time.perf_counter() - tic_iteration
            instConfig.container.cTimeUnix += interval
            updateServerTime(instConfig)
            logger.info(f'Totel duration of the current iteration: {iterationTime} seconds')
            logger.info(f'Everything fine, prepare for the next iteration')
            logger.info('Let the time sleep to round up for pre-configured interval, and move to the next iteration')
            if totalTime > interval:
                logger.info(
                    'The last subprocess might have caused the total time to exceed the interval for a couple of ms, ignore them')
            else:
                time.sleep(interval - totalTime)

        if instConfig.container.timeSyncCounter >= 5 * int(
                60 / instConfig.CLS.interval):  # synchronize time every 5 minutes
            syncServerTime(instConfig)

        logger.info('-------------------------------------------------------------------\n')

    return instConfig.container.isAbort


def runVirtualCLS(pathConfig: str, routineTimeout: int):
    """
    We use this function to intialise a virtual CLS application (IED server) using only the SCL file.
    Further configuration can be and should be done by modifying the config YAML file.

    # TODO: consider use binary values to represent cls routine status
    -------------------------------------------------------------------
    cls routine status indicators:
        1 - isRunning
        2 - isRestart
        3 - isAbort
    clsConfig.container.status = isRunning*2 + isRestart*2^2 + isAbort*2^3
    -------------------------------------------------------------------

    """

    helper.setWorkDir()

    # nameLogger = 'pyiec61850clsLogger'
    # logger = rotatingLogger(nameLogger)

    def initClsConfig(pathConfig=None):
        clsConfig = config.Config()
        config.updateConfig(clsConfig, pathConfig)
        config.displayConfigInfo(clsConfig)
        config.initContainerTime(clsConfig)
        isInitialized = True
        return clsConfig, isInitialized

    [clsConfig, isInitialized] = initClsConfig(pathConfig)
    clsConfig.container.isRunning = True
    clsConfig.container.isRestart = False
    clsConfig.container.isAbort = False
    nRoutine = 0

    while clsConfig.container.isRunning:
        logger.info(f'Start routine with index {nRoutine}')
        [iedServer, clsConfig] = clsInitialization(clsConfig)
        # to avoid failures where the CLS server core gets stucked due to communication delay, let it restart each day at 8 am UTC
        clsConfig.container.isAbort = clsRoutine(clsConfig, iedServer)

        if clsConfig.container.isAbort:
            logger.info('-------------------------------------------------------------------\n')
            logger.info(
                'It is 8 am now, hope the inverter slept well! Now give him a cup of coffee by triggering a service restart.')
            logger.info('Destroy running iedServer and the clsConfig instance.')
            clsConfig = None
            iedServer = None
            clsConfig.container.isRestart = True

            logger.info('-------------------------------------------------------------------\n')

        if clsConfig.container.isRestart:
            clsConfig.container.isRestart = False
            logger.info('-------------------------------------------------------------------\n')
            logger.info('Re-initialise the clsConfig and restart the IEC 61850 server.')
            [clsConfig, isInitialized] = initClsConfig(pathConfig)
            [iedServer, clsConfig] = clsInitialization(clsConfig)
            logger.info('Start a new CLS routine.')
            logger.info('-------------------------------------------------------------------\n')
            clsConfig.container.isAbort = clsRoutine(clsConfig, iedServer)

        nRoutine += 1

        print('*******************************************')
        print('*******************************************')
        print('*******************************************')
        if routineTimeout > 0 and nRoutine >= routineTimeout:
            clsConfig.container.isRunning = False
            clsConfig.container.isRestart = False
            clsConfig.container.isAbort = True
            clsConfig = None
            iedServer = None

    return clsConfig, iedServer






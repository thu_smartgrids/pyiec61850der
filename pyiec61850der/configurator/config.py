# -*- coding: utf-8 -*-
"""
Created on Tue Feb 28 15:31:43 2023

@author: chen
"""

import json
import copy
import logging
from datetime import datetime, timedelta, timezone
import time
import calendar
import os
import sys
import yaml
import configurator.helper as helper

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)

class Config(object):
    """
    The parameters in the Config class will be used in many other functions/methods
    of pyiec61850DER, which is quite a bad idea. Well for now we have to more or less live with it.
    
    The point here is: try to avoid changing existing configurator attributes, as you have no idea in
    which part of other scripts that particular attribute will be called.
    
    Most of the config parameters have a default value when initializing an instance, this makes sure
    even if you don't proceed further parameterisation, the virtual CLS can still use some dummy info 
    to start the service.
    
    Update of the configuraiton can be done using the function updateConfig(). It is recommended to 
    update the configuration using the YAML config file, because the IEC 61850 data 
    model generator also relies on this YAML configuration when adjusting the data model topology.
    
    To distinguish the configuration class and instances, attribute names in lower case are used overall
    except CLS (cls is system var for class)
    
    TODO: add detailed description later
    
    TODO: too many interaction among the sub-modules, use a function tracer to automatically generate sequence diagram?
    
    """


    def __init__(self):
        """
        initialise config class templates.
        TODO: there are still some old configurations in this class, check and remove them later.
        """

        class DER(object):
            nPV = 1
            nLOAD = 1
            nSTO = 1
            dictListDER = {'PV': ['PV'], 
                          'LOAD': ['LOAD'],
                          'STO': ['STO'],
                          }
            
        class CLS(object):
            # server settings and stats
            port = None
            localTcpPort = 61850  # this should always be 61850, do not overwrite it with the config file!!
            hostname = "127.0.0.1"
            dataSource = 'local'  # local, influxdb or random
            
            # IED setting
            nameIED = "demoVirtualCLS"
            #DeviceName = "demoProsumerXY"
            derType = 'pv'
            clsGUID = ''
            filenameProfile = '0a45182b-2f4a-4a2f-b136-91cd182a457e.csv'
            interval = 60
            nControlIteration = 10
            timeTriggerRoutine = 900
            timeTriggerDataUpload = 3600
        
        class Container(object):
            # container setting
            containerGUID = None
            serviceName = None
            containerName= None
            network = None
            routineStartTime = None
            port = None
            
            
            # time settings
            timeMode = 'simulation'  # simulation or absolute
            startTime = '2016-07-01T12:00:00'
            endTime = '2016-07-31T23:59:59'
            startTimeUnix = 0
            endTimeUnix = 0
            timeAccelarationFactor = 1
            cTimeUnix = 0
            cTime = None
            cTimeUTC = None
            timezone = 'Europe/Berlin'
            
            # global counter
            gcount = 0
        
        
        class Influxdb(object):
            measurementRead = 'unknown'  # string of the _measurement for reading from influxdb
            measurementWrite = 'unknown'  # string of the _measurement for writing to influxdb
            tagInfluxdb = {}
            bucketRead = None
            bucketWrite = None
            clientInfluxdbRead = None
            clientInfluxdbWrite = None
            tokenRead = None
            tokenWrite = None
            orgRead = None
            urlRead =None
            orgWrite = None
            urlWrite = None
        
        class PV(object):
            dataSource = 'local'
            ratedPower = 100
            unitFactor = 1
            scaleFactor = 1
            inverterIP = None
            inverterSlaveID = None
            tcpPort = None
            GUID = None
    
        
        class PCC(object):
            placeholder = None
    
    
        class STO(object):
            placeholder = None
            dataSource = None
            
        class LOAD(object):
            placeholder = None
            dataSource = None
        
        class Interface(object):
            # dictionary and pandas DataFrame objects
            dfLookupCSV = None  # initialisation will be performed by interface.sunspec
            dfSunspecMapping = None  # initialisation will be performed by communication.iec61850Server
            dictConfigYAML = None  # initialisation will be performed by configurator.config.updateConfig()
            
            # paths relevant for file i/o and communication interfaces
            pathConfigYAML = './configurator/config.yaml'
            pathConfigDataModel = './model/config_IEC61850_model.xlsx'
            pathLookupCSV = './configurator/IEC61850_DA_lookup_demoIED.csv'
            pathSunspecMapping = './interface/interface_SunSpec.xlsx'
            pathSunspecMappingExport = './interface/interface_SunSpec_export.xlsx'
            pathSCL = None
            keepNonePara = True
            listReadLookupColumn = ['index', 'dataSource', 'configJSON', 'GUID', 'isMonitor', 'isControl']
            

        self.der = DER()    
        self.container = Container()    
        self.influxdb = Influxdb()
        self.CLS = CLS()
        self.pv = PV()
        self.load = LOAD()
        self.sto = STO()
        self.pcc = PCC()
        self.interface = Interface()


def updateConfig(instConfig=None, pathConfig=None, addNewAttribute=True):
    """
    This function has the job to load a config file and update the attributes
    in the config instance (the second argument), accordingly.

    For a better readability, this global configuration file has now YAML format 
    instead of JSON. It facilitates the harmonisation with container applications.
    """
    
    logger.info('=================   Begin load local config   =================')
    if pathConfig is None:
        pathConfigFile = instConfig.interface.pathConfigYAML
    else:
        pathConfigFile = pathConfig
    with open(pathConfigFile, "r") as stream:
        try:
            configLocal = yaml.safe_load(stream)
            # Before proceeding to the config update, first parse the config file and reconstruct the 
            #   instance of the Config class -> duplicate pv, 
            
            if configLocal is None:
                logger.warning('YAML config file is empty, can not proceed, no change in the DER config will be made.')
            elif type(configLocal) is not dict:
                logger.warning('YAML config has unknown type, can not proceed, no change in the DER config will be made.')
            else:
                for key in ['PV', 'LOAD', 'STO']:
                    listUpdate = [item for item in configLocal if key in item]
                    if len(listUpdate) > 0:
                        for item in listUpdate:
                            setattr(instConfig, item.lower(), getattr(instConfig, key.lower()))
                        delattr(instConfig, key.lower())
                        
                for configKind in list(configLocal.keys()):
                    if configKind == 'CLS':  # CLS is an exception because cls is system var
                        configClass = configKind
                    else:
                        configClass = configKind.lower() # all other classes use lower case name
                    for key, value in configLocal[configKind].items():
                        objConfig = getattr(instConfig, configClass)
                        if key not in dir(objConfig):
                            if addNewAttribute:
                                setattr(objConfig, key, value)
                                logger.info(f'Unknown config parameter {key} has been added to the config instance {configClass}')
                            else:
                                logger.warning(f'Unknown config parameter {key} detected, skip it')
                        else:
                            setattr(objConfig, key, value)
                instConfig.interface.dictConfigYAML = configLocal
                
        except yaml.YAMLError as errorCode:
            logger.exception(f'The config YAML can not be loaded, error code: {errorCode}')
            logger.warning(f'The programm will proceed using default configuration, which would cause errors!!!')
    logger.info('=================   End load local config   =================\n')    


def readSecret(instConfig=None, filename=None, addNewAttribute=True):
    """
    TODO: update desc
    
    load the secrets for data interfaces.
    currently we only need this for influxdb.
    All secret files must be located in the folder 'secret', the filename must include it extensions, in most cases it would
    be a JSON file.
    This function uses the same logic as the function updateConfig, well for security reasons we should not consolidate them.
    """
    
    isReadSecret = True
    configClass = filename.split('.')[0]
    
    if filename not in os.listdir('./secret'):
        logger.error(f'Secret file {filename} not found, please check it and restart the program.')
        isReadSecret = False
    elif configClass not in dir(instConfig):
        logger.error(f'The secret file has unknown config class {configClass}, please check it and restart the program.')
    else:
        pathConfig = f'./secret/{filename}'
        secretFile = open(pathConfig)
        secrets = json.load(secretFile)
        objConfig = getattr(instConfig, configClass)
        for key, value in secrets.items():
            if key not in dir(objConfig):
                if addNewAttribute:
                    setattr(objConfig, key, value)
                    logger.info(f'Unknown secret parameter {key} has been added to  config instance')
                else:
                    logger.warning(f'Unknown secret parameter {key} detected, skip it')
            else:
                if value is None or value == '':
                    logger.warning(f'Invalid value of secret parameter {key} detected, please check this parameter')
                    isReadSecret = False
                else:
                    setattr(objConfig, key, value)
    return isReadSecret


def updateListDER(instConfig=None):
    """
    Update the DER list in config object.
    """
    
    if instConfig.interface.dictConfigYAML is None:
        logger.warning('YAML config file is empty, can not proceed, no change in the IED topology will be made.')
    elif type(instConfig.interface.dictConfigYAML) is not dict:
        logger.warning('YAML config has unknown type, can not proceed, no change in the IED topology will be made.')
    else:
        for key in ['PV', 'LOAD', 'STO']:
            listUpdate = [item for item in instConfig.interface.dictConfigYAML if key in item]
            if len(listUpdate) > 0:
                instConfig.der.dictListDER[key] = listUpdate
            else:
                instConfig.der.dictListDER[key] = []
                    
    
def displayConfigInfo(instConfig=None):
    """
    This method can display the current configuration of a running virtual CLS.
    """
    
    logger.info('=================   Begin get DER information   =================')
    # listInfo = ['derType', 'pvRatedPower', 'timeMode', 'startTime', 'endTime', 
    #             'dataSource', 'timeAccelarationFactor', 'scaleFactor','unitFactor',
    #             'clsGUID', 'containerGUID', 'hostname', 'port', 'localTcpPort']
    
    for keyConfig in vars(instConfig).keys():
        item = instConfig.__dict__[keyConfig]
        logger.info(f'-------------  Display <{keyConfig}> configuration  -------------')
        for key, val in vars(item).items():
            logger.info(f'{key}: {val}')
    logger.info('=================   End get DER information   =================\n')

def initContainerTime(instConfig=None, timeStrFormat='%Y-%m-%d %H:%M:%S'):
    logger.info('Start initialising container time.')    
    
    if instConfig.container.timeMode == 'simulation':
        instConfig.container.startTimeUnix = time.mktime(datetime.strptime(instConfig.container.startTime, timeStrFormat).timetuple())
        instConfig.container.endTimeUnix = time.mktime(datetime.strptime(instConfig.container.endTime, timeStrFormat).timetuple())
        
    elif instConfig.container.timeMode == 'absolute':
        instConfig.container.startTimeUnix = helper.getCurrentTimestamp(localTimezone=instConfig.container.timezone, timeOffset=0)[0]
        instConfig.container.endTimeUnix = instConfig.container.startTimeUnix + 60*60*24*365
    
    instConfig.container.startTime = helper.unixTstmpToString(instConfig.container.startTimeUnix)
    instConfig.container.endTime = helper.unixTstmpToString(instConfig.container.endTimeUnix)
    
    logger.info('End initialising container time.')  
    

def setEnv(libiec61850Version='1.4.1'):
    cdir = os.getcwd()
    if os.name != 'nt':  # if not windows os
        sys.path.append(f"{cdir}/")
        # append the folder of libiec61850 to sys and os.environ to ensure that libiec61850 can be loaded
        # it has been proved that these codes would not help, but I just leave them here.
        sys.path.append(f"{cdir}/pylibiec61850/linux/")
        sys.path.append(f"{cdir}/pylibiec61850/linux/libiec61850-{os.environ['LIB_VERSION']}")
        
        listFile = os.listdir(f"{cdir}/")
        logger.info("check files in working directory")
        logger.info(listFile)
        
        # just to be safe, also add lib path to ENV
        os.environ["PATH"] = f"{cdir}/;" + os.environ["PATH"]
        os.environ["PATH"] = f"{cdir}/pylibiec61850/linux/;" + os.environ["PATH"]
        os.environ["PATH"] = f"{cdir}/pylibiec61850/linux/libiec61850-{os.environ['LIB_VERSION']};" + os.environ["PATH"]
        
    else:
        pythonVersion = sys.version[0:3]
        os.environ["PATH"] = fr"{cdir}\;" + os.environ["PATH"]
        os.environ["PATH"] = fr"{cdir}\pylibiec61850\windows\;" + os.environ["PATH"]

    
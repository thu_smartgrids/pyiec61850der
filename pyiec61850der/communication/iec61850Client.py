# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 12:08:20 2020

@author: J. Morris; S. Chen

The Pyiec61850Client() class can be used to initialize IEC 61850 compatible client applicaiton, read measured values from
or write control setpoints to an IEC 61850 server via IEC 61850 MMS communication.

It also supports connection status check of the MMS channel and data model scan.

TODO: implementation of other client side application based on the libIEC61850 client API
is possible, such functionalities might be added here in the future and is also left open for users.

The intialization for pyiec61850 client object usng the libIEC61850 client API 
was taken from a test framework as described in:

J. Morris, F. Ebe, J. Pichl, S. Chen, G. Heilscher, and J.-G. Leeser, 
‘Implementation of an automated test bench for monitoring and controlling systems of 
decentralized energy systems through Controller Hardware-In-the-Loop and 
Power Hardware-In-the-Loop methodology’, in 2020 5th IEEE Workshop on the Electronic Grid (eGRID), 
Aachen, Germany: IEEE, Nov. 2020, pp. 1–8. doi: 10.1109/eGRID48559.2020.9330636.

libiec61850 client error code:
ref: https://support.mz-automation.de/doc/libiec61850/net/latest/namespace_i_e_c61850_1_1_client.html
      IedClientError.IED_ERROR_OK = 0
      IedClientError.IED_ERROR_NOT_CONNECTED = 1
      IedClientError.IED_ERROR_ALREADY_CONNECTED = 2
      IedClientError.IED_ERROR_CONNECTION_LOST = 3
      IedClientError.IED_ERROR_SERVICE_NOT_SUPPORTED = 4
      IedClientError.IED_ERROR_CONNECTION_REJECTED = 5
      IedClientError.IED_ERROR_USER_PROVIDED_INVALID_ARGUMENT = 10
      IedClientError.IED_ERROR_ENABLE_REPORT_FAILED_DATASET_MISMATCH = 11
      IedClientError.IED_ERROR_OBJECT_REFERENCE_INVALID = 12
      IedClientError.IED_ERROR_UNEXPECTED_VALUE_RECEIVED = 13
      IedClientError.IED_ERROR_TIMEOUT = 20
      IedClientError.IED_ERROR_ACCESS_DENIED = 21
      IedClientError.IED_ERROR_OBJECT_DOES_NOT_EXIST = 22
      IedClientError.IED_ERROR_OBJECT_EXISTS = 23
      IedClientError.IED_ERROR_OBJECT_ACCESS_UNSUPPORTED = 24
      IedClientError.IED_ERROR_TYPE_INCONSISTENT = 25,
      IedClientError.IED_ERROR_TEMPORARILY_UNAVAILABLE = 26
      IedClientError.IED_ERROR_OBJECT_UNDEFINED = 27
      IedClientError.IED_ERROR_INVALID_ADDRESS = 28
      IedClientError.IED_ERROR_HARDWARE_FAULT = 29
      IedClientError.IED_ERROR_TYPE_UNSUPPORTED = 30
      IedClientError.IED_ERROR_OBJECT_ATTRIBUTE_INCONSISTENT = 31
      IedClientError.IED_ERROR_OBJECT_VALUE_INVALID = 32
      IedClientError.IED_ERROR_OBJECT_INVALIDATED = 33
      IedClientError.IED_ERROR_MALFORMED_MESSAGE = 34
      IedClientError.IED_ERROR_SERVICE_NOT_IMPLEMENTED = 98
      IED_ERROR_UNKNOWN = 99
"""

import sys
import os
import time

# TODO: add module import check here
if os.name != 'nt':  # if not windows os
    from pylibiec61850.linux import iec61850
else:
    from pylibiec61850.windows import iec61850

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)

class Pyiec61850Client(object):
    '''
    This client class is taken from some demo codes in another project, I'm too lazy to update the stuff here.
    It should be able to interact with a static IEC 61850 server, we do not really need that, just ignore the codes if you find them confusing.
    '''
    
    def __init__(self, ipAddress=None, port=None):
        '''
        Initialsing the cleint object
        Sets connection parameters: TCP Port and host IP address of IEC 61850 Server
        '''

        if ipAddress is None:
            print('No IP address specified, use localhost as default')
            self.ipAddress = '127.0.0.1'
        else:
            self.ipAddress = ipAddress

        if port is None:
            print('No TCP port specified, use localhost as default')
            self.port = 61850
        else:
            self.port = port

        self.connection = None
        self.connTimeout = 10000
        self.error = None
        self.status = None

        self.iedModel = None
        self.allDO = {}
        self.allDA = {}
        self.allDA_MX = {}  # all DA with FC type MX
        self.allDA_SP = {}  # all DA with FC type SP
        self.allRCB = {}  # all Report Control Blocks
        self.listRefRCB = []  # stores the RCB mms address

        self.SP = iec61850.IEC61850_FC_SP
        self.MX = iec61850.IEC61850_FC_MX


    def create_IED_connection(self):
        '''
        Creates IED connection with given conncection parameters
        '''

        isConnected = False
        self.connection = iec61850.IedConnection_create()
        iec61850.IedConnection_setConnectTimeout(self.connection, int(self.connTimeout))
        
        self.error = iec61850.IedConnection_connect(self.connection, self.ipAddress, self.port)

        if self.error == iec61850.IED_ERROR_OK:
            print('IEC 61850 connection established')
            isConnected = True
        elif self.error == iec61850.IED_ERROR_CONNECTION_REJECTED:
            print('IEC 61850 connection rejected')
        else:
            print('IEC 61850 connection failed')

        if isConnected:
            self.status = 'connected'
        else:
            self.status = 'connection error'

        return isConnected


    def getState_IED_connection(self):
        '''
        Connection state of the IedConnection instance (either idle, connected or closed)
        Enumeration litervals for IED status:
            - IED_STATE_IDLE
            - IED_STATE_CONNECTED
            - IED_STATE_CLOSED
        
        Querry State of current IED connection:
            - IED_STATE_CLOSED
            - IED_STATE_CONNECTING
            - IED_STATE_CONNECTED
            - IED_STATE_CLOSING
        '''

        state = iec61850.IedConnection_getState(self.connection)
        stateString = None

        if state == iec61850.IED_STATE_IDLE:
            stateString = "IED_STATE_IDLE"
        elif state == iec61850.IED_STATE_CONNECTED:
            stateString = "IED_STATE_CONNECTED"
        elif state == iec61850.IED_STATE_CLOSED:
            stateString = "IED_STATE_CLOSED"
        else:
            stateString = "ERROR"

        return stateString

    def close_IED_connection(self):
        '''
        Closes current IED connection
        '''

        iec61850.IedConnection_close(self.connection)
        print('IEC 61850 connection closed')

    def terminate_IED_connection(self):
        '''
        Terminates current IED connection
        '''

        iec61850.IedConnection_destroy(self.connection)
        print('IEC 61850 connection terminated')

    def get_srv_model(self, verbose=False):
        '''
        Get the IED data model from the IED server
        Returns IED model, MX and SP data attributes with corresponding keys
        
        TODO: the following logic operator based on number of $ is very floppy, because the existence of SDO 
              and SDA will have an impact on this. But it is just a test script right, why would we care? 
                        
        '''

        isSuccess = False
        self.iedModel = {}
        # MX dictionary: LD/LN$FC$DO($SDO)$DA($SDA1)($SDA2)(...) 
        # () means without garanty of appearance
        # --> example: {'Relay1/MMXU$MX$TotW$mag$f': '0.01'}

        if self.error == iec61850.IED_ERROR_OK:
            [deviceList, error] = iec61850.IedConnection_getLogicalDeviceList(self.connection)
            device = iec61850.LinkedList_getNext(deviceList)
            while device:
                LD_name = iec61850.toCharP(device.data)
                print("LD: %s" % LD_name)
                self.iedModel[LD_name] = {}
                [logicalNodes, self.error] = iec61850.IedConnection_getLogicalDeviceDirectory(self.connection, LD_name)
                logicalNode = iec61850.LinkedList_getNext(logicalNodes)
                while logicalNode:
                    LN_name = iec61850.toCharP(logicalNode.data)
                    if verbose:
                        print(" LN: %s" % LN_name)
                    self.iedModel[LD_name][LN_name] = {}
                    [LNobjects, self.error] = iec61850.IedConnection_getLogicalNodeVariables(self.connection, LD_name+"/"+LN_name)
                    LNobject = iec61850.LinkedList_getNext(LNobjects)
                    while LNobject:
                        VAR_name = iec61850.toCharP(LNobject.data)
                        if verbose:
                            print("  VAR: %s" % VAR_name)
                        self.iedModel[LD_name][LN_name][VAR_name] = {VAR_name}

                        if 'RP' in VAR_name and VAR_name.count('$') == 1:
                            RP_MMS = LD_name+'/'+LN_name+'$'+VAR_name
                            self.listRefRCB.append(RP_MMS)
                            RP_MMS = RP_MMS.replace('$RP', '')
                            RP_key = RP_MMS.replace('$', '.')
                            self.allRCB[RP_key] = ''
                            print(f'Found Report Control Block: {RP_key}')
                            
                        elif VAR_name.count('$') == 1:  # FC$DO($SDO)$DA($SDA1)($SDA2)(...) 
                            DO_MMS = LD_name+'/'+LN_name+'$'+VAR_name
                            FCType = VAR_name.split('$')[0]
                            FCTypeKey = getattr(iec61850, f'IEC61850_FC_{FCType}')
                            DO_MMS = DO_MMS.replace(f'${FCType}', '')
                            DO_key = DO_MMS.replace('$', '.')
                            #DO_val = iec61850.IedConnection_readFloatValue(self.connection, DO_key, FCTypeKey)
                            self.allDO[DO_key] = None
                        
                        elif VAR_name.count('$') == 2:  # FC$DO($SDO)$DA($SDA1)($SDA2)(...)
                            DA_MMS = LD_name+'/'+LN_name+'$'+VAR_name
                            FCType = VAR_name.split('$')[0]
                            FCTypeKey = getattr(iec61850, f'IEC61850_FC_{FCType}')
                            DA_MMS = DA_MMS.replace(f'${FCType}', '')
                            DA_key = DA_MMS.replace('$', '.')
                            #DA_val = iec61850.IedConnection_readFloatValue(self.connection, DA_key, FCTypeKey)
                            self.allDA[DA_key] = None
                            
                        elif VAR_name.count('$') > 2:
                            SDA_MMS = LD_name+'/'+LN_name+'$'+VAR_name
                            FCType = VAR_name.split('$')[0]
                            FCTypeKey = getattr(iec61850, f'IEC61850_FC_{FCType}')
                            SDA_MMS = SDA_MMS.replace(f'${FCType}', '')
                            SDA_key = SDA_MMS.replace('$', '.')
                            if FCType == 'MX' and '.f' in SDA_MMS:
                                SDA_val = iec61850.IedConnection_readFloatValue(self.connection, SDA_key, FCTypeKey)
                                self.allDA_MX[SDA_key] = SDA_val
                                if verbose:
                                    print(f'{SDA_key}: {SDA_val}')
                            elif FCType == 'SP' and '.f' in SDA_MMS:
                                SDA_val = iec61850.IedConnection_readFloatValue(self.connection, SDA_key, FCTypeKey)
                                self.allDA_SP[SDA_key] = SDA_val
                                if verbose:
                                    print(f'{SDA_key}: {SDA_val}')
                            
                        
                            
                        LNobject = iec61850.LinkedList_getNext(LNobject)
                    iec61850.LinkedList_destroy(LNobjects)
                    logicalNode = iec61850.LinkedList_getNext(logicalNode)
                iec61850.LinkedList_destroy(logicalNodes)
                device = iec61850.LinkedList_getNext(device)

            iec61850.LinkedList_destroy(deviceList)

            if self.iedModel != {}:
                print('IEC 61850 server has IED data model')
                print(f'Number of DO in the IED data model: {len(self.allDO)}')
                print(f'Number of DA in the IED data model: {len(self.allDA)}')
                print(f'Number of RCB in the IED data model: {len(self.allRCB)}')
                isSuccess = True
            else:
                print('IED data model transmission failed')
        else:
            print('IEC 61850 connection failed. IED data model transmission failed')

        return isSuccess


    def readFloatValue(self, objRef, fcType='MX', rounding=None):
        '''
        Simple get function: Get current float set point from IED server
        rounding
        '''
        
        fcTypeKey = getattr(iec61850, f'IEC61850_FC_{fcType}')
        [value, errorCode] = iec61850.IedConnection_readFloatValue(self.connection, objRef, fcTypeKey)
        
        if errorCode == 0:
            if type(rounding) is int:
                value = float(format(round(value[0], rounding)))
            print(f'Measurement (IED server: {objRef}): {value}')
        else:
            print(f'Reading float value failed, error code: {errorCode}')
        return value, errorCode


    def writeFloatValue(self, objRef=None, value=None, fcType=None):
        '''
        Simple write function
        '''
        
        fcTypeKey = getattr(iec61850, f'IEC61850_FC_{fcType}')
        errorCode = iec61850.IedConnection_writeFloatValue(self.connection, objRef, fcTypeKey, value)
        
        return errorCode
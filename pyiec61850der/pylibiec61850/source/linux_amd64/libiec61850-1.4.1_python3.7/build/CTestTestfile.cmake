# CMake generated Testfile for 
# Source directory: /work/libiec61850-1.4.1
# Build directory: /work/libiec61850-1.4.1/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("hal")
subdirs("examples")
subdirs("src")
subdirs("pyiec61850")

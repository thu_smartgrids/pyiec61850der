# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 18:12:15 2023

@author: chen
"""

import logging
import numpy as np
import configurator.helper as helper

from configurator.logging import rotatingLogger
logger = rotatingLogger(__name__)

'''
##################################################################
##########     some dummy functions for tests  ###################
##################################################################
'''

def setValToOne():
    val = 1.0
    return val

def counter(val):
    val += 1.0
    return val


'''
##################################################################
##########     IEC 61850 oriented calculation  ###################
##################################################################
'''


'''
For all calculation methods, we assume iedServer, dfLookupActive, and the current dataBuffer instance
are passed as default input args, even if they might not be used.

In this way, in the CSV lookup table, one only needs to define functionHandler and args,
args should be the integer index of a dataBuffer instance


'''

def aggregatePCC(iedServer=None, dataBuffer=None, *args):
    '''
    This function scans all active logical nodes in the data model, aggregates the
    sum of all TotW.mag.f and write it to the DO TotW of the LN "PCC".
    
    The input addressMmsPCC is the MMS address of to be calculated aggregator parameters, e.g.
    active / reactive / apparent power of different DER behind a PCC.
    
    Currently we use simple logic, just iterate over the dfLookupActive dataframe and sum up the values
    of all DO that have the same LN_DO_DA identifier.
    
    TODO: check if LN should be removed because the suffix integer might not always be 1.
    
    TODO: all values are positive, find a way to determine the direction (absorption or injection)
    
    TODO: pass index of target DataBuffer instances as *args?
    '''
    
    dfLookupActive = iedServer.dfLookupActive
    addressMmsPCC = dataBuffer.DO.monitorAddressDA
    parameterIdentifier = addressMmsPCC.split('/')[1]
    valAgg = 0
    for i in range(len(dfLookupActive)):
        if addressMmsPCC == dfLookupActive.loc[i, 'monitorDA']:
            pass
        elif parameterIdentifier in dfLookupActive.loc[i, 'monitorDA']:
            idxBuffer = dfLookupActive.loc[i, 'idxBuffer']
            [idx, thisVal] = iedServer.getDataBufferIndex(idxBuffer)

            if thisVal is None:
                logger.warning(f'Value of DA {iedServer.listDataBuffer[idx].DO.monitorAddressDA} is None, skip it')
            elif not helper.isValNumber(thisVal):
                logger.warning(f'Value of DA {iedServer.listDataBuffer[idx].DO.monitorAddressDA} is not a number, skip it') 
            elif 'PV' in dfLookupActive.loc[i, 'monitorDA']:
                valAgg += -1 * thisVal
            else:
                valAgg += thisVal
            
    return valAgg


def multiplication(iedServer=None, dataBuffer=None, *args:int):
    '''
    This function takes the index of dataBuffer instances and performs a simple multiplication of them all.
    '''
    
    dfLookupActive = iedServer.dfLookupActive
    valMulti = None
    firstMulti = None
    for idxBuffer in args:
        [idx, thisVal] = iedServer.getDataBufferIndex(idxBuffer) 
        
        if thisVal is None:
            logger.warning(f'Value of DA {iedServer.listDataBuffer[idx].DO.monitorAddressDA} is None, skip it')
        elif not helper.isValNumber(thisVal):
            logger.warning(f'Value of DA {iedServer.listDataBuffer[idx].DO.monitorAddressDA} is not a number, skip it')
        else:
            if firstMulti is None:
                firstMulti = thisVal
                valMulti = thisVal
            else:
                valMulti *= thisVal
    
    return valMulti
        

def solarCurtailment():
    '''
    In case no PV inverter is availalbe, this function can be called to represent 
    the effect of PV curtailment. It compares the "measured" value and threhold given
    by the curtailment and take the smaller one as "actual" PV active feed-in power.

    '''
    
    # only a placeholder, this functionality is currently implemented as limits.
    
    pass
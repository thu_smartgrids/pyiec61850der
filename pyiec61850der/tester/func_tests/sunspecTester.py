# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 15:55:19 2023

@author: chen
"""

import sunspec2.modbus.client as client


def SunspecFullScan(ipaddr='192.168.170.90', port=502, slave_id=126):
    d = client.SunSpecModbusClientDeviceTCP(slave_id, ipaddr, port)
    d.scan()
    print(d.models)
    if len(d.models) != 0 or d.models is not None:
        listModelName = [key for key, obj in d.models.items() if type(key) is str]
        listModelID = [key for key, obj in d.models.items() if type(key) is int]
        print(f'List of sunspec model names: {listModelName}')
        print(f'List of sunspec model ID: {listModelID}')
        for key, obj in d.models.items():
            if type(key) is int:
                print(f'Found Sunspec model {key}')
                modelID = key
                points = getattr(d.models[modelID][0], 'points')
                for name, val in points.items():
                    val = getattr(d.models[modelID][0], name).cvalue
                    print(f'Parameter name {name}, value: {val}')


def controlTester(ipaddr='192.168.170.90', port=502, slave_id=126):
    d = client.SunSpecModbusClientDeviceTCP(slave_id, ipaddr, port)
    d.scan()

    modelID = 123

    obj = getattr(d.models[modelID][0], 'WMaxLim_Ena')
    obj.read()
    obj.cvalue
    obj.cvalue = 1
    obj.write()
    obj.read()
    obj.cvalue

    obj = getattr(d.models[modelID][0], 'WMaxLimPct')
    obj.cvalue = 100
    obj.write()
    obj.read()
    obj.cvalue

    modelID = 704
    obj = getattr(d.models[modelID][0], 'WMaxLimPctEna')
    obj.read()
    obj.cvalue
    obj.cvalue = 1
    obj.write()
    obj.read()
    obj.cvalue

    obj = getattr(d.models[modelID][0], 'WMaxLimPct')
    obj.cvalue = 100  # Attention: the scale factor is probaly not working properly!
    obj.write()
    obj.read()
    obj.cvalue


slave_id = 126
ipaddr = '192.168.170.76'
port = 502

SunspecFullScan(ipaddr, port, slave_id)
controlTester(ipaddr, port, slave_id)

#################################################################
#################### IEC 61850 relevante tests  #################
#################################################################

import time
import pandas as pd
from datetime import datetime
from configurator import helper
from configurator import config
import communication.iec61850Server as iec61850Server
import interface
from configurator.logging import rotatingLogger
import model

helper.setWorkDir()
nameLogger = 'sunspecTester'
logger = rotatingLogger(nameLogger)

instConfig = config.Config()
pathConfig = './tester/func_tests/configTester.yaml'
config.updateConfig(instConfig, pathConfig)
config.displayConfigInfo(instConfig)

dmGen = model.IEC61850DataModelGenerator(pathConfigFile=instConfig.interface.pathConfigDataModel,
                                         nameIED=instConfig.CLS.nameIED)
[pathOutputSCL, dictOutputSCL] = dmGen.main(readConfigYAML=True, configYAML=instConfig.interface.dictConfigYAML)
instConfig.interface.pathSCL = pathOutputSCL

iedServer = iec61850Server.runIedServerMMS(instConfig)
iedServer.configServer.container = instConfig.container  # synchronise the server configuration with global config (mainly about the time setting)
iedServer.configServer.der = instConfig.der
iedServer.configServer.der.dictListDER = dmGen.Config.dictListDER
instConfig.interface.pathLookupCSV = iedServer.pathLookupCSV
instConfig.interface.dfLookupCSV = iedServer.dfLookupCSV

interface.sunspec.initSunspecInterface(instConfig, iedServer)


def SunspecMonitorIED(ipaddr='192.168.170.90', port=502, slave_id=126):
    d = client.SunSpecModbusClientDeviceTCP(slave_id, ipaddr, port)
    dfLookup = instConfig.interface.dfLookupCSV
    dfMapping = instConfig.interface.dfSunspecMapping

    dfData = pd.DataFrame(columns=['tstmp', 'Unix'] + list(dfMapping['Name']))

    counter = 0

    for i in range(999999999):

        ctime = helper.getCurrentTimestamp()
        timeStart = ctime[0]

        print(f'Time: {ctime[0]}, Unix: {ctime[1]}')

        dfNew = interface.sunspec.routineReading(instConfig, d)
        dfNew['tstmp'] = ctime[0]
        dfNew['Unix'] = ctime[1]
        dfData = dfData.append(dfNew)
        counter += 1

        if counter % 6 * 15 == 0:
            dfData.to_csv(f'./tester/func_tests/dataRecording_{instConfig.CLS.nameIED}.csv')

        ctime = helper.getCurrentTimestamp()
        timeEnd = ctime[0]
        processingTime = timeEnd - timeStart

        time.sleep(10 - processingTime)


SunspecMonitorIED(ipaddr, port, slave_id)
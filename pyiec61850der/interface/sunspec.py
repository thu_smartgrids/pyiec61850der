# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 17:18:27 2023

@author: chen

Use the open-source lib sunspec2 to establish ModbusTCP connection to a SunSpec
compatible PV inverter.

The first priority at this phase is to get this concept to technically work, optimisation
of code efficiency and communication effort is the next step.

TODO: improve the code to achieve better performance for the sunspec interface.
TODO: double check if the changes in dfLookup and dfMapping are automatically passed to attributes of 
    instConfig and iedServer. Probably one needs to reassigne the updated values?
TODO: check again, do we have to return the instConfig and iedServer objects in each function?
TODO: duplicate the attribute dfMapping, one for default sunspec mapping template, one for the sunspec table consisting of active parameters
"""

import sunspec2.modbus.client as client
from sunspec2.modbus.modbus import ModbusClientError
import pandas as pd
import numpy as np
import copy
import logging
from difflib import SequenceMatcher as SM

import configurator.helper as helper

from configurator.logging import rotatingLogger

logger = rotatingLogger(__name__)


def parseMapping(instConfig=None, iedServer=None):
    """
    TODO: add desc

    generate a mappting table with valid Sunspec parameters.

    """

    logger.info('-------------------------------------------------------------------')
    logger.info('Start parsing the sunspec mapping table')

    nameIED = iedServer.dataModel.nameIED
    dfLookup = pd.read_csv(instConfig.interface.pathLookupCSV, sep=',')
    dfMapping = pd.read_excel(instConfig.interface.pathSunspecMapping, sheet_name="SunSpec_Classic")
    listPV = iedServer.configServer.der.dictListDER['PV']
    dfMappingAll = pd.DataFrame()

    for item in listPV:
        dfMappingNew = copy.deepcopy(dfMapping)
        dfMappingNew.loc[dfMappingNew['LD'] == 'PV', 'LD'] = item
        dfMappingAll = pd.concat([dfMappingAll, dfMappingNew], sort=False)
    dfMappingAll = dfMappingAll.reset_index(drop=True)
    dfMappingAll = dfMappingAll.drop_duplicates()

    dfMappingValid = dfMappingAll.loc[dfMappingAll['excluded'] != 'x']  # remove excluded parameters
    # dfMappingValid = dfMappingValid.loc[dfMappingValid['incompatible'] != 'x']  # remove incompatible parameters
    dfMappingValid = dfMappingValid.loc[
        np.isnan(dfMappingValid['Address Offset']) == False]  # remove enumeration value rows
    dfMappingValid['isActive'] = False
    dfMappingValid['isDelivered'] = False

    for idx in dfMappingValid.index:
        item = dfMappingValid.loc[idx]
        # TODO: pay attention to the instance 1 behind LN name. In the current implementation, each LN has an index number 1
        #   except for LLN0, find a pythonic way to solve this problem
        dfMappingValid.loc[idx, 'mmsAddress'] = f'{nameIED}_{item["LD"]}/{item["LN"]}1.{item["DO"]}.{item["DA"]}'
        isMonitorActive = dfMappingValid.loc[idx, 'mmsAddress'] in list(dfLookup['monitorDA'])
        isControlActive = dfMappingValid.loc[idx, 'mmsAddress'] in list(dfLookup['controlDA']) and dfMappingValid.loc[
            idx, 'RW Access (RW)'] == 'RW'
        dfMappingValid.loc[idx, 'isActive'] = isMonitorActive or isControlActive

    dfMappingActive = dfMappingValid.loc[dfMappingValid['isActive'] == True]
    instConfig.interface.dfSunspecMapping = dfMappingActive

    logger.info(f'IED server has detected {len(dfMappingActive)} active sunspec parameters.')
    logger.info('-------------------------------------------------------------------\n')


def connectServer(slave_id=None, ipaddr=None, port=None):
    """
    It checks the availability of the configured sunspec server. If the server exists
    and can be connected, then move on to an initial reading and update the CLS lookup
    table accordingly. Otherwise

    """
    # slave_id=126
    # ipaddr='192.168.170.84'
    # port=502

    try:
        d = client.SunSpecModbusClientDeviceTCP(slave_id, ipaddr, port)
        d.connect()
        isConnected = True
        sunspecConn = d
        logger.info(f'Connected to the sunspec server {ipaddr}')
    except ModbusClientError:  # most probably ModbusClientError
        logger.exception(f'The configured sunspec server {ipaddr} is not available')
        isConnected = False
        sunspecConn = None
    except:
        logger.exception('unexpected sunspec connection error, using profiles instead of real-time data')
        isConnected = False
        sunspecConn = None
    return isConnected, sunspecConn


def keepAlive(sunspecConn=None):
    """
    This function can be used to regularly check the active connection to the sunSpec
    server, in case of disconnection, try to reconnect.
    """

    isConnected = False
    try:
        sunspecConn.connect()
        isConnected = True
        logger.info('Connection to the sunspec server is alive.')
    except ModbusClientError:  # most probably ModbusClientError
        logger.exception('The configured sunspec server is no longer available, try to reconnect')
        isConnected = False

        try:
            sunspecConn.connect()
            isConnected = True
            logger.info('The sunspec server is sucessfully reconnected.')
        except ModbusClientError:
            logger.exception('Sunspec Server reconnection failed, ModbusClientError')
    except:
        logger.exception('unexpected sunspec connection error, reconnection failed')

    return isConnected, sunspecConn


def updateMapping(instConfig=None, iedServer=None):
    """
    This function initializes a sunspec connection to the server via ModbusTCP. Meanwhile it updates
    the interface configuration in the CLS lookup table.

    All sunspec parameters that deliver    measurments or status information will be recorded in
    the CLS lookup table.

    During the CLS server routine, associated DO will be updated with "sunspec" as data source.

    It is possible to validate the sunspec parameters regularly, and remove inactivate parameters
    or add new sunspec parameters.

    TODO: when indexing the DA ref, only empty list and list with length 1 are considered, because
    each DA should have an unique mmsAddress. But duplicated mmsAddress could still occur due to
    errors in the engineering process. Implement a unique address check later.
    """

    logger.info('-------------------------------------------------------------------')
    logger.info('Start initial reading of the sunspec parameters and update of the lookup table')

    dfMapping = instConfig.interface.dfSunspecMapping
    dfLookup = instConfig.interface.dfLookupCSV

    isUpdateLookup = False  # only update the lookup table if one sunspec device is active and delivers values
    for idx, key in enumerate(instConfig.der.dictListDER['PV']):
        ipaddr = getattr(instConfig, key.lower()).inverterIP
        slave_id = getattr(instConfig, key.lower()).inverterSlaveID
        port = getattr(instConfig, key.lower()).tcpPort

        [isConnected, sunspecConn] = connectServer(slave_id, ipaddr, port)
        if isConnected:
            try:
                sunspecConn.scan()
            except:
                logger.exception('Could not perform the sunspec mapping scan, check the slave ID and port')
                continue
            listIdxParameter = dfMapping.loc[dfMapping['LD'] == key].index.to_list()
            if listIdxParameter != []:
                for idxPara in listIdxParameter:
                    modelID = dfMapping.loc[idxPara, 'Model ID']

                    # check if the parameter is coming with a scale factor
                    hasScale = helper.isValNumber(dfMapping.loc[idxPara, 'factor'])
                    if hasScale and np.isnan(dfMapping.loc[idxPara, 'factor']):
                        hasScale = False

                    if modelID in sunspecConn.models:
                        name = dfMapping.loc[idxPara, 'Name']
                        value = getattr(sunspecConn.models[modelID][0], name).cvalue
                        if value is None:
                            if instConfig.interface.keepNonePara:
                                logger.warning(
                                    f'The sunspec parameter {modelID} - {name} returns None, but still keep it in the list.')
                            else:
                                logger.warning(
                                    f'The sunspec parameter {modelID} - {name} returns None, set it to inactive in the sunspec mapping table.')
                                continue

                        dfMapping.loc[idxPara, 'isDelivered'] = True
                        mmsAddressDA = dfMapping.loc[idxPara, 'mmsAddress']
                        # TODO: convert the lookup search into a method
                        idxLookup = dfLookup.loc[dfLookup[
                                                     'monitorDA'] == mmsAddressDA].index.to_list()  # controlDA always comes along with a monitorDA

                        if len(idxLookup) != 1:
                            logger.warning(f'Something went wrong, can not locate lookup index for DA {mmsAddressDA}')
                        else:
                            # TODO: place data buffer update in a separate method?? iedServer.updateDataBufferConfig()??
                            idxLookup = idxLookup[0]
                            isControl = dfMapping.loc[idxPara, 'RW Access (RW)'] == 'RW'
                            isUpdateLookup = True
                            logger.info(
                                f'Succesfully located DA {mmsAddressDA} by sunspec mapping, enable sunspec dataSource')
                            dfLookup.loc[
                                idxLookup, 'connObject'] = sunspecConn  # object saved in the DataFrame will be of the type Series
                            dfLookup.loc[idxLookup, 'dataSource'] = 'sunspec'
                            dfLookup.loc[idxLookup, 'isMonitor'] = True
                            # TODO: it is hard to unify the configuration of all data sources, so there are some keys that could be missleading.
                            #       find a way out
                            dfLookup.loc[idxLookup, 'configJSON'] = str({'ipaddr': ipaddr,
                                                                         'slave_id': slave_id,
                                                                         'port': port
                                                                         })
                            if hasScale:
                                dfLookup.loc[idxLookup, 'scaleFactor'] = dfMapping.loc[idxPara, 'factor']
                            if isControl:
                                dfLookup.loc[idxLookup, 'isControl'] = True

                            # also update the DataBuffer instances
                            # Attention, index lookup is not always the index in listDataBuffer, so relocate the DataBuffer instance by search mmsAddress
                            idxBuffer = [idx for idx, item in enumerate(iedServer.listDataBuffer) if
                                         item.DO.monitorAddressDA == mmsAddressDA]
                            if idxBuffer == []:
                                logger.warning(
                                    f'Something went wrong, can not locate DataBuffer index for DA {mmsAddressDA}')
                            else:
                                idxBuffer = idxBuffer[0]
                                iedServer.listDataBuffer[idxBuffer].dataSource = 'sunspec'
                                iedServer.listDataBuffer[
                                    idxBuffer].connObj = sunspecConn  # the connObj must be written as attribute to the DataBuffer instance!
                                iedServer.listDataBuffer[idxBuffer].isMonitor = True
                                if hasScale:
                                    iedServer.listDataBuffer[idxBuffer].scaleFactor = dfMapping.loc[idxPara, 'factor']
                                if isControl:
                                    iedServer.listDataBuffer[idxBuffer].isControl = True

        else:
            logger.warning(f'No sunspec service available for the host {ipaddr}:{port}')

    dfMappingFinal = dfMapping.loc[dfMapping['isDelivered'] == True]
    instConfig.interface.dfSunspecMapping = dfMappingFinal
    iedServer.dfLookupCSV = dfLookup
    instConfig.interface.dfLookupCSV = dfLookup
    if isUpdateLookup and instConfig.interface.forceOverwrite:
        dfLookup.to_csv(instConfig.interface.pathLookupCSV, index=False)
        logger.info('Successfully exported the lookup table of running IED with updated configuration')
    else:
        logger.info('The lookup table of running IED has been updated, but will not overwrite the original CSV file.')

    dfMappingFinal.to_excel(instConfig.interface.pathSunspecMappingExport)

    logger.info(
        f'Initial sunspec reading finished, {len(dfMappingFinal)} sunspec parameters are available for the IED server.')
    logger.info('-------------------------------------------------------------------\n')


def readValFromInverter(conn=None, modelID=None, paraName=None):
    """
    Perform a read value event to get cvalue for a specific sunspec parameter.
    """

    val = getattr(conn.models[modelID][0], paraName).cvalue

    return val


def writeValToInverter(conn=None, modelID=None, paraName=None, val=None):
    """
    Perform a write value event by setting the attribute cvalue of a specific sunspec parameter.
    There are two variants, the first directly performs the control on the pre-defined parameter, if the first one
    fails, variant 2 will be called, which set the associated enable parameter on True and then write the value.

    TODO: implement a logic to verify the scale factor of the associated sunspec parameter. Currently the SF is taken
    directly from the sunspec mapping table, which are normally set to 0.01 (-2), e.g. for WMaxLimPct
    to transform between percentage value and the float value. In other use cases this could be different, it is
    better to double check the scale factor before writing any control values.

    TODO: test whether this method for validating new value works
    On the experimental inverter, control check by reading new values did not throw any error,
    HOWEVER, when WMaxLimPctEna was set to 0, the control limit was taken but not in effect.
    This is really bad but there is no way out other than visually check the actual output power.
    """

    def sunspecWriter(func):
        def error_catcher(*args, **kwargs):
            try:
                res = func(*args, **kwargs)
            except AttributeError as err:
                logger.exception(
                    f'Failed to control due to AttributeError, {paraName} probably not implemented for the inverter')
            except ValueError as err:
                logger.exception(f'Failed to control due to ValueError, {val} of type {type(val)} is not a valid type')
            except KeyError as err:
                logger.exception(f'Failed to control due to KeyError, probably wrong modelID {modelID}')
            except TimeoutError as err:
                logger.exception(
                    f'Failed to control due to TimeoutError, the inverter probably blocks control function')
            except ModbusClientError as err:
                logger.exception('Failed to control, the inverter raised ModbusClient Error')
            except Exception as err:
                logger.exception(f'Failed to write action to parameter {paraName} due to unknown error')
            else:
                return res

        return error_catcher

    @sunspecWriter
    def singleValueWritter(conn, modelID, paraName, val, typeEna, isWritten):
        if typeEna == 'ignore':
            # be careful when using this variant, since some standards require enabling for each writing action
            paraNameEna = None
            pass
        elif typeEna == 'identifier':
            isEnabled = False
            for enaIdentifer in ['Ena', '_Ena', 'ena', '_ena']:
                try:
                    paraNameEna = f'{paraName}{enaIdentifer}'
                    objEna = getattr(conn.models[modelID][0], paraNameEna)
                    if objEna.cvalue != 1:
                        logger.info(f'Target control parameter is not enabled, set {paraName}{enaIdentifer} to 1')
                        objEna.cvalue = 1
                        objEna.write()
                        objEna.read()
                        if objEna.cvalue != 1:
                            logger.warning(
                                f'Tried to set 1 for associated enable parameter, but not taken by the inverter, control probably can not be proceeded.')
                        isEnabled = True
                        break
                    else:
                        logger.info(f'The enable parameter {paraNameEna} already has Boolean value 1')
                        isEnabled = True
                        break
                except:
                    logger.exception(
                        f'Failed to set 1 for associated enable parameter {paraName}{enaIdentifer}, probaly wrong name.')
            if not isEnabled:
                return
        elif typeEna == 'fuzzy':
            listPoints = list(conn.models[modelID][0].points)
            listMatch = [(point, SM(None, point, f'{paraName}_Ena').ratio()) for idx, point in enumerate(listPoints)]
            paraNameEna = listMatch[np.argmax([item[1] for item in listMatch])][0]
            objEna = getattr(conn.models[modelID][0], paraNameEna)
            if objEna.cvalue != 1:
                logger.info(f'Target control parameter is not enabled, set {paraNameEna} to 1')
                objEna.cvalue = 1
                objEna.write()
                objEna.read()
                if objEna.cvalue != 1:
                    logger.warning(
                        f'Tried to set 1 for associated enable parameter, but not taken by the inverter, control probably can not be proceeded.')
            else:
                pass
        else:
            logger.warning(f'Unknown type {typeEna} for enabling control parameter, just perform the control')

        logger.info(f'Passing control value {val} to parameter {paraName}')
        obj = getattr(conn.models[modelID][0], paraName)
        obj.cvalue = val
        obj.write()

        if paraNameEna is not None:
            objEna = getattr(conn.models[modelID][0], paraNameEna)
            objEna.cvalue = 1
            objEna.write()
            logger.info(f'Setting the ENABLE parameter {paraNameEna} to 1')
        else:
            logger.warning('No ENABLE parameter availabe, the control might fail.')

        obj.read()
        if abs(obj.cvalue - val) < 1e-5:
            isWritten = True
            logger.info(
                'Success, passing the control value did not throw error, check whether the control was performed')
        else:
            raise ValueError('Passing the control value did not throw error, but the value was not taken')

        return isWritten

    logger.info('-------------------------------------------------------------------')
    isWritten = False

    # logger.info(f'Control variant 1: passing control to inverter by directly setting {paraName} to {val}')
    # typeEna = 'ignore'
    # isWritten = singleValueWritter(conn, modelID, paraName, val, typeEna, isWritten)

    if not isWritten:
        logger.info(f'Control variant 2: enable the control parameter by identifier and then set the value to {val}')
        typeEna = 'identifier'
        isWritten = singleValueWritter(conn, modelID, paraName, val, typeEna, isWritten)
    if not isWritten:
        logger.info(f'Control variant 3: enable the control parameter by fuzzy match and then set the value to {val}')
        typeEna = 'fuzzy'
        isWritten = singleValueWritter(conn, modelID, paraName, val, typeEna, isWritten)
    if not isWritten:
        logger.warning(f'Both control variants failed to write {val} to the parameter {paraName} in model {modelID}')
        logger.info('Contact inverter owner or technician to find out whether the control function has been locked.')

    logger.info('-------------------------------------------------------------------\n')

    return isWritten


def readSingleValue(instConfig=None, iedServer=None, dataBuffer=None):
    """
    This function can be used to read single sunspec parameters based on the configuration
    stored in the associated DataBuffer instance.

    If the sunspec connection is gone, the process will try to reconnect server.

    TODO: is it necessary to load both dfMapping and dfLookup?

    TODO: is it necessary to loop over the entire lookup table and matching table for each DataBuffer instance?
    """

    dfMapping = instConfig.interface.dfSunspecMapping
    dfLookup = instConfig.interface.dfLookupCSV

    val = None
    mmsAddress = dataBuffer.DO.monitorAddressDA
    idxLookup = dfLookup.loc[dfLookup['monitorDA'] == mmsAddress].index.to_list()
    idxMapping = dfMapping.loc[dfMapping['mmsAddress'] == mmsAddress].index.to_list()

    if idxLookup == []:
        logger.warning(f'Something went wrong, can not locate the lookup index for DA {mmsAddress}')
    elif idxMapping == []:
        logger.warning(f'Something went wrong, can not locate the mapping index for DA {mmsAddress}')
    else:
        idxLookup = idxLookup[0]
        idxMapping = idxMapping[0]
        try:
            sunspecConnThisPara = dataBuffer.connObj
            modelID = dfMapping.loc[idxMapping, 'Model ID']
            name = dfMapping.loc[idxMapping, 'Name']
            val = readValFromInverter(sunspecConnThisPara, modelID, name)

            logger.info(f'Sucessfully updated sunspec parameter {modelID} - {name} with value {val}')
        except ModbusClientError:
            logger.exception('Sunspec server for DA unavailable, try to reconnect')
            logger.info(f'New reading for DA {mmsAddress} will be performed in the next iteration')
            [isConnected, sunspecConnThisPara] = keepAlive(sunspecConnThisPara)
        except AttributeError:
            logger.exception('Reading sunspec parameter caused AttributeError, skip this parameter')
            logger.info(f'New reading for DA {mmsAddress} will be performed in the next iteration')
        except:
            logger.exception(
                f'New reading for DA {mmsAddress} failed due to unexpected error, please check source code.')

    return val


def routineReading(instConfig=None, sunspecConn=None):
    """
    This function should perform the sunspec reading for a list of parameters as routine.
    Currently it is constructed but not used, as the runtime service processing.routine
    will go through all Databuffer instances and call update single value accordingly.

    Its functionality could be used to read and process data that are not directly involved
    in the real-time IEC 61850 application, e.g. display sunspec values or status check.

    Step to the function readSingleValue for more details.
    """

    dfLookup = instConfig.interface.dfLookupCSV
    dfLookupSunspec = dfLookup.loc[dfLookup['dataSource'] == 'sunspec']
    dfMapping = instConfig.interface.dfSunspecMapping

    sunspecConn.scan()
    dfReading = pd.DataFrame(columns=dfMapping['Name'])
    for idx, name in enumerate(list(dfReading)):
        idxMapping = dfMapping.loc[dfMapping['Name'] == name].index.to_list()[0]
        modelID = dfMapping.loc[idxMapping, 'Model ID']

        if sunspecConn is None:
            sunspecConn = dfLookupSunspec.loc[idx, 'connObject']

        val = readValFromInverter(sunspecConn, modelID, name)
        dfReading.loc[0, name] = val

    return dfReading


def initSunspecInterface(instConfig=None, iedServer=None):
    """
    This function performs an initial sunspec service activation after the IED server has been
    started.
    """

    parseMapping(instConfig, iedServer)
    updateMapping(instConfig, iedServer)